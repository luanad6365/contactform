<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CfConstant
 * Class contact form constant define all const in system
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class CfConst {
	
	//Define Disqus identifier, user for blog page
	const DISQUS_IDENTIFIER = 'bettercontactform.net';
	
	//Role of user
	const ROLE_ADMIN = 'admin';
	const ROLE_MEMBER = 'member';
	
	//Type of social provider connect
	const SOCIAL_PROVIDER_FACEBOOK = 'facebook';
	const SOCIAL_PROVIDER_GOOGLE = 'google';
	
	//Language
	const CF_LANG_ENGLISH = 'EN';
	const CF_LANG_VIETNAMESE = 'VN';
	const CF_LANG_FRENCH = 'FR';
	const CF_LANG_SPAIN = 'ES';
    const CF_LANG_GERMANY = 'DE';
    const CF_LANG_ITALY = 'IT';
    const CF_LANG_PORTUGAL = 'PT';
    const CF_LANG_RUSSIA = 'RU';
    
    public static function getAllLanguage(){
        return array(
            self::CF_LANG_ENGLISH => 'English',
            //self::CF_LANG_VIETNAMESE => 'Vietnamese',
            self::CF_LANG_FRENCH => 'French',
            self::CF_LANG_SPAIN => 'Spanish',
            self::CF_LANG_GERMANY => 'German',
            self::CF_LANG_ITALY => 'Italian',
            self::CF_LANG_PORTUGAL => 'Portuguese',
            self::CF_LANG_RUSSIA => 'Russian',
        );
    }
	
	//Button "contact us" position
	const BTN_CONTACT_POS_LEFT = 'left';
	const BTN_CONTACT_POS_BOTTOM_LEFT = 'bottom_left';
	const BTN_CONTACT_POS_BOTTOM_RIGHT = 'bottom_right';
	const BTN_CONTACT_POS_RIGHT = 'right';
	
	//Contact form style
	const CF_STYLE_BASIC = 'style_basic';
	const CF_STYLE_NORMAL = 'style_normal';
	const CF_STYLE_CLASSIC = 'style_classic';
	const CF_STYLE_CHILD = 'style_child';
	const CF_STYLE_FREESTYLE = 'style_freestyle';
	const CF_STYLE_RESTAURANT = 'style_restaurant';
	
	public static function getAllFormStyle(){
        return array(
            self::CF_STYLE_BASIC => 'Basic',
            self::CF_STYLE_NORMAL => 'Normal',
            self::CF_STYLE_CLASSIC => 'Classic',
			self::CF_STYLE_CHILD => 'Child',
            self::CF_STYLE_FREESTYLE => 'Freestyle',
            self::CF_STYLE_RESTAURANT => 'Restaurant',
        );
    }
	
	//Get form avatar when create, edit
	public static function getFormAvatar($style) {
		$imgPath = self::IMG_DIR_FORM_AVATAR . 'contact-form_basic.jpg';
		switch ($style) {
			case self::CF_STYLE_RESTAURANT:
				$imgPath = self::IMG_DIR_FORM_AVATAR . 'contact-form_restaurant.jpg';
				break;
			case self::CF_STYLE_NORMAL:
				$imgPath = self::IMG_DIR_FORM_AVATAR . 'contact-form_normal.jpg';
				break;
			case self::CF_STYLE_CLASSIC:
				$imgPath = self::IMG_DIR_FORM_AVATAR . 'contact-form_classic.jpg';
				break;
			case self::CF_STYLE_CHILD:
				$imgPath = self::IMG_DIR_FORM_AVATAR . 'contact-form_child.jpg';
				break;
			case self::CF_STYLE_FREESTYLE:
				$imgPath = self::IMG_DIR_FORM_AVATAR . 'contact-form_freestyle.jpg';
				break;
			default:
				$imgPath = self::IMG_DIR_FORM_AVATAR . 'contact-form_basic.jpg';
				break;
		}
		return $imgPath;
	}

	//Contact form default color
	const CF_DEFAULT_COLOR = '#009fe1';
    //Contact form default extra info
    const CF_DEFAULT_EXTRA_INFO = 'Tel: 0164 966 889';
	
	
	//Contact form status
	const CF_FORM_STATUS_INACTIVE = 'inactive';
	const CF_FORM_STATUS_ACTIVE = 'active';
	const CF_FORM_STATUS_DISABLE = 'disable';
	const CF_FORM_STATUS_DELETE = 'delete';
	
	public static function getAllFormStatus(){
        return array(
            self::CF_FORM_STATUS_INACTIVE => 'Inactive',
            self::CF_FORM_STATUS_ACTIVE => 'Active',
            self::CF_FORM_STATUS_DISABLE => 'Disable',
			self::CF_FORM_STATUS_DELETE => 'Delete',
        );
    }
	
	//Key of form language
	const LANG_KEY_CLASS_SIZE = 'class_size';
	const LANG_KEY_CONTACT_LABEL = 'contact_label';
	const LANG_KEY_INVALID_NAME = 'invalid_name';
	const LANG_KEY_INVALID_EMAIL = 'invalid_email';
	const LANG_KEY_INVALID_PHONE = 'invalid_phone';
	const LANG_KEY_INVALID_COMPANY = 'invalid_company';
	const LANG_KEY_INVALID_WEBSITE = 'invalid_website';
	const LANG_KEY_INVALID_SUBJECT = 'invalid_subject';
	const LANG_KEY_INVALID_MESSAGE = 'invalid_message';
	const LANG_KEY_BTN_CONTACT = 'btn_contact';
	const LANG_KEY_BTN_SEND = 'btn_send';
	const LANG_KEY_PLACEHOLDER_NAME = 'placeholder_name';
	const LANG_KEY_PLACEHOLDER_EMAIL = 'placeholder_email';
	const LANG_KEY_PLACEHOLDER_PHONE = 'placeholder_phone';
	const LANG_KEY_PLACEHOLDER_COMPANY = 'placeholder_company';
	const LANG_KEY_PLACEHOLDER_WEBSITE = 'placeholder_website';
	const LANG_KEY_PLACEHOLDER_SUBJECT = 'placeholder_subject';
	const LANG_KEY_PLACEHOLDER_MESSAGE = 'placeholder_message';
	const LANG_KEY_TO_RECIPIENT = 'to_recipient';
	
	//Class size of language
	const LANG_CLASS_SIZE_SMALL = 'class_size_small';
	const LANG_CLASS_SIZE_MEDIUM = 'class_size_medium';
	const LANG_CLASS_SIZE_LARGE = 'class_size_large';
	
	public static function getAllClassSize(){
	  return array(
            self::LANG_CLASS_SIZE_SMALL => 'Small',
            self::LANG_CLASS_SIZE_MEDIUM => 'Medium',
            self::LANG_CLASS_SIZE_LARGE => 'Large',
        );	
	}

	public static function getAllKeyFormLanguage(){
        return array(
            self::LANG_KEY_CLASS_SIZE => 'Class size',
            self::LANG_KEY_CONTACT_LABEL => 'Contact label',
            self::LANG_KEY_INVALID_NAME => 'Invalid name',
            self::LANG_KEY_INVALID_EMAIL => 'Invalid email',
            self::LANG_KEY_INVALID_PHONE => 'Invalid phone',
            self::LANG_KEY_INVALID_COMPANY => 'Invalid company',
            self::LANG_KEY_INVALID_WEBSITE => 'Invalid website',
            self::LANG_KEY_INVALID_SUBJECT => 'Invalid subject',
            self::LANG_KEY_INVALID_MESSAGE => 'Invalid message',
            self::LANG_KEY_BTN_CONTACT => 'Button contact',
            self::LANG_KEY_BTN_SEND => 'Button send',
			self::LANG_KEY_PLACEHOLDER_NAME => 'Placeholder name',
            self::LANG_KEY_PLACEHOLDER_EMAIL => 'Placeholder email',
            self::LANG_KEY_PLACEHOLDER_PHONE => 'Placeholder phone',
            self::LANG_KEY_PLACEHOLDER_COMPANY => 'Placeholder company',
            self::LANG_KEY_PLACEHOLDER_WEBSITE => 'Placeholder website',
            self::LANG_KEY_PLACEHOLDER_SUBJECT => 'Placeholder subject',
            self::LANG_KEY_PLACEHOLDER_MESSAGE => 'Placeholder message',
            self::LANG_KEY_TO_RECIPIENT => 'To recipient',
        );
    }
	
	//Admin Module
	const ADMIN_PAGE_SIZE = 10;
	//Frontend Module
	const FRONTEND_PAGE_SIZE = 5;
	
	//Image upload folder
	const IMG_DIR_BASE_UPLOAD = '/upload/images/';
	//Feature page
	const IMG_DIR_FEATURES = '/upload/images/features/';
	//Blog page
	const IMG_DIR_BLOG = '/upload/images/blog/';
	//Slide home page
	const IMG_DIR_SLIDE = '/upload/images/slide/';
	//Form image avatar
	const IMG_DIR_FORM_AVATAR = '/upload/images/form_avatar/';
	
	//Image thumb
	const THUMB_FOLDER_NAME = 'thumbs/';
	const THUMB_IMG_PREFIX_NAME = 'thumb_';
	const THUMB_IMG_DEFAULT_WIDTH = '150'; //px
	
	//Data send mail
	const DATA_STATUS_SENT = 'sent'; //Customer sent
	const DATA_STATUS_RECEIVE = 'receive';//User receive email
	const DATA_STATUS_DELETE = 'delete';
	const DATA_STATUS_REPLY = 'reply';
	
	public static function getAllDataStatus(){
        return array(
            self::DATA_STATUS_SENT => 'Sent',
			self::DATA_STATUS_RECEIVE => 'Receive',
            self::DATA_STATUS_DELETE => 'Delete',
			self::DATA_STATUS_REPLY => 'Reply',
        );
    }

    //Authorize.net environment
    const PAYMENT_ENV_SANDBOX = 'sandbox';
    const PAYMENT_ENV_LIVE = 'live';

	public static function getAllEnv(){
        return array(
            self::PAYMENT_ENV_SANDBOX => 'Sandbox (Test)',
            self::PAYMENT_ENV_LIVE => 'Live',
        );
    }

    //Payment Type
    const PAYMENT_TYPE_FREE_PERIOD = 'free_period';
    const PAYMENT_TYPE_PREMIUM_MONTHLY = 'premium_monthly';
    const PAYMENT_TYPE_PREMIUM_YEARLY = 'premium_yearly';
    
    public static function getAllPaymentTypePremium(){
        $paymentConfig = PaymentConfig::model()->find();
        if ( isset($paymentConfig->monthly_premium_fee) && $paymentConfig->monthly_premium_fee) {
            $monthlyFee = $paymentConfig->monthly_premium_fee;
        } else {
            $monthlyFee = CfConst::DEFAULT_FEE_MONTHLY;
        }
        if ( isset($paymentConfig->yearly_premium_fee) && $paymentConfig->yearly_premium_fee) {
            $yearlyFee = $paymentConfig->yearly_premium_fee;
        } else {
            $yearlyFee = CfConst::DEFAULT_FEE_YEARLY;
        }
        return array(
            self::PAYMENT_TYPE_PREMIUM_MONTHLY => 'Monthly plan - '.$monthlyFee.' $',
            self::PAYMENT_TYPE_PREMIUM_YEARLY => 'Yearly plan - '.$yearlyFee.' $',
        );
    }

    //Some default value for payment
    const DEFAULT_FREE_PERIOD = 5; //days
    const DEFAULT_FEE_MONTHLY = 10; //USD
    const DEFAULT_FEE_YEARLY = 100; //USD

    //Form premium feature name
    const PREMIUM_FEATURE_MULTI_DEPARTMENT = 'multi_department';
    const PREMIUM_FEATURE_AUTORESPOND = 'auto_respond';
    
    //Email type: sent to user form
    const EMAIL_TYPE_NOTHING = 'sent_nothing';// When create or puschare form
    const EMAIL_TYPE_NOTICE = 'sent_notice';// Send notice before...day
    const EMAIL_TYPE_DEACTIVATE = 'sent_deactivate';// Send email notice form expired

    const DEFAULT_RESPOND_MESSAGE = 'Thank you for contact us. Your message has been sent successfully. We will contact you as soon as possible.';


}
