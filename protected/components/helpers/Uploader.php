<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Upload
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class Uploader {

	/*
	 *
	 * Example 
	 * $fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_FEATURES);
	 * $fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_FEATURES, $model->image, 'image', array(125, 125));
	 *  
	 * 	 */
	public static function uploadImage($model, $directory, $oldImageName = false, $attribute = 'image', $thumb = array(CfConst::THUMB_IMG_DEFAULT_WIDTH)/*width : require, height: optional*/) {
		if (!isset($model->$attribute)) {
			return false;
		}
		
		//Get instance image upload
		$uploadedFile = CUploadedFile::getInstance($model, $attribute);
		//If have upload action
		if ($uploadedFile) {
			//Create new unique image name
			$newImageName = time().'_'.$uploadedFile;
			//Create full directory
			$fullDir = Yii::app()->getBasePath() . '/..' . $directory;
			$thumbFolder = $fullDir . CfConst::THUMB_FOLDER_NAME;
			//Create folder if dont exist
			self::createFolder($fullDir);
			//Save image
			$uploadedFile->saveAs($fullDir . $newImageName);
			//Create thumb
			if (!empty( $thumb )) {
				//Create thumb folder if dont exist
				self::createFolder($thumbFolder);
				//Create thumb
				self::createThumb($newImageName, $fullDir, $thumbFolder, $thumb);
			}
			//Remove old image
			if ($oldImageName) {
				//Remove image and thumb
				self::removeImage($fullDir, $oldImageName);
				//Remove thumb
				//self::removeImage($thumbFolder, CfConst::THUMB_IMG_PREFIX_NAME . $oldImageName);
			}

			return $newImageName;
		} else { //Return current image name
			return $model->$attribute;
		}
	}

	public static function createThumb($imageName, $fromDir, $saveToDir, $thumbAttr) {
		$thumb = Yii::app()->phpThumb->create($fromDir . $imageName);
		if(count($thumbAttr) == 1){
			//Set width of thumb, auto resize hegit, keep asptec ratio
			$thumb->resize($thumbAttr[0]);
		} else {
			$thumb->adaptiveResize($thumbAttr[0], $thumbAttr[1]);	
		}
		
		$thumb->save($saveToDir . CfConst::THUMB_IMG_PREFIX_NAME . $imageName);
	}

	public static function removeImage($directory, $imageName) {
		//Re-gen full path
		if(!strpos($directory, 'protected')){
			$directory = Yii::app()->getBasePath() . '/..' . $directory;
		}
		if (file_exists($directory . $imageName)) {
			@unlink($directory . $imageName);
		}
		self::removeThumb($directory, $imageName);
	}
	
	public static function removeThumb($directory, $imageName){
		if(!strpos($directory, 'protected')){
			$directory = Yii::app()->getBasePath() . '/..' . $directory;
		}
		$thumbFolder = $directory . CfConst::THUMB_FOLDER_NAME;
		$thumbName = CfConst::THUMB_IMG_PREFIX_NAME . $imageName;
		if (file_exists($thumbFolder . $thumbName)) {
			@unlink($thumbFolder . $thumbName);
		}
	}

	public static function createFolder($folderPath) {
		if (!is_dir($folderPath)) {
			mkdir($folderPath);
			chmod($folderPath, 0777);
		}
	}

}
