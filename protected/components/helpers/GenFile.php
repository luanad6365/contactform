<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GenFile
 *
 * @author An Dinh Luan
 */
class GenFile {

	//put your code here
	public static function genScript($data) {
		//generate css file for client
		$content = file_get_contents(Yii::app()->getBasePath() . '/../client/template/template_css.css');
		$myfile = fopen(Yii::app()->getBasePath() . '/../client/css/' . md5($data['form_id']) . '.css', "w") or die("Unable to open file!");
		//Replace hash form id
		$txt = str_replace('HASH_FORM_ID', md5($data['form_id']), $content);
		//Replace FULL_DOMAIN to current domain
		$txt = str_replace('FULL_DOMAIN', ABS_BASE_URL, $txt);
		if (isset(Yii::app()->params->minify) && Yii::app()->params->minify) {
			//Minify css
			$txt = Minifier::minifyCss($txt);
		}
		fwrite($myfile, $txt);
		fclose($myfile);

		//generate js file for client
        //First, check if form is premium and still avaiable (no disable, no delete), and active multi department, then add new attribute for $data
        if($data['form_id']){
            $multiDept = FormPremiumFeature::model()->findByAttributes(
                array(
                    'form_id' => $data['form_id'],
                    'feature_name' => CfConst::PREMIUM_FEATURE_MULTI_DEPARTMENT,
                    'is_enable' => 1,
                )
            );
            if( isset($multiDept->id) && $multiDept->id ){
                $data['multi_recipient'] = json_decode( $multiDept->meta_feature );
            }
        }
		$content = file_get_contents(Yii::app()->getBasePath() . '/../client/template/template_js.js');
		$myfile = fopen(Yii::app()->getBasePath() . '/../client/js/' . md5($data['form_id']) . '.js', "w") or die("Unable to open file!");
		//Replace form data
		$txt = str_replace('DATA_FORM', json_encode($data), $content);
		//Replace hash form id
		$txt = str_replace('HASH_FORM_ID', md5($data['form_id']), $txt);
		//Replace FULL_DOMAIN to current domain
		$txt = str_replace('FULL_DOMAIN', ABS_BASE_URL, $txt);
		if (isset(Yii::app()->params->minify) && Yii::app()->params->minify) {
			//Minify js
			$txt = Minifier::minify($txt);
		}
		fwrite($myfile, $txt);
		fclose($myfile);

		//generate core js
		if (isset(Yii::app()->params->env) && Yii::app()->params->env == 'DEVELOPMENT') {
			$content = file_get_contents(Yii::app()->getBasePath() . '/../library/js/classic/cf_render.js');
			$myfile = fopen(Yii::app()->getBasePath() . '/../client/template/core_render.js', "w") or die("Unable to open file!");
			$txt = str_replace('PARAMS.baseUrl+\'', '\'' . ABS_BASE_URL, $content);
			fwrite($myfile, $txt);
			fclose($myfile);
			//Gen language, and minify
			FormLanguage::model()->genClientLanguage();
		}
		return md5($data['form_id']);
	}

}
