<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Common : All function user for system
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class Common {

	//put your code here
	//Check user login or not
	public static function checkLogin() {
		//Redirect if it guest
		if (Yii::app()->user->isGuest) {
			Yii::app()->controller->redirect(Yii::app()->getModule('user')->loginUrl);
		}
	}

	//Check user own resource (form, data,...)
	public static function checkOwnResource($resource_user_id, $return_url = false, $do_redirect = true) { //form_user_id, data_user_id
		//First, check login;
		self::checkLogin();
		//If admin, do need check because admin full control
		if (!Yii::app()->getModule('user')->isAdmin()) {
			if (Yii::app()->user->id != $resource_user_id) {
				if ($do_redirect) {
					if (!$return_url) {
						Yii::app()->controller->redirect(Yii::app()->getModule('user')->loginUrl);
					} else {
						Yii::app()->controller->redirect($return_url);
					}
				} else {
					//If not do_redirect, return false;
					return false;
				}
			}
			return true;
		}
		return true;
	}

    public static function getMonthOfYear(){
        $result = array();
        for ($i = 1; $i <= 12; $i++){
            $m = strlen($i) == 1 ? '0'.$i : $i;
            $result[$m] = $m;
        }
        return $result;
    }

    public static function getYearRange($radius = 6){
        $year = date('Y');
        $result = array();
        for ($i = $year - 1; $i <= $year + $radius; $i++){
            $result[$i] = $i;
        }
        return $result;
        
    }
    
    public static function debug($val){
        echo '<pre>';
        print_r($val);
        exit();
    }
}
