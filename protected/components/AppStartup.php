<?php

class AppStartup extends CBehavior {

    public function events() {
		return array_merge(parent::events(), array(
			'onBeginRequest' => 'beginRequest',
		));
	}

	public function beginRequest() {
		$this->initConst();
	}

	
	//Init constant url for PHP & JS
	public function initConst() {
        //Absolute path, shoud be use in <a>
		define('ABS_BASE_URL', Yii::app()->request->getBaseUrl(true));
        //Relative path, shoud be use in view (image,..)
        define('REL_BASE_URL', Yii::app()->request->baseUrl);
        //Relative theme path
		define('THEME_URL', Yii::app()->theme->baseUrl);
		$bases = array(
			'baseUrl' => ABS_BASE_URL,
			'themeUrl' => THEME_URL
		);
        //Param for js
        $params= 'PARAMS = ' . json_encode($bases);
		Yii::app()->clientScript->registerScript('BASE_DATA', $params, CClientScript::POS_END);
	}

}
