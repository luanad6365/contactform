<?php

/**
 * Description of AuthorizeNet
 * CfPayment provider based on AuthorizeNet libraby
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */

/*How to demo : Note : Sanbox acc only report all transaction once in day*/
/*
    $data = array(
        'amount' => '900',
        'card_num' => '6011000000000012',
        'exp_date' => '12/15',
        'first_name' => 'An Dinh',
        'last_name' => 'Luan SETA',
        'address' => 'Duy Tan',
        'city' => 'Ha Noi',
        'state' => 'Long Bien',
        'country' => 'Voet Nam',
        'zip' => '100000',
        'email' => 'luanad6365@gmail.com',
        'card_code' => '782'
    );
    $a  = CfPayment::getInstance()->processAuthorizeAIM($data);
    echo '<pre>';
    var_dump($a); die;
 */
class CfPayment
{
    public static $_instance = null;
    public $transaction;
    public $config;
    public $merchantInfo;
    public $sandboxMode;

    public function __construct()
    {
        //Require all lib necessary
        $path = Yii::getPathOfAlias('ext.authorizenet');
        require_once $path.'/shared/AuthorizeNetRequest.php';
        require_once $path.'/shared/AuthorizeNetTypes.php';
        require_once $path.'/shared/AuthorizeNetXMLResponse.php';
        require_once $path.'/shared/AuthorizeNetResponse.php';
        require_once $path.'/AuthorizeNetAIM.php';
        require_once $path.'/AuthorizeNetARB.php';
        require_once $path.'/AuthorizeNetCIM.php';
        require_once $path.'/AuthorizeNetSIM.php';
        require_once $path.'/AuthorizeNetDPM.php';
        require_once $path.'/AuthorizeNetTD.php';
        require_once $path.'/AuthorizeNetCP.php';

        if (class_exists("SoapClient")) {
            require_once $path.'/AuthorizeNetSOAP.php';
        }

        $this->config = PaymentConfig::model()->find();
        if (!isset($this->config->id)) {
            return false;
        }
        $this->merchantInfo = MerchantInfo::model()->findByAttributes(array('type' => $this->config->merchant_env));
        if (!isset($this->merchantInfo->login_id)) {
            return false;
        }
        $this->sandboxMode = ( $this->config->merchant_env == CfConst::PAYMENT_ENV_SANDBOX )
                ? true : false;
    }

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @param array $data Data (name, card, amount...)
     */
    public function processAuthorizeAIM($data)
    {
        //Note : set $VERIFY_PEER in AuthorizeNetRequest to false if have problem with ssl/cert.pem
        $this->transaction = new AuthorizeNetAIM($this->merchantInfo->login_id, $this->merchantInfo->login_key);
        $this->transaction->setSandbox($this->sandboxMode);
        $this->transaction->setFields(
            $data
        );

        $response = $this->transaction->authorizeAndCapture();

        return $response;
    }

    public function getTransactionDetail($transactionId)
    {
        // Get Settled Batch List
        $this->transaction = new AuthorizeNetTD($this->merchantInfo->login_id, $this->merchantInfo->login_key);
        $this->transaction->setSandbox($this->sandboxMode);

        // Get Transaction Details
        $response = $this->transaction->getTransactionDetails($transactionId);
        //$response->xml->transaction->transactionStatus;

        return $response;
    }
}