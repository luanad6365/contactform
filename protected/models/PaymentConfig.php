<?php

/**
 * This is the model class for table "payment_config".
 *
 * The followings are the available columns in table 'payment_config':
 * @property integer $id
 * @property string $merchant_env
 * @property string $authorize_md5_setting
 * @property integer $free_period
 * @property double $monthly_premium_fee
 * @property double $yearly_premium_fee
 * @property integer $notice_before_day
 * @property string $created
 * @property string $updated
 */
class PaymentConfig extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment_config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('merchant_env, free_period, monthly_premium_fee, yearly_premium_fee', 'required'),
            array('authorize_md5_setting', 'length', 'is' => 32),
			array('free_period, notice_before_day', 'numerical', 'integerOnly'=>true),
			array('monthly_premium_fee, yearly_premium_fee', 'numerical'),
			array('merchant_env, authorize_md5_setting', 'length', 'max'=>255),
			array('created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, merchant_env, authorize_md5_setting, free_period, monthly_premium_fee, yearly_premium_fee, notice_before_day, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'merchant_env' => 'Merchant Env',
			'authorize_md5_setting' => 'Authorize Md5 Setting',
			'free_period' => 'Free Period (Days)',
			'monthly_premium_fee' => 'Monthly Premium Fee (USD)',
			'yearly_premium_fee' => 'Yearly Premium Fee (USD)',
			'notice_before_day' => 'Notice Before Day',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('merchant_env',$this->merchant_env,true);
		$criteria->compare('authorize_md5_setting',$this->authorize_md5_setting,true);
		$criteria->compare('free_period',$this->free_period);
		$criteria->compare('monthly_premium_fee',$this->monthly_premium_fee);
		$criteria->compare('yearly_premium_fee',$this->yearly_premium_fee);
		$criteria->compare('notice_before_day',$this->notice_before_day);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
