<?php

/**
 * This is the model class for table "data_receive".
 *
 * The followings are the available columns in table 'data_receive':
 * @property integer $id
 * @property integer $user_id
 * @property integer $form_id
 * @property string $form_style
 * @property string $form_language
 * @property string $recipient_email
 * @property string $from_site
 * @property string $customer_email
 * @property string $customer_name
 * @property string $customer_phone
 * @property string $customer_company
 * @property string $customer_website
 * @property string $customer_subject
 * @property string $customer_message
 * @property string $create_at
 * @property string $data_status
 */
class DataReceive extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'data_receive';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, form_id', 'numerical', 'integerOnly' => true),
			array('form_style, data_status', 'length', 'max' => 50),
			array('form_language, recipient_email, from_site, customer_email, customer_name, customer_phone, customer_company, customer_website, customer_subject', 'length', 'max' => 255),
			array('customer_message, create_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, form_id, form_style, form_language, recipient_email, from_site, customer_email, customer_name, customer_phone, customer_company, customer_website, customer_subject, customer_message, create_at, data_status', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user_info' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'user_id' => 'Owner Form',
			'form_id' => 'Form',
			'form_style' => 'Form Style',
			'form_language' => 'Form Language',
			'recipient_email' => 'Recipient Email',
			'from_site' => 'From Domain',
			'customer_email' => 'Customer Email',
			'customer_name' => 'Customer Name',
			'customer_phone' => 'Customer Phone',
			'customer_company' => 'Customer Company',
			'customer_website' => 'Customer Website',
			'customer_subject' => 'Customer Subject',
			'customer_message' => 'Customer Message',
			'create_at' => 'Create At',
			'data_status' => 'Data Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->with = array('user_info');
		$criteria->compare('id', $this->id);
		//User user_id like search email in criterial, set true for search with string
		$criteria->compare('user.email', $this->user_id, true);
		$criteria->compare('form_id', $this->form_id);
		$criteria->compare('form_style', $this->form_style, true);
		$criteria->compare('form_language', $this->form_language, true);
		$criteria->compare('recipient_email', $this->recipient_email, true);
		$criteria->compare('from_site', $this->from_site, true);
		$criteria->compare('customer_email', $this->customer_email, true);
		$criteria->compare('customer_name', $this->customer_name, true);
		$criteria->compare('customer_phone', $this->customer_phone, true);
		$criteria->compare('customer_company', $this->customer_company, true);
		$criteria->compare('customer_website', $this->customer_website, true);
		$criteria->compare('customer_subject', $this->customer_subject, true);
		$criteria->compare('customer_message', $this->customer_message, true);
		$criteria->compare('t.create_at', $this->create_at, true);
		$criteria->compare('data_status', $this->data_status, true);
        //$criteria->order = 't.create_at DESC';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => CfConst::ADMIN_PAGE_SIZE),
		));
	}
	
	//Manage data submit via single form
	public function search_single() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->with = array('user_info');
		$criteria->compare('id', $this->id);
		//User user_id like search email in criterial, set true for search with string
		$criteria->compare('user.email', $this->user_id, true);
		$criteria->compare('form_id', $this->form_id);
		$criteria->compare('form_style', $this->form_style, true);
		$criteria->compare('form_language', $this->form_language, true);
		$criteria->compare('recipient_email', $this->recipient_email, true);
		$criteria->compare('from_site', $this->from_site, true);
		$criteria->compare('customer_email', $this->customer_email, true);
		$criteria->compare('customer_name', $this->customer_name, true);
		$criteria->compare('customer_phone', $this->customer_phone, true);
		$criteria->compare('customer_company', $this->customer_company, true);
		$criteria->compare('customer_website', $this->customer_website, true);
		$criteria->compare('customer_subject', $this->customer_subject, true);
		$criteria->compare('customer_message', $this->customer_message, true);
		$criteria->compare('t.create_at', $this->create_at, true);
		$criteria->compare('data_status', $this->data_status, true);
        //$criteria->order = 't.create_at DESC';
		
		//If not Admin
		if (!Yii::app()->getModule('user')->isAdmin()) {
			$criteria->addCondition('data_status != "'.CfConst::DATA_STATUS_DELETE.'"');
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => CfConst::ADMIN_PAGE_SIZE),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DataReceive the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	//Delete data reeceive, only update status
	public function completelyDelete() {
		//Delete cant be delete again
		if ($this->id && $this->data_status != CfConst::DATA_STATUS_DELETE) {
			//Check if data is own by this use do delete
			$userOwnData = true;
			$userOwnData = Common::checkOwnResource($this->user_id, false, false);
			if ($userOwnData) {
				$this->data_status = CfConst::DATA_STATUS_DELETE;
				$this->save();
			}
		}
	}

}
