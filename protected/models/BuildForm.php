<?php

class BuildForm extends CFormModel {

    public $form_language;
	public $has_name;
    public $has_email;
	public $has_phone;
	public $has_company;
	public $has_website;
	public $has_subject;
    public $has_message;
    public $form_color;
    public $button_position;
	public $form_style;//basic, classic, freestyle..
    public $extra_info;
    public $custom_logo_link;
	public $recipient_email;
    
    public $form_id;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('has_name, has_email, has_phone, has_company, has_website, has_subject, has_message', 'safe'),
            array('form_language, form_color, button_position, form_style, extra_info, custom_logo_link, recipient_email, form_id', 'safe'),
            array('recipient_email, form_color', 'required'),
            array('recipient_email', 'email'),
            array('custom_logo_link', 'validate_image'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
			'has_name' => '',
			'has_email' => '',
			'has_phone' => '',
			'has_company' => '',
			'has_website' => '',
			'has_subject' => '',
        );
    }
    
    //Validate URL image
    public function validate_image($attribute, $params){
        if($this->$attribute && !$this->checkRemoteFile($this->$attribute)){
            $this->addError($attribute, 'Image link does not valid');
        } else {
            
        }
    }
    
    public function checkRemoteFile($url) {
        //check is a URL
        if ( !preg_match('/(https:[\/][\/]|http:[\/][\/]|www.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9\-\._\?\,\'\/\\\+&amp;%\$#\=~])*$/', $url) ){
            return false;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (curl_exec($ch) !== FALSE) {
            if (getimagesize($url) !== false) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
