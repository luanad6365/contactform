<?php

/**
 * This is the model class for table "users_payment".
 *
 * The followings are the available columns in table 'users_payment':
 * @property integer $id
 * @property integer $transaction_id
 * @property integer $user_id
 * @property integer $form_id
 * @property string $payment_type
 * @property double $amount
 * @property string $purchase_date
 * @property string $expired_date
 * @property string $new_expired_date
 */
class UsersPayment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transaction_id, user_id, form_id', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('payment_type', 'length', 'max'=>50),
			array('purchase_date, expired_date, new_expired_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, transaction_id, user_id, form_id, payment_type, amount, purchase_date, expired_date, new_expired_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user_info' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'transaction_id' => 'Transaction',
			'user_id' => 'User',
			'form_id' => 'Form ID',
			'payment_type' => 'Payment Type',
			'amount' => 'Amount',
			'purchase_date' => 'Purchase Date',
			'expired_date' => 'Expired Date',
			'new_expired_date' => 'New Expired Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('user_info');
		$criteria->compare('id',$this->id);
		//User user_id like search email in criterial, set true for search with string
		$criteria->compare('user.email', $this->user_id, true);
		$criteria->compare('transaction_id',$this->transaction_id);
		$criteria->compare('form_id',$this->form_id);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('expired_date',$this->expired_date,true);
		$criteria->compare('new_expired_date',$this->new_expired_date,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => CfConst::ADMIN_PAGE_SIZE),
		));
	}
    
	//Get history payment of each user with user id
	public function search_single() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('user_info');
		$criteria->compare('id',$this->id);
		//User user_id like search email in criterial, set true for search with string
		$criteria->compare('t.user_id', Yii::app()->user->id);
		$criteria->compare('transaction_id',$this->transaction_id);
		$criteria->compare('form_id',$this->form_id);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('expired_date',$this->expired_date,true);
		$criteria->compare('new_expired_date',$this->new_expired_date,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => CfConst::ADMIN_PAGE_SIZE),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
