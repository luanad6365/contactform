<?php

/**
 * This is the model class for table "form_language".
 *
 * The followings are the available columns in table 'form_language':
 * @property integer $id
 * @property string $language
 * @property string $message_key
 * @property string $message_value
 */
class FormLanguage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'form_language';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message_value', 'required'),
			array('language', 'length', 'max'=>11),
			array('message_key, message_value', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, language, message_key, message_value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'language' => 'Language',
			'message_key' => 'Message Key',
			'message_value' => 'Message Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('message_key',$this->message_key,true);
		$criteria->compare('message_value',$this->message_value,true);
		$criteria->order = 'language';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array('pageSize' => CfConst::ADMIN_PAGE_SIZE + 8),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormLanguage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
		//Only effect in contact form site
	public function genAdminLanguage(){
		$datas = $this->findAll(array('order'=>'language'));		
		if  ($datas) {
			//$myPhpFile = fopen(Yii::app()->getBasePath().'/messages/form_messages.php', "w") or die("Unable to open file!");
			$myJsFile = fopen(Yii::app()->getBasePath().'/../library/js/classic/form_messages.js', "w") or die("Unable to open file!");
			$txtPhp = "<?php\n";
			//$txtPhp .= "\$message = array();\n";
			$txtJs = "var CF_FORM_MESSAGE = {};";
			$allLangs = FormLanguage::model()->findAll(array(
				'select'=>'t.language',
				'distinct'=>true,
			));
			foreach ($allLangs as $k => $lang){
				$txtJs .= "CF_FORM_MESSAGE.".$lang->language." = {};";
			}
			foreach ($datas as $k => $data){
				//print_r($data->value);
				//$txtPhp .= "\$message['".$data->language."']['".$data->message_key."'] = '".htmlentities($data->message_value, ENT_QUOTES)."';\n";
				//$txtJs .= "CF_FORM_MESSAGE.".$data->language.".".$data->message_key."='".htmlentities($data->message_value, ENT_QUOTES)."';";
				$txtJs .= "CF_FORM_MESSAGE.".$data->language.".".$data->message_key."='".str_replace("'", "\'", $data->message_value)."';";
			}
		}
		//Write PHP
		$txtPhp .= "?>";
		//fwrite($myPhpFile, $txtPhp);
        //fclose($myPhpFile);
		//Write JS
		fwrite($myJsFile, $txtJs);
        fclose($myJsFile);
	}
	
	//Effect in contact form site and client site
	public function genClientLanguage(){
		$this->genAdminLanguage();
		$path = Yii::app()->getBasePath().'/../client/template/core_render.js';
		//Get current content
		$content = file_get_contents($path);
		//Open file
		$myfile = fopen($path, "w") or die("Unable to open file!");
		//Get content language
		$contentLanguage = file_get_contents(Yii::app()->getBasePath().'/../library/js/classic/form_messages.js');
		// Add language to content
		$content .= "\n".$contentLanguage;
		if( isset(Yii::app()->params->minify) && Yii::app()->params->minify){
			//Minify js
			$content = Minifier::minify($content);
		}
		//Write file
		fwrite($myfile, $content);
		fclose($myfile);
	}
}
