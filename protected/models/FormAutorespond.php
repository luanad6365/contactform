<?php

class FormAutorespond extends CFormModel
{
    public $autorespondMessage;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('autorespondMessage ', 'required'),
            array('autorespondMessage', 'length', 'max' => 2000)
        );
    }



}
