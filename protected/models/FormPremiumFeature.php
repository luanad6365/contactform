<?php

/**
 * This is the model class for table "form_premium_feature".
 *
 * The followings are the available columns in table 'form_premium_feature':
 * @property integer $id
 * @property integer $user_id
 * @property integer $form_id
 * @property string $feature_name
 * @property integer $is_enable
 * @property string $meta_feature
 * @property string $created
 * @property string $updated
 */
class FormPremiumFeature extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'form_premium_feature';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, form_id, is_enable', 'numerical', 'integerOnly'=>true),
			array('feature_name', 'length', 'max'=>255),
			array('meta_feature, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, form_id, feature_name, is_enable, meta_feature, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'form_id' => 'Form',
			'feature_name' => 'Feature Name',
			'is_enable' => 'Is Enable',
			'meta_feature' => 'Meta Feature',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('form_id',$this->form_id);
		$criteria->compare('feature_name',$this->feature_name,true);
		$criteria->compare('is_enable',$this->is_enable);
		$criteria->compare('meta_feature',$this->meta_feature,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormPremiumFeature the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
