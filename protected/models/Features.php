<?php

/**
 * This is the model class for table "features".
 *
 * The followings are the available columns in table 'features':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property integer $order_position
 * @property integer $show
 */
class Features extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'features';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('name, description', 'required'),
			array('order_position, show', 'numerical', 'integerOnly'=>true),
			array('name, image', 'length', 'max'=>255),
			array('description', 'safe'),
            array('image', 'required', 'on' => 'insert'),
			array('image', 'file','types'=>'jpg, gif, png, jpeg', 'maxSize'=>1024 * 1024 * 5, 'tooLarge'=>'File has to be smaller than 5MB', 'allowEmpty'=>true, 'on'=>'update'),
            array('image', 'file','types'=>'jpg, gif, png, jpeg', 'maxSize'=>1024 * 1024 * 5, 'tooLarge'=>'File has to be smaller than 5MB', 'allowEmpty'=>false, 'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, image, order_position, show', 'safe', 'on'=>'search'),
		);
	}
	
//	public function image_required($attribute, $params) {
//		
//		if (empty($this->image)) {
//			$this->addError($attribute, Yii::t('app', "$attribute cannot be blank"));
//			return false;
//		}
//		return true;
//	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'image' => 'Image',
			'order_position' => 'Order Position',
			'show' => 'Show',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('order_position',$this->order_position);
		$criteria->compare('show',$this->show);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>  CfConst::ADMIN_PAGE_SIZE)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Features the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
