<?php

class FormDepartment extends CFormModel
{
    public $departmentName;
    public $departmentEmail;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('departmentName, departmentEmail ', 'required'),
            array('departmentName, departmentEmail', 'length', 'max' => 255),
            array('departmentEmail', 'email')
        );
    }



}
