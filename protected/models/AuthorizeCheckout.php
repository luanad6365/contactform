<?php

class AuthorizeCheckout extends CFormModel
{
    public $paymentType;
    public $firstName;
    public $lastName;
    public $country;
    public $state;
    public $city;
    public $address;
    public $zip;
    public $cardNumber;
    public $expiredMonth;
    public $expiredYear;
    public $cvvCode;
    
    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('paymentType, cardNumber, expiredMonth, expiredYear, cvvCode', 'safe'),
            array('firstName, lastName, country, state, city, address, zip', 'safe'),
            array('paymentType, cardNumber, expiredMonth, expiredYear, cvvCode', 'required'),
            array('firstName, lastName, country, state, city, address, zip', 'required'),
            array('cardNumber, expiredMonth, expiredYear, cvvCode, zip', 'numerical', 'integerOnly' => true),
            array('paymentType, cardNumber, firstName, lastName, state, city, address, zip', 'length', 'max' => 255),
            array('expiredMonth', 'length', 'max' => 2),
            array('expiredYear, cvvCode, country', 'length', 'max' => 5),
            //array('expiredMonth, expiredYear', 'validate_card_expiration'),
        );
    }
    
    
    
}
