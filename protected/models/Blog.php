<?php

/**
 * This is the model class for table "blog".
 *
 * The followings are the available columns in table 'blog':
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $description
 * @property string $content
 * @property string $author
 * @property integer $author_id
 * @property integer $category_id
 * @property string $tag
 * @property integer $show
 * @property string $url_slug
 * @property string $publish_date
 * @property string $created
 * @property string $updated
 */
class Blog extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'blog';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, content, author, author_id, category_id, publish_date', 'required'),
			array('category_id, show, author_id', 'numerical', 'integerOnly' => true),
			array('title, image, description, author, tag, url_slug', 'length', 'max' => 255),
			array('content, publish_date, created, updated', 'safe'),
			array('image', 'file', 'types' => 'jpg, gif, png, jpeg', 'maxSize' => 1024 * 1024 * 5, 'tooLarge' => 'File has to be smaller than 5MB', 'allowEmpty' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, image, description, content, author, author_id, category_id, tag, show, url_slug. publish_date, created, updated', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'image' => 'Image',
			'description' => 'Description',
			'content' => 'Content',
			'author' => 'Author',
			'author_id' => 'Author',
			'category_id' => 'Category',
			'tag' => 'Tag',
			'show' => 'Show',
			'url_slug' => 'Url Slug',
			'publish_date' => 'Publish Date',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('image', $this->image, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('content', $this->content, true);
//		if ($this->author != 'All') {
//			$criteria->compare('author', $this->author, true);
//		}
		$criteria->compare('author', $this->author, true);
		$criteria->compare('author_id', $this->author_id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('tag', $this->tag, true);
		$criteria->compare('show', $this->show);
		$criteria->compare('url_slug', $this->url_slug, true);
		$criteria->compare('publish_date', $this->publish_date, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);


		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => CfConst::ADMIN_PAGE_SIZE),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Blog the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	//Get all author
	public function getAuthor() {
		$criteria = new CDbCriteria;
		$criteria->select = array(
			't.author',
		);
		$criteria->distinct = true;
		return $this->findAll($criteria);
//		return new CActiveDataProvider($this, array(
//			'criteria' => $criteria,
//		));
	}

	public function behaviors() {
		return array(
			'slugmaker' => array(
				'class' => 'PcSimpleSlugBehavior',
			// 'sourceIdAttr' => the 'id' attribute name in this model. Default value = 'id'
			// 'sourceStringPrepareMethod' => If defined, will be used to get the base slug string text. Use it if you need some small manipulation of data in the model object in order to 'get' the base slug string. For example, if you need to concatenate 'first_name' and 'last_name' fields together.
			// 'sourceStringAttr' => the 'main string attribute' from which the slug will be built. Typically 'title', which is the default as well.
			// 'maxChars' => Maximum allowed slug total length. resulted slug will be trimmed to this value if longer than this value (*with* the prepended 'id-'...)
			// 'avoidIdPrefixing' => Setting this to true (default = false) will enable you to generate and parse URLs without the ID of the model record. WARNING: you need to carefully think before choosing this method as if not well thought, it could be used in an environment where there could be two or more records with same slug! 
			)
		);
	}

}
