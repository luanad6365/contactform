<?php

/**
 * This is the model class for table "user_form".
 *
 * The followings are the available columns in table 'user_form':
 * @property integer $id
 * @property integer $user_id
 * @property string $recipient_email
 * @property string $form_style
 * @property string $form_language
 * @property string $meta_form
 * @property integer $submit_count
 * @property string $form_status
 * @property string $avatar
 * @property string $created
 * @property string $updated
 * @property string $payment_type
 * @property integer $free_period
 * @property string $expired_date
 * @property integer $expired_time
 * @property integer $notice_before_day
 * @property string $sent_email_type
 */
class UserForm extends CActiveRecord {

    //Use for cron jon Reminder
    public $email_account;
     /**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'user_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('user_id, submit_count, free_period, expired_time, notice_before_day', 'numerical', 'integerOnly' => true),
            array('recipient_email, form_language, form_status, avatar', 'length', 'max' => 255),
            array('form_style, payment_type', 'length', 'max' => 50),
            array('meta_form, created, updated, expired_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, recipient_email, form_style, form_language, meta_form, submit_count, form_status, avatar, created, updated, payment_type, free_period, expired_date, expired_time, notice_before_day, sent_email_type', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user_info' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'user_id' => 'Owner User',
			'recipient_email' => 'Recipient Email',
			'form_style' => 'Form Style',
			'form_language' => 'Form Language',
			'meta_form' => 'Meta Form',
			'submit_count' => 'Submit Count',
			'form_status' => 'Form Status',
            'avatar' => 'Avatar',
			'created' => 'Created',
			'updated' => 'Updated',
            'payment_type' => 'Payment Type',
            'free_period' => 'Free Period',
            'expired_date' => 'Expired Date',
            'expired_time' => 'Expired Time',
            'notice_before_day' => 'Notice Before Day',
            'sent_email_type' => 'Sent Email Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->with = array('user_info');
		$criteria->compare('id', $this->id);
		//User user_id like search email in criterial, set true for search with string
		$criteria->compare('user.email', $this->user_id, true);
		$criteria->compare('recipient_email', $this->recipient_email, true);
		$criteria->compare('form_style', $this->form_style, true);
		$criteria->compare('form_language', $this->form_language, true);
		$criteria->compare('meta_form', $this->meta_form, true);
		$criteria->compare('submit_count', $this->submit_count);
		$criteria->compare('form_status', $this->form_status);
//		if ($this->created) {
//			// - 1 second because if not, create at will be + 1 day
//			$criteria->addBetweenCondition('UNIX_TIMESTAMP(created)', strtotime($this->created), strtotime($this->created) + 24 * 60 * 60 - 1);
//		}
        $criteria->compare('created',$this->created,true);
        $criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('updated', $this->updated, true);
        $criteria->compare('payment_type',$this->payment_type,true);
        $criteria->compare('free_period',$this->free_period);
        $criteria->compare('expired_date',$this->expired_date,true);
        $criteria->compare('expired_time',$this->expired_time);
        $criteria->compare('notice_before_day',$this->notice_before_day);
        $criteria->compare('sent_email_type',$this->sent_email_type,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => CfConst::ADMIN_PAGE_SIZE),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserForm the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function searchByUser() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', Yii::app()->user->id);
		$criteria->compare('recipient_email', $this->recipient_email, true);
		$criteria->compare('form_style', $this->form_style, true);
		$criteria->compare('meta_form', $this->meta_form, true);
		$criteria->compare('submit_count', $this->submit_count);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	//Delete all form
    //$mode = cronjob => no need to checkOwnResource 
	public function completelyDelete($act = 'delete', $mode = '') {
        //Form expired cant use this function: enable or disable
        if($act != 'delete'){ // Thats mean call disable action
            // If form expire, form free or premium
            if($this->form_status == CfConst::CF_FORM_STATUS_DISABLE && $this->sent_email_type == CfConst::EMAIL_TYPE_DEACTIVATE){
                echo CJSON::encode(array('status'=>'form_expired', 'message' => 'Your form is expire. Please upgrade your form.'));
                Yii::app()->end();
            }
        }
		//Delete cant be enable, disable, delete
		if ($this->id && $this->form_status != CfConst::CF_FORM_STATUS_DELETE) {
			//Check if form is own by this use do delete
			$userOwnForm = true;
            if($mode != 'cronjob'){
                $userOwnForm = Common::checkOwnResource($this->user_id, false, false);
            }
			if ($userOwnForm) {
				if ($this->removeResource($act)) {
					if ($act == 'delete') {
						$this->form_status = CfConst::CF_FORM_STATUS_DELETE;
					} else {
						$this->form_status = CfConst::CF_FORM_STATUS_DISABLE;
					}
					$this->save();
                    echo CJSON::encode(array('status'=>'success', 'message' => ''));
				}
			}
		}
	}

	//Remove css file, and replace js file
	public function removeResource($act = 'delete') {
		if ($this->id) {
			$hashID = md5($this->id);
			//Delete css path if act = delete, if act = disable, no delete
			if ($act == 'delete') {
				$cssFilePath = Yii::app()->getBasePath() . '/../client/css/' . $hashID . '.css';
				if (file_exists($cssFilePath)) {
					@unlink($cssFilePath);
				}
			}

			//Change content js
			$jsFilePath = Yii::app()->getBasePath() . '/../client/js/' . $hashID . '.js';

			//generate js file for client
			$content = file_get_contents(Yii::app()->getBasePath() . '/../client/template/form_status_delete.js');
			$myfile = fopen($jsFilePath, "w") or die("Unable to open file!");
			fwrite($myfile, $content);
			fclose($myfile);
			return true;
		}
		return false;
	}
	
	//Enable form
    //$mode = upgrade : no need code check in first step
	public function completelyEnable($mode = '') {
        //Form expired cant use this function: enable or disable
        // If form expire, form free or premium
        if($this->form_status == CfConst::CF_FORM_STATUS_DISABLE && $this->sent_email_type == CfConst::EMAIL_TYPE_DEACTIVATE && $mode == ''){
            echo CJSON::encode(array('status'=>'form_expired', 'message' => 'Your form is expire. Please upgrade your form.'));
            Yii::app()->end();
        }
		//Delete cant be enable, disable, delete
		if ($this->id && $this->form_status != CfConst::CF_FORM_STATUS_DELETE) {
			//Check if form is own by this use do delete
			$userOwnForm = true;
			$userOwnForm = Common::checkOwnResource($this->user_id, false, false);
			if ($userOwnForm) {
				if ($this->restoreResource()) {
					if ($this->submit_count && $this->submit_count > 0) {
						$this->form_status = CfConst::CF_FORM_STATUS_ACTIVE;
					} else {
						$this->form_status = CfConst::CF_FORM_STATUS_INACTIVE;
					}
					$this->save();
                    //If mode == upgrade : no echo json because cant redicrect to config premium feature page
                    if( $mode == '' ){
                        echo CJSON::encode(array('status'=>'success', 'message' => ''));
                    }
                    
				}
			}
		}
	}
	
	//Restore css file, and replace js file
	public function restoreResource() {
		if ($this->id) {
			GenFile::genScript(json_decode($this->meta_form, true));
			return true;
		}
		return false;
	}

}
