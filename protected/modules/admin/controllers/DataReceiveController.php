<?php

class DataReceiveController extends BaseAdminController {

	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$this->pageName = 'View Detail Data';
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$this->loadModel($id)->completelyDelete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionDeletes() {
		if (isset($_POST['theIds'])) {
			$IDs = explode(',', $_POST['theIds']);
			foreach ($IDs as $id) {
				$this->loadModel($id)->completelyDelete();
			}
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('DataReceive');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$this->pageName = 'Manage Data Submit';
		$model = new DataReceive('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['DataReceive']))
			$model->attributes = $_GET['DataReceive'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DataReceive the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = DataReceive::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param DataReceive $model the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'data-receive-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	//Manage data submit via single form
	public function actionSingle($id){
		$this->pageName = 'Manage Data Submit via Form';
		$model = new DataReceive('search_single');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['DataReceive']))
			$model->attributes = $_GET['DataReceive'];

		$model->form_id = $id;
		$modelForm = UserForm::model()->findByPk($id);
		$this->render('single', array(
			'model' => $model,
			'modelForm' => $modelForm,
		));
	}

}
