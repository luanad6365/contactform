<?php

class ManageUserController extends BaseAdminController {

	public $defaultAction = 'admin';
	//public $layout='//layouts/column2';

	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$this->pageName = 'Manage Users';
		$model = new User('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['User'])){
			$model->attributes = $_GET['User'];
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView() {
		$model = $this->loadModel();
		$this->render('view', array(
			'model' => $model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$this->pageName = 'Create User';
		$model = new User;
		$profile = new Profile;
		$this->performAjaxValidation(array($model, $profile));
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			$model->activkey = Yii::app()->getModule('user')->encrypting(microtime() . $model->password);
			$model->username = $model->email;
			$profile->attributes = $_POST['Profile'];
			$profile->user_id = 0;
			if ($model->validate() && $profile->validate()) {
				$model->password = Yii::app()->getModule('user')->encrypting($model->password);
				//Clone from profile to users
				$model->firstname = $profile->firstname;
				$model->lastname = $profile->lastname;
				$model->role = $model->superuser ? CfConst::ROLE_ADMIN : CfConst::ROLE_MEMBER;

				if ($model->save()) {
					$profile->user_id = $model->id;
					$profile->save();
				}
				$this->redirect(array('admin'));
			} else{
				$profile->validate();
			}
				
		}

		$this->render('create', array(
			'model' => $model,
			'profile' => $profile,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate() {
		$this->pageName = 'Update User';
		$model = $this->loadModel();
		$profile = $model->profile;
		$this->performAjaxValidation(array($model, $profile));
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			$profile->attributes = $_POST['Profile'];

			if ($model->validate() && $profile->validate()) {
				$old_password = User::model()->notsafe()->findByPk($model->id);
				if ($old_password->password != $model->password) {
					$model->password = Yii::app()->getModule('user')->encrypting($model->password);
					$model->activkey = Yii::app()->getModule('user')->encrypting(microtime() . $model->password);
				}
				$model->firstname = $profile->firstname;
				$model->lastname = $profile->lastname;
				$model->save();
				$profile->save();
				$this->redirect(array('admin'));
			} else{
				$profile->validate();
			}
				
		}

		$this->render('update', array(
			'model' => $model,
			'profile' => $profile,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete() {
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$model = $this->loadModel();
			$profile = Profile::model()->findByPk($model->id);
			$profile->delete();
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_POST['ajax'])){
				$this->redirect(array('/manageUser/admin'));
			}
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	public function actionDeletes() {
		if (isset($_POST['theIds'])) {
			$IDs = explode(',', $_POST['theIds']);
			foreach ($IDs as $id) {
				$model = $this->loadModel($id);
				$profile = Profile::model()->findByPk($model->id);
				$profile->delete();
				$model->delete();
			}
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($validate) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
			echo CActiveForm::validate($validate);
			Yii::app()->end();
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel($id = null) {
		if ($id) {
			$this->_model = User::model()->notsafe()->findbyPk($id);
		}
		if ($this->_model === null) {
			if (isset($_GET['id'])) {
				$this->_model = User::model()->notsafe()->findbyPk($_GET['id']);
			}
			if ($this->_model === null) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		return $this->_model;
	}

}
