<?php

class PaymentConfigController extends BaseAdminController
{

    public function actionIndex()
    {
        $this->pageName = 'Payment Configuration';
        $model = PaymentConfig::model()->find();
        if (!isset($model)) {
            $model = new PaymentConfig();
            $model->created = date('Y-m-d H:i:s', time());
        }
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'payment-config-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['PaymentConfig'])) {
            $model->setAttributes($_POST['PaymentConfig']);
            $model->updated = date('Y-m-d H:i:s', time());
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Update successfully!");
            }
        }
        $this->render('index', array('model' => $model));
    }
}