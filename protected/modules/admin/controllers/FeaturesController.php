<?php

class FeaturesController extends BaseAdminController {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex() {
		$this->pageName = 'Features';
		$model = StaticPage::model()->findByAttributes(array('page_id' => 'features'));
		if (!isset($model)) {
			$model = new StaticPage();
		}
		$model->page_id = 'features';
		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'features-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['StaticPage'])) {
			$model->setAttributes($_POST['StaticPage']);
			$model->updated = date('Y-m-d H:i:s', time());
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Update successfully!");
			}
		}
		$this->render('index', array('model' => $model));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$this->pageName = 'Create Feature';
		$model = new Features;

		//Default always show in front end
		$model->show = 1;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['Features'])) {
			$model->attributes = $_POST['Features'];
			if (!$model->order_position) {
				$model->order_position = 0;
			}
			$model->image = CUploadedFile::getInstance($model, 'image');
			if ($model->validate()) {
				//Dont worry about duplicate upload image when validate model many time, because validate ajax frist
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_FEATURES);
				$model->image = $fileName;
			}
			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$this->pageName = 'Update Feature';
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['Features'])) {
			$model->attributes = $_POST['Features'];
			if (!$model->order_position) {
				$model->order_position = 0;
			}
			if ($model->validate()) {
				//Dont worry about duplicate upload image when validate model many time, because validate ajax frist
				//In edit action, put old image name to function to remove old image, if new image uploaded
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_FEATURES, $model->image);
				$model->image = $fileName;
			}
			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$model = $this->loadModel($id);
		//Remove image and thumb
		Uploader::removeImage(CfConst::IMG_DIR_FEATURES, $model->image);
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function actionDeletes() {
		if (isset($_POST['theIds'])) {
			$IDs = explode(',', $_POST['theIds']);
			foreach ($IDs as $id) {
				$model = $this->loadModel($id, 'Features');
				//Remove image and thumb
				Uploader::removeImage(CfConst::IMG_DIR_FEATURES, $model->image);
				$model->delete();
			}
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$this->pageName = 'Manage Features';
		$model = new Features('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Features'])) {
			$model->attributes = $_GET['Features'];
		}

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Features the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = Features::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Features $model the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'features-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
