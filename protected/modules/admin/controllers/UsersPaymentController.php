<?php

class UsersPaymentController extends BaseAdminController
{

    public $defaultAction = 'admin';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('UsersPayment');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->pageName = 'Manage Payment';
        $model = new UsersPayment('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['UsersPayment']))
            $model->attributes = $_GET['UsersPayment'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return UsersPayment the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = UsersPayment::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param UsersPayment $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-payment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
