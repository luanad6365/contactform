<?php

class BlogController extends BaseAdminController {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex() {
		$this->pageName = 'Blog';
		$model = StaticPage::model()->findByAttributes(array('page_id' => 'blog'));
		if (!isset($model)) {
			$model = new StaticPage();
		}
		$model->page_id = 'blog';
		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'blog-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['StaticPage'])) {
			$model->setAttributes($_POST['StaticPage']);
			$model->updated = date('Y-m-d H:i:s', time());
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Update successfully!");
			}
		}
		$this->render('index', array('model' => $model));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$this->pageName = 'Create Blog';
		$model = new Blog;
		//Default publish date
		$model->publish_date = date('Y-m-d');
		//Default always show in front end
		$model->show = 1;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['Blog'])) {
			$model->attributes = $_POST['Blog'];
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->created = date('Y-m-d H:i:s', time());
			$model->updated = date('Y-m-d H:i:s', time());
			//Author name and id
			$firstName = ( isset(Yii::app()->user->firstname) && Yii::app()->user->firstname ) ? Yii::app()->user->firstname : '';
			$lastName = ( isset(Yii::app()->user->lastname) && Yii::app()->user->lastname ) ? Yii::app()->user->lastname : '';
			$authorName = $firstName.' '.$lastName;
			$model->author = $authorName ? $authorName : 'No Name';
			$model->author_id = Yii::app()->user->id;
			if ($model->validate()) {
				//Dont worry about duplicate upload image when validate model many time, because validate ajax frist
				//Square image for recent post in frontend blog page
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_BLOG, false, 'image', array(CfConst::THUMB_IMG_DEFAULT_WIDTH, CfConst::THUMB_IMG_DEFAULT_WIDTH));
				$model->image = $fileName;
			}
			if ($model->save()) {
				//Update slug with new ID
				$model->url_slug = $model->generateUniqueSlug();
				$model->save();
				$this->redirect(array('admin'));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$this->pageName = 'Update Blog';
		$model = $this->loadModel($id);
		//Re-gen publish date
		$model->publish_date = date('Y-m-d', strtotime($model->publish_date));
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['Blog'])) {
			$model->attributes = $_POST['Blog'];
			//Update time
			$model->updated = date('Y-m-d H:i:s', time());

			//Remove image, only show option when edit and already upload image in create action
			if (isset($_POST['remove_old_image']) && $_POST['remove_old_image']) {
				Uploader::removeImage(CfConst::IMG_DIR_BLOG, $model->image);
				$model->image = '';
			}
			//Update slug
			$model->url_slug = $model->generateUniqueSlug();
			//Author name and id
			$firstName = ( isset(Yii::app()->user->firstname) && Yii::app()->user->firstname ) ? Yii::app()->user->firstname : '';
			$lastName = ( isset(Yii::app()->user->lastname) && Yii::app()->user->lastname ) ? Yii::app()->user->lastname : '';
			$authorName = $firstName.' '.$lastName;
			$model->author = $model->author ? $model->author : $authorName;
			$model->author_id = $model->author_id ? $model->author_id : Yii::app()->user->id;
			if ($model->validate()) {
				//Dont worry about duplicate upload image when validate model many time, because validate ajax frist
				//In edit action, put old image name to function to remove old image, if new image uploaded
				//Square image for recent post in frontend blog page
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_BLOG, $model->image, 'image', array(CfConst::THUMB_IMG_DEFAULT_WIDTH, CfConst::THUMB_IMG_DEFAULT_WIDTH));
				$model->image = $fileName;
			}
			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$model = $this->loadModel($id);
		//Remove image and thumb
		Uploader::removeImage(CfConst::IMG_DIR_BLOG, $model->image);
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function actionDeletes() {
		if (isset($_POST['theIds'])) {
			$IDs = explode(',', $_POST['theIds']);
			foreach ($IDs as $id) {
				$model = $this->loadModel($id, 'Blog');
				//Remove image and thumb
				Uploader::removeImage(CfConst::IMG_DIR_BLOG, $model->image);
				$model->delete();
			}
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$this->pageName = 'Manage Blogs';
		$model = new Blog('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Blog'])) {
			$model->attributes = $_GET['Blog'];
		}
		$this->render('admin', array(
			'model' => $model,
			'allCategory' => CHtml::listData(Category::model()->findAll(), 'id', 'name'),
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Blog the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = Blog::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Blog $model the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'blog-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
