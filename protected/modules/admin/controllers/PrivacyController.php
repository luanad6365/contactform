<?php

class PrivacyController extends BaseAdminController {

	public function actionIndex() {
		$this->pageName = 'Privacy Policy';
		$model = StaticPage::model()->findByAttributes(array('page_id' => 'privacy_policy'));
		if (!isset($model)) {
			$model = new StaticPage();
		}
		$model->page_id = 'privacy_policy';
		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'privacy-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['StaticPage'])) {
			$model->setAttributes($_POST['StaticPage']);
			$model->updated = date('Y-m-d H:i:s', time());
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Update successfully!");
			}
		}
		$this->render('index', array('model' => $model));
	}

}
