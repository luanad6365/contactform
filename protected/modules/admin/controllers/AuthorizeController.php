<?php

class AuthorizeController extends BaseAdminController
{

    public $defaultAction = 'sandbox';

    public function actionSandbox()
    {
        $this->pageName = 'Authorize.net Sandbox Account';
        $model = MerchantInfo::model()->findByAttributes(array('type' => CfConst::PAYMENT_ENV_SANDBOX));
        if (!isset($model)) {
            $model = new MerchantInfo();
            $model->created = date('Y-m-d H:i:s', time());
        }
        $model->type = CfConst::PAYMENT_ENV_SANDBOX;
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'merchant-info-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['MerchantInfo'])) {
            $model->setAttributes($_POST['MerchantInfo']);
            $model->updated = date('Y-m-d H:i:s', time());
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Update successfully!");
            }
        }
        $this->render('create', array('model' => $model));
    }

    public function actionLive()
    {
        $this->pageName = 'Authorize.net Live Account';
        $model = MerchantInfo::model()->findByAttributes(array('type' => CfConst::PAYMENT_ENV_LIVE));
        if (!isset($model)) {
            $model = new MerchantInfo();
            $model->created = date('Y-m-d H:i:s', time());
        }
        $model->type = CfConst::PAYMENT_ENV_LIVE;
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'merchant-info-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['MerchantInfo'])) {
            $model->setAttributes($_POST['MerchantInfo']);
            $model->updated = date('Y-m-d H:i:s', time());
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Update successfully!");
            }
        }
        $this->render('create', array('model' => $model));
    }
}