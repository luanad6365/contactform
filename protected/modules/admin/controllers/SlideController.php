<?php

class SlideController extends BaseAdminController {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex() {
		$this->pageName = 'Slide';
		$this->render('index', array());
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$this->pageName = 'Create Slide';
		$model = new Slide;

		//Default always show in front end
		$model->show = 1;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['Slide'])) {
			$model->attributes = $_POST['Slide'];
			if (!$model->order_position) {
				$model->order_position = 0;
			}
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->image_background = CUploadedFile::getInstance($model, 'image_background');
			if ($model->validate()) {
				//Dont worry about duplicate upload image when validate model many time, because validate ajax frist
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_SLIDE, false, 'image');
				$model->image = $fileName;
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_SLIDE, false, 'image_background');
				$model->image_background = $fileName;
			}
			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$this->pageName = 'Update Slide';
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['Slide'])) {
			$model->attributes = $_POST['Slide'];
			if (!$model->order_position) {
				$model->order_position = 0;
			}
			if ($model->validate()) {
				//Dont worry about duplicate upload image when validate model many time, because validate ajax frist
				//In edit action, put old image name to function to remove old image, if new image uploaded
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_SLIDE, $model->image, 'image');
				$model->image = $fileName;
				$fileName = Uploader::uploadImage($model, CfConst::IMG_DIR_SLIDE, $model->image_background, 'image_background');
				$model->image_background = $fileName;
			}
			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$model = $this->loadModel($id);
		//Remove image and thumb
		Uploader::removeImage(CfConst::IMG_DIR_SLIDE, $model->image);
		Uploader::removeImage(CfConst::IMG_DIR_SLIDE, $model->image_background);
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function actionDeletes() {
		if (isset($_POST['theIds'])) {
			$IDs = explode(',', $_POST['theIds']);
			foreach ($IDs as $id) {
				$model = $this->loadModel($id, 'Slide');
				//Remove image and thumb
				Uploader::removeImage(CfConst::IMG_DIR_SLIDE, $model->image);
				Uploader::removeImage(CfConst::IMG_DIR_SLIDE, $model->image_background);
				$model->delete();
			}
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$this->pageName = 'Manage Slides';
		$model = new Slide('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Slide']))
			$model->attributes = $_GET['Slide'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Slide the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = Slide::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Slide $model the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'slide-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
