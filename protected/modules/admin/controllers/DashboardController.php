<?php

class DashboardController extends BaseAdminController {

	public function actionIndex() {
		$this->pageName = 'Dashboard';
		$this->render('index');
	}

}
