<?php

class BaseAdminController extends Controller {

	public $layout='//layouts/admin';
	
	public $pageName = '';
	public function init() {
		if ( ! Yii::app()->getModule('user')->isAdmin()) {
			$this->redirect(Yii::app()->user->returnUrl);
		}
		parent::init();
	}
	
	  protected function beforeAction($action) {
//		if (Yii::app()->request->isAjaxRequest) {
//			Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//			Yii::app()->clientScript->scriptMap['jquery-2.0.0.js'] = false;
//			Yii::app()->clientScript->scriptMap['anything.js'] = false;
//		}
		//DISABLE JQUERY and PUT JS to Bottom in ADMIN
		Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_END;
		return parent::beforeAction($action);
	}

}
