<?php
/* @var $this DataReceiveController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Data Receives',
);

$this->menu=array(
	array('label'=>'Create DataReceive', 'url'=>array('create')),
	array('label'=>'Manage DataReceive', 'url'=>array('admin')),
);
?>

<h1>Data Receives</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
