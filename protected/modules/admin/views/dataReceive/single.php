<style>
	/*Margin for edit button*/
	.fa.fa-times, .fa.fa-ban{margin-right: 8px;}
</style>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#data-receive-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				List Data
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<div class="search-form col-lg-12">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
						'type'=>'single'
					)); ?>
					</div><!-- search-form -->	
					<div class="table-responsive col-lg-12">
						<div>
							<?php
							echo CHtml::ajaxLink(
								'Delete Data', $this->createUrl('dataReceive/deletes'), 
								array(
									'type' => 'POST',
									'data' => 'js:{theIds : $.fn.yiiGridView.getChecked("data-receive-grid","data-receive-check-boxes").toString()}',
									'success' => 'function( data )
										{
											$("#data-receive-grid").yiiGridView.update("data-receive-grid");
										}',
									'error' => 'function(data) { alert("error"); }',
								), 
								array(
									'confirm' => Yii::t('app', 'Are you sure you want to delete Data?'),
									'class' => 'ajaxDelete btn btn-danger',
								)
							);
							?>
							
							<?php
							echo CHtml::ajaxLink(
								'Enable Form', $this->createUrl('userForm/enable', array('id'=>$modelForm->id)), 
								array(
									'type' => 'POST',
									'data' => 'js:{theIds : $.fn.yiiGridView.getChecked("data-receive-grid","data-receive-check-boxes").toString()}',
									'success' => 'function( data )
										{
											$("#disable-form").removeClass("hide");
											$("#enable-form").addClass("hide");
										}',
									'error' => 'function(data) { alert("error"); }',
								), 
								array(
									'confirm' => Yii::t('app', 'Are you sure you want to enable this form? (Will show form in user site)'),
									'class' => 'btn btn-outline btn-primary',
									'id' => 'enable-form',
								)
							);
							?>
							
							<?php
							echo CHtml::ajaxLink(
								'Disable Form', $this->createUrl('userForm/disable', array('id'=>$modelForm->id)), 
								array(
									'type' => 'POST',
									'data' => 'js:{theIds : $.fn.yiiGridView.getChecked("data-receive-grid","data-receive-check-boxes").toString()}',
									'success' => 'function( data )
										{
											$("#enable-form").removeClass("hide");
											$("#disable-form").addClass("hide");
										}',
									'error' => 'function(data) { alert("error"); }',
								), 
								array(
									'confirm' => Yii::t('app', 'Are you sure you want to disable this form? (Will hide form in user site)'),
									'class' => 'btn btn-outline btn-danger',
									'id' => 'disable-form',
								)
							);
							?>
						</div>
						
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'data-receive-grid',
							'htmlOptions'=>array(
							),
							//'ajaxUpdate'=>false, 
							'enableSorting'=>true,
							'filterPosition'=>'none', // Hide filter
							'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
							'dataProvider'=>$model->search_single(),
							'filter'=>$model,
							'columns'=>array(
								array(
									'class'=>'CCheckBoxColumn',
									'header'=>'Selected',
									'id'=>'data-receive-check-boxes',
									'selectableRows' => 2,
									'checkBoxHtmlOptions'=>array('class'=>'data_receive_id'),//for js bottom of page
								),			
								/*
								'customer_phone',
								'customer_company',
								'customer_website',
								'customer_subject',
								*/
								array(
									'name' => 'id',
									'type' => 'raw',
									'value' => 'CHtml::link(CHtml::encode($data->id),array("dataReceive/view","id"=>$data->id), array("target"=>"_blank"))',
								),
//								array(
//									'name' => 'user_id',
//									'type' => 'raw',
//									'value'=>'CHtml::link(UHtml::markSearch($data->user_info,"email"), "mailto:".$data->user_info->email)',
//								),
//								array(
//									'name'=>'recipient_email',
//									'type'=>'raw',
//									'value'=>'CHtml::link(UHtml::markSearch($data,"recipient_email"), "mailto:".$data->recipient_email)',
//								),
								array(
									'name'=>'from_site',
									'type'=>'raw',
									'value'=>'CHtml::link(UHtml::markSearch($data,"from_site"), $data->from_site)',
								),
								array(
									'name'=>'customer_email',
									'type'=>'raw',
									'value'=>'CHtml::link(UHtml::markSearch($data,"customer_email"), "mailto:".$data->customer_email)',
								),
								'customer_name',
								'customer_message',
//								array(
//									'name' => 'form_style',
//									'value'=>function($data,$row){
//										$styles = CfConst::getAllFormStyle();
//										if ($data->form_style && isset($styles[$data->form_style])) {
//											return $styles[$data->form_style];
//										}
//										return '';
//									},
//								),
//								array(
//									'name' => 'form_language',
//									'value'=>function($data,$row){
//										$langs = CfConst::getAllLanguage();
//										if ($data->form_language && isset($langs[$data->form_language])) {
//											return $langs[$data->form_language];
//										}
//										return '';
//									},
//								),
								'create_at',
								array(
									'name' => 'data_status',
									'value'=>function($data,$row){
										$stt = CfConst::getAllDataStatus();
										if ($data->data_status && isset($stt[$data->data_status])) {
											return $stt[$data->data_status];
										}
										return '';
									},
								),
								array(
									'class'=>'CButtonColumn',
									'template'=>'{delete}{view}',
									'deleteButtonImageUrl'=>false,
									'updateButtonImageUrl'=>false,
									'viewButtonImageUrl'=>false,
									'buttons'=>array(
										'update' => array(
											'options'=>array('class'=>'fa fa-pencil', 'target'=>'_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upđate'),
											'label'=>'',
											'url'=>'Yii::app()->createUrl("form/editForm", array("id"=>$data->id))',
										),
										'delete' => array(
											'options'=>array('class'=>'fa fa-times', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
											'label'=>''
										),
										'view' => array(
											'options'=>array('class'=>'fa fa-search', 'target'=>'_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View Detail'),
											'label'=>'',
											//'url'=>'Yii::app()->createUrl("admin/dataReceive/single", array("id"=>$data->id))',
										),
									),
								),
							),
							'pager'=>array(
								'htmlOptions'=>array('class'=>'pagination'),
								'class'=>'CLinkPager',
								//'cssFile'=>false,
								//'firstPageLabel'=>'First',
								//'lastPageLabel'=>'Last',
								'nextPageLabel'=>'Next',
								'prevPageLabel'=>'Previous',
								'selectedPageCssClass'=>'active',
								'hiddenPageCssClass'=>'disabled',
								'header'=>'',
							),
						)); ?>
					</div>
					<!-- /.table-responsive -->
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<?php
	Yii::app()->clientScript->registerScript('check-form', "
		$('.ajaxDelete').click(function (){
			if($('.data_receive_id:checkbox:checked').length > 0){
				return true;
			}else {
				alert('You must select at least one item to do this action.');
				return false;
			}
		});
		return true;
	", CClientScript::POS_READY);
?>
