<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				View Data
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="table-responsive col-lg-6">					
						<?php $this->widget('zii.widgets.CDetailView', array(
							'data'=>$model,
							'itemCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
							'attributes'=>array(
								'id',
								array(
									'name' => 'user_id',
									'type' => 'raw',
									'value'=>CHtml::link(UHtml::markSearch($model->user_info,"email"), "mailto:".$model->user_info->email),
								),
								array(
									'name' => 'form_style',
									'value'=>function($model){
										$styles = CfConst::getAllFormStyle();
										if ($model->form_style && isset($styles[$model->form_style])) {
											return $styles[$model->form_style];
										}
										return '';
									},
								),
								array(
									'name' => 'form_language',
									'value'=>function($model){
										$langs = CfConst::getAllLanguage();
										if ($model->form_language && isset($langs[$model->form_language])) {
											return $langs[$model->form_language];
										}
										return '';
									},
								),
								array(
									'name' => 'recipient_email',
									'type' => 'raw',
									'value'=>CHtml::link(UHtml::markSearch($model,"recipient_email"), $model->recipient_email),
								),
								array(
									'name' => 'from_site',
									'type' => 'raw',
									'value'=>CHtml::link(UHtml::markSearch($model,"from_site"), $model->from_site),
								),
								array(
									'name' => 'customer_email',
									'type' => 'raw',
									'value'=>CHtml::link(UHtml::markSearch($model,"customer_email"), $model->customer_email),
								),
								'customer_name',
								'customer_phone',
								'customer_company',
								array(
									'name' => 'customer_website',
									'type' => 'raw',
									'value'=>CHtml::link(UHtml::markSearch($model,"customer_website"), $model->customer_website),
								),
								'customer_subject',
								'customer_message',
								'create_at',
							),
						)); ?>
					</div>
					<!-- /.col-lg-6 (nested) -->
				</div>
				<!-- /.row (nested) -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
	
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				Quick Reply
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">					
						
					</div>
					<!-- /.col-lg-6 (nested) -->
				</div>
				<!-- /.row (nested) -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
