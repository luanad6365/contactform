<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id'=>'user-form',
		'enableClientValidation'=>false,
		'enableAjaxValidation'=>true,
		'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnType'=>false,
			'validateOnChange'=>false,
			'successCssClass' => 'has-success',
			'errorCssClass' => 'has-error',
			'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
		),
		'htmlOptions' => array(
			'onSubmit' => '',//Define in ckeditor config.js
			'enctype'=>'multipart/form-data'
		),
	)); 
?>

	<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
		<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
	<?php endif;?>
			
	<?php echo $form->error($model,'email'); ?>
	<?php echo $form->error($model,'password'); ?>
	<?php echo $form->error($model,'superuser'); ?>
	<?php echo $form->error($model,'status'); ?>
	<?php
		$profileFields = $profile->getFields();
		if ($profileFields) {
			foreach ($profileFields as $field) {
				echo $form->error($profile, $field->varname);
			}
		}		
	?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('maxlength'=>128, 'class' => 'form-control')); ?>
	</div>
				
	<div class="form-group">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('maxlength'=>128, 'class' => 'form-control')); ?>
	</div>
			
	<div class="form-group">
		<?php echo $form->labelEx($model,'superuser'); ?>
		<?php echo $form->dropDownList($model,'superuser',User::itemAlias('AdminStatus'), array('class' => 'form-control')); ?>
	</div>
		
	<div class="form-group">		
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',User::itemAlias('UserStatus'), array('class' => 'form-control')); ?>
	</div>
		
	<?php
		if ($profileFields) :
			foreach ($profileFields as $field) :
	?>
	<div class="form-group">
		<?php  echo $form->labelEx($profile, $field->varname); ?>
		<?php
			if ($widgetEdit = $field->widgetEdit($profile)) {
				echo $widgetEdit;
			} elseif ($field->range) {
				echo $form->dropDownList($profile, $field->varname, Profile::range($field->range));
			} elseif ($field->field_type == "TEXT") {
				echo $form->textArea($profile, $field->varname, array('rows' => 6, 'cols' => 50));
			} else {
				echo $form->textField($profile, $field->varname, array('class'=>'form-control', 'maxlength' => (($field->field_size) ? $field->field_size : 255)));
			}
		?>
	</div>
	<?php
			endforeach;
		endif;
	?>

	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>

<?php $this->endWidget(); ?>

