<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
<!--		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon">Username</span>
				<?php // echo $form->textField($model,'username',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>-->
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon">Email</span>
				<?php echo $form->textField($model,'email',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon">Is Admin</span>
				<?php echo $form->dropDownList($model,'superuser',array_merge( array(''=>'List All Users'), $model->itemAlias('AdminStatus') ), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon">Status</span>
				<?php echo $form->dropDownList($model,'status', array_merge( array(''=>'All'), $model->itemAlias('UserStatus') ), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon">Registration date</span>
				<div class="input-group date" id="wrap_search_date">
					<?php echo $form->textField($model, 'create_at', array('maxlength' => 255, 'class' => 'form-control')); ?>
					<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		</div>
	</div>
<?php $this->endWidget(); ?>
<script>
$('#wrap_search_date').datepicker({
	format: 'yyyy-mm-dd',
	todayBtn: 'linked',
	clearBtn: true,
	autoclose: true,
	todayHighlight: true
});
</script>
