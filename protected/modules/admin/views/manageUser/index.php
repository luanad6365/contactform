<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});	
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('user-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				List Users
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<div class="search-form col-lg-12">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
					</div><!-- search-form -->	
					<div class="table-responsive col-lg-12">
						<div>
							<?php
							echo CHtml::link('Create new User',array('manageUser/create'), array('class'=>'btn btn-primary'));
							?>			
							<?php
							echo CHtml::ajaxLink(
								'Delete User(s)', $this->createUrl('manageUser/deletes'), 
								array(
									'type' => 'POST',
									'data' => 'js:{theIds : $.fn.yiiGridView.getChecked("user-grid","user-check-boxes").toString()}',
									'success' => 'function( data )
										{
											$("#user-grid").yiiGridView.update("user-grid");
										}',
									'error' => 'function(data) { alert("error"); }',
								), 
								array(
									'confirm' => Yii::t('app', 'Are you sure you want to delete User(s)?'),
									'class' => 'ajaxDelete btn btn-danger',
								)
							);
							?>
						</div>
						
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'user-grid',
							'htmlOptions'=>array(
							),
							//'ajaxUpdate'=>false, 
							'enableSorting'=>false,
							'filterPosition'=>'none', // Hide filter
							'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								array(
									'class'=>'CCheckBoxColumn',
									'header'=>'Selected',
									'id'=>'user-check-boxes',
									'selectableRows' => 2,
									'checkBoxHtmlOptions'=>array('class'=>'user_id'),//for js bottom of page
								),
								array(
									'name' => 'Id',
									'type'=>'raw',
									'value' => 'CHtml::link(CHtml::encode($data->id),array("manageUser/update","id"=>$data->id))',
								),
								array(
									'name' => 'Name',
									'type'=>'raw',
									'value' => '$data->firstname." ".$data->lastname',
								),
								array(
									'name'=>'email',
									'type'=>'raw',
									'value'=>'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
								),
								'create_at',
								'lastvisit_at',
								array(
									'name'=>'superuser',
									'value'=>'User::itemAlias("AdminStatus",$data->superuser)',
									'filter'=>User::itemAlias("AdminStatus"),
								),
								array(
									'name'=>'status',
									'value'=>'User::itemAlias("UserStatus",$data->status)',
									'filter' => User::itemAlias("UserStatus"),
								),
								array(
									'class'=>'CButtonColumn',
									'template'=>'{update}{delete}{view}',
									'deleteButtonImageUrl'=>false,
									'updateButtonImageUrl'=>false,
									'viewButtonImageUrl'=>false,
									'buttons'=>array(
										'update' => array(
											'options'=>array('class'=>'fa fa-pencil', 'target'=>'_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upđate'),
											'label'=>''
										),
										'delete' => array(
											'options'=>array('class'=>'fa fa-times', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
											'label'=>''
										),
										'view' => array(
											'options'=>array('class'=>'fa fa-search', 'target'=>'_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
											'label'=>''
										),  
									),
								),
							),
							'pager'=>array(
								'htmlOptions'=>array('class'=>'pagination'),
								'class'=>'CLinkPager',
								//'cssFile'=>false,
								//'firstPageLabel'=>'First',
								//'lastPageLabel'=>'Last',
								'nextPageLabel'=>'Next',
								'prevPageLabel'=>'Previous',
								'selectedPageCssClass'=>'active',
								'hiddenPageCssClass'=>'disabled',
								'header'=>'',
							),
						)); ?>
					</div>
					<!-- /.table-responsive -->
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<?php
	Yii::app()->clientScript->registerScript('check-form', "
		$('.ajaxDelete').click(function (){
			if($('.user_id:checkbox:checked').length > 0){
				return true;
			}else {
				alert('You must select at least one item to do this action.');
				return false;
			}
		});
		return true;
	", CClientScript::POS_READY);
?>
