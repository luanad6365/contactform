<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Update User
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<?php $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile)); ?>
					</div>
					<!-- /.col-lg-6 (nested) -->
				</div>
				<!-- /.row (nested) -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>