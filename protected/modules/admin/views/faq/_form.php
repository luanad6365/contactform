<script src="<?php echo REL_BASE_URL; ?>/ckeditor/ckeditor.js"></script>
<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id'=>'faq-form',
		'enableClientValidation'=>false,
		'enableAjaxValidation'=>true,
		'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnType'=>false,
			'validateOnChange'=>false,
			'successCssClass' => 'has-success',
			'errorCssClass' => 'has-error',
			'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
		),
		'htmlOptions' => array(
			'onSubmit' => 'saveCkEditor()',//Define in ckeditor config.js
		),
	)); 
?>

	<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
		<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
	<?php endif;?>
	<?php echo $form->error($model,'question'); ?>
	<?php echo $form->error($model,'answer'); ?>
	<?php echo $form->error($model,'order_position'); ?>
	<?php echo $form->error($model,'show'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'question'); ?>
		<?php echo $form->textField($model,'question',array('maxlength'=>500, 'class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'answer'); ?>
		<?php echo $form->textArea($model,'answer',array('class' => 'ckeditor')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'order_position'); ?>
		<?php echo $form->textField($model,'order_position', array('class' => 'form-control')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'show'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->checkBox($model, 'show'); ?>
			</label>
		</div>                                          
	</div>
		
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>

<?php $this->endWidget(); ?>

<script type="text/javascript">
//Override height
CKEDITOR.replace('Faq_answer', {
	height: 300,
});
</script>
		