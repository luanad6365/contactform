<?php
/* @var $this FaqController */
/* @var $model Faq */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group input-group">
				<span class="input-group-addon">Question</span>
				<?php echo $form->textField($model,'question',array('maxlength'=>500, 'class'=>'form-control')); ?>
			</div>
		</div>
		<div class="col-md-1">
			<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		</div>
	</div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->