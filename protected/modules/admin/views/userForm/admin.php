<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-form-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				List Forms
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<div class="search-form col-lg-12">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
					</div><!-- search-form -->	
					<div class="table-responsive col-lg-12">
						<div>
							<?php
							echo CHtml::link('Create new Form',array('/form/build'), array('class'=>'btn btn-primary', "target"=>"_blank"));
							?>	
							<?php
							echo CHtml::ajaxLink(
								'Delete Form(s)', $this->createUrl('userForm/deletes'), 
								array(
									'type' => 'POST',
									'data' => 'js:{theIds : $.fn.yiiGridView.getChecked("user-form-grid","user-form-check-boxes").toString()}',
									'success' => 'function( data )
										{
											$("#user-form-grid").yiiGridView.update("user-form-grid");
										}',
									'error' => 'function(data) { alert("error"); }',
								), 
								array(
									'confirm' => Yii::t('app', 'Are you sure you want to delete Form(s)?'),
									'class' => 'ajaxDelete btn btn-danger',
								)
							);
							?>
						</div>
						
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'user-form-grid',
							'htmlOptions'=>array(
							),
							//'ajaxUpdate'=>false, 
							'enableSorting'=>true,
							'filterPosition'=>'none', // Hide filter
							'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								array(
									'class'=>'CCheckBoxColumn',
									'header'=>'Selected',
									'id'=>'user-form-check-boxes',
									'selectableRows' => 2,
									'checkBoxHtmlOptions'=>array('class'=>'user_form_id'),//for js bottom of page
								),
								array(
									'name' => 'id',
									'type'=>'raw',
									'value' => 'CHtml::link(CHtml::encode($data->id),array("/form/editForm","id"=>$data->id), array("target"=>"_blank"))',
								),
								array(
									'name' => 'user_id',
									'type'=>'raw',
									'value' => '',
									'value'=>'CHtml::link(UHtml::markSearch($data->user_info,"email"), "mailto:".$data->user_info->email)',
								),
								array(
									'name'=>'recipient_email',
									'type'=>'raw',
									'value'=>'CHtml::link(UHtml::markSearch($data,"recipient_email"), "mailto:".$data->recipient_email)',
								),
								array(
									'name' => 'form_style',
									'value'=>function($data,$row){
										$styles = CfConst::getAllFormStyle();
										if ($data->form_style && isset($styles[$data->form_style])) {
											return $styles[$data->form_style];
										}
										return '';
									},
								),
								array(
									'name' => 'form_language',
									'value'=>function($data,$row){
										$langs = CfConst::getAllLanguage();
										if ($data->form_language && isset($langs[$data->form_language])) {
											return $langs[$data->form_language];
										}
										return '';
									},
								),
								'created',
								array(
									'name' => 'form_status',
									'value'=>function($data,$row){
										$stt = CfConst::getAllFormStatus();
										if ($data->form_status && isset($stt[$data->form_status])) {
											return $stt[$data->form_status];
										}
										return '';
									},
								),
//								array(
//									'name'=>'status',
//									'value'=>'User::itemAlias("UserStatus",$data->status)',
//									'filter' => User::itemAlias("UserStatus"),
//								),
								'submit_count',
								array(
									'class'=>'CButtonColumn',
									'template'=>'{update}{delete}{view}',
									'deleteButtonImageUrl'=>false,
									'updateButtonImageUrl'=>false,
									'viewButtonImageUrl'=>false,
									'buttons'=>array(
										'update' => array(
											'options'=>array('class'=>'fa fa-pencil', 'target'=>'_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upđate'),
											'label'=>'',
											'url'=>'Yii::app()->createUrl("form/editForm", array("id"=>$data->id))',
										),
										'delete' => array(
											'options'=>array('class'=>'fa fa-times', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
											'label'=>''
										),
//										'disable' => array(
//											'options'=>array('class'=>'fa fa-ban', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Disable', 'ajax' => array('confirm'=>'Yes or No','type' => 'post', 'url'=>'js:$(this).attr("href")', 'success' => 'js:function(data) { $.fn.yiiGridView.update("user-form-grid")}') ),
//											'label'=>'',
//											'url'=>'Yii::app()->createUrl("admin/userForm/disable", array("id"=>$data->id))',
//
//										),
										'view' => array(
											'options'=>array('class'=>'fa fa-search', 'target'=>'_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Explore'),
											'label'=>'',
											'url'=>'Yii::app()->createUrl("admin/dataReceive/single", array("id"=>$data->id))',
										),
									),
								),
							),
							'pager'=>array(
								'htmlOptions'=>array('class'=>'pagination'),
								'class'=>'CLinkPager',
								//'cssFile'=>false,
								//'firstPageLabel'=>'First',
								//'lastPageLabel'=>'Last',
								'nextPageLabel'=>'Next',
								'prevPageLabel'=>'Previous',
								'selectedPageCssClass'=>'active',
								'hiddenPageCssClass'=>'disabled',
								'header'=>'',
							),
						)); ?>
					</div>
					<!-- /.table-responsive -->
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<?php
	Yii::app()->clientScript->registerScript('check-form', "
		$('.ajaxDelete').click(function (){
			if($('.user_form_id:checkbox:checked').length > 0){
				return true;
			}else {
				alert('You must select at least one item to do this action.');
				return false;
			}
		});
		return true;
	", CClientScript::POS_READY);
?>