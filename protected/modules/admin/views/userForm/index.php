<?php
/* @var $this UserFormController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Forms',
);

$this->menu=array(
	array('label'=>'Create UserForm', 'url'=>array('create')),
	array('label'=>'Manage UserForm', 'url'=>array('admin')),
);
?>

<h1>User Forms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
