<?php
/* @var $this UserFormController */
/* @var $data UserForm */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recipient_email')); ?>:</b>
	<?php echo CHtml::encode($data->recipient_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('form_style')); ?>:</b>
	<?php echo CHtml::encode($data->form_style); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('form_language')); ?>:</b>
	<?php echo CHtml::encode($data->form_language); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_form')); ?>:</b>
	<?php echo CHtml::encode($data->meta_form); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('submit_count')); ?>:</b>
	<?php echo CHtml::encode($data->submit_count); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>