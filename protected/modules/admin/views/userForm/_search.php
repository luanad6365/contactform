<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="col-md-5">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'user_id'); ?></span>
				<?php echo $form->textField($model,'user_id',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'form_language'); ?></span>
				<?php echo $form->dropDownList($model,'form_language', array_merge( array(''=>'All'), CfConst::getAllLanguage()), array('class' => 'form-control') ); ?>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'form_style'); ?></span>
				<?php echo $form->dropDownList($model,'form_style', array_merge( array(''=>'All'), CfConst::getAllFormStyle()), array('class' => 'form-control') ); ?>
			</div>
		</div>
		
		<div class="col-md-1">
			<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		</div>
	</div>

	<div class="row">
		<div class="col-md-5">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'recipient_email'); ?></span>
				<?php echo $form->textField($model,'recipient_email',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'form_status'); ?></span>
				<?php echo $form->dropDownList($model,'form_status', array_merge( array(''=>'All'), CfConst::getAllFormStatus()), array('class' => 'form-control') ); ?>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'created'); ?></span>
				<div class="input-group date" id="wrap_search_date">
					<?php echo $form->textField($model, 'created', array('maxlength' => 255, 'class' => 'form-control')); ?>
					<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
				</div>
			</div>
		</div>
		<div class="col-md-1">
			<!--<button onclick="location.reload(false);" class="btn btn-default" type="button" title="Reset"><i class="fa fa-refresh" ></i></button>-->
		</div>
	</div>
<?php $this->endWidget(); ?>
<script>
$('#wrap_search_date').datepicker({
	format: 'yyyy-mm-dd',
	todayBtn: 'linked',
	clearBtn: true,
	autoclose: true,
	todayHighlight: true
});
</script>