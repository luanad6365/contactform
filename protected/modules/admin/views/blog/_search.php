<?php
/* @var $this BlogController */
/* @var $model Blog */
/* @var $form CActiveForm */
?>



<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon">Title</span>
				<?php echo $form->textField($model,'title',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon">Author</span>
				<?php //echo $form->dropDownList($model,'author', CHtml::listData(array_merge( array(''=> array('author'=>'', 'author'=>'All')), Blog::model()->getAuthor() ) , 'author', 'author'), array('class' => 'form-control')); ?>
				<?php echo $form->textField($model,'author',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon">Category</span>
				<?php echo $form->dropDownList($model,'category_id', CHtml::listData(array_merge( array(''=> array('id'=>'', 'name'=>'All')), Category::model()->findAll() ) , 'id', 'name'), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon">Publish Date</span>
				<div class="input-group date" id="wrap_search_date">
					<?php echo $form->textField($model, 'publish_date', array('maxlength' => 255, 'class' => 'form-control')); ?>
					<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
				</div>
			</div>
		</div>
		<div class="col-md-1">
			<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		</div>
	</div>
<?php $this->endWidget(); ?>
<script>
$('#wrap_search_date').datepicker({
	format: 'yyyy-mm-dd',
	todayBtn: 'linked',
	clearBtn: true,
	autoclose: true,
	todayHighlight: true
});
</script>