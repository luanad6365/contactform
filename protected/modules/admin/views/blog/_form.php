<script src="<?php echo REL_BASE_URL; ?>/ckeditor/ckeditor.js"></script>
<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id'=>'blog-form',
		'enableClientValidation'=>true,
		//'enableAjaxValidation'=>true, DOESNT NEED AJAX IN FORM HAS UPLOAD FILE FIELD
		'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnType'=>false,
			'validateOnChange'=>false,
			'successCssClass' => 'has-success',
			'errorCssClass' => 'has-error',
			'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
		),
		'htmlOptions' => array(
			'onSubmit' => 'saveCkEditor()',//Define in ckeditor config.js
			'enctype' => 'multipart/form-data', // For upload image
		),
	)); 
?>

	<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
		<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
	<?php endif;?>
	<?php echo $form->error($model,'title'); ?>
	<?php echo $form->error($model,'description'); ?>
	<?php echo $form->error($model,'image'); ?>
	<?php echo $form->error($model,'category_id'); ?>
	<?php echo $form->error($model,'publish_date'); ?>
	<?php echo $form->error($model,'content'); ?>
	<?php echo $form->error($model,'show'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('maxlength'=>255, 'class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'image'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->fileField($model, 'image'); ?> 
			</label>
		</div>
	</div>
		
	<div class="form-group">
		<?php if($model->isNewRecord!='1' && $model->image): ?>
			<?php echo CHtml::image(CfConst::IMG_DIR_BLOG.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$model->image,"image"); ?>
			&nbsp;Remove image&nbsp;<?php echo CHtml::checkBox('remove_old_image'); ?>
		<?php endif;?>
	</div>
				
	<div class="form-group">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model,'category_id', CHtml::listData(Category::model()->findAll(), 'id', 'name'), array('class' => 'form-control')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'publish_date'); ?>
		<div class="input-group date" id="wrap_publish_date">
			<?php echo $form->textField($model,'publish_date',array('maxlength'=>255, 'class' => 'form-control')); ?>
			<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		</div>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('maxlength'=>255, 'class' => 'form-control')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('class' => 'ckeditor')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'show'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->checkBox($model, 'show'); ?>
			</label>
		</div>    
	</div>
		
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>

<?php $this->endWidget(); ?>

<script type="text/javascript">
//Override height
CKEDITOR.replace('Blog_content', {
	height: 300,
});
$('#wrap_publish_date').datepicker({
	format: 'yyyy-mm-dd',
	todayBtn: 'linked',
	clearBtn: true,
	autoclose: true,
	todayHighlight: true
});
</script>