<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-payment-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				List Payments
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<div class="search-form col-lg-12">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
					</div><!-- search-form -->	
					<div class="table-responsive col-lg-12">
					
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'users-payment-grid',
							'htmlOptions'=>array(
							),
							//'ajaxUpdate'=>false, 
							'enableSorting'=>true,
							'filterPosition'=>'none', // Hide filter
							'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
                                'id',
								array(
									'name' => 'user_id',
									'type'=>'raw',
									'value' => '',
									'value'=>'CHtml::link(UHtml::markSearch($data->user_info,"email"), "mailto:".$data->user_info->email)',
								),
                                'transaction_id',
								array(
									'name' => 'payment_type',
									'value'=>function($data,$row){
                                        if($data->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_MONTHLY){
                                            return 'Premium monthly';
                                        } elseif ($data->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_YEARLY) {
                                            return 'Premium yearly';
                                        }
									},
								),
                                'amount',
                                'purchase_date',
                                'form_id',
								array(
									'name' => 'expired_date',
									'header'=>'Form Expired Date',
								),
                                array(
									'name' => 'new_expired_date',
									'header'=>'Form New Expired Date',
								),
							),
							'pager'=>array(
								'htmlOptions'=>array('class'=>'pagination'),
								'class'=>'CLinkPager',
								//'cssFile'=>false,
								//'firstPageLabel'=>'First',
								//'lastPageLabel'=>'Last',
								'nextPageLabel'=>'Next',
								'prevPageLabel'=>'Previous',
								'selectedPageCssClass'=>'active',
								'hiddenPageCssClass'=>'disabled',
								'header'=>'',
							),
						)); ?>
					</div>
					<!-- /.table-responsive -->
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
