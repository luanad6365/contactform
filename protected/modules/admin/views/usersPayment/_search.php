<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'user_id'); ?></span>
				<?php echo $form->textField($model,'user_id',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
        
		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'form_id'); ?></span>
				<?php echo $form->textField($model,'form_id',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
        

		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'payment_type'); ?></span>
				<?php echo $form->dropDownList($model,'payment_type', array_merge( array(''=>'All'), CfConst::getAllPaymentTypePremium()), array('class' => 'form-control') ); ?>
			</div>
		</div>
		
		

		<div class="col-md-2">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'transaction_id'); ?></span>
				<?php echo $form->textField($model,'transaction_id',array('maxlength'=>255, 'class'=>'form-control')); ?>
			</div>
		</div>
		
		
		<div class="col-md-3">
			<div class="form-group input-group">
				<span class="input-group-addon"><?php echo $form->label($model,'purchase_date'); ?></span>
				<div class="input-group date" id="wrap_search_date">
					<?php echo $form->textField($model, 'purchase_date', array('maxlength' => 255, 'class' => 'form-control')); ?>
					<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
				</div>
			</div>
		</div>
		<div class="col-md-1">
			<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		</div>
	</div>
<?php $this->endWidget(); ?>
<script>
$('#wrap_search_date').datepicker({
	format: 'yyyy-mm-dd',
	todayBtn: 'linked',
	clearBtn: true,
	autoclose: true,
	todayHighlight: true
});
</script>