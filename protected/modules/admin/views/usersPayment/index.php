<?php
/* @var $this UsersPaymentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users Payments',
);

$this->menu=array(
	array('label'=>'Create UsersPayment', 'url'=>array('create')),
	array('label'=>'Manage UsersPayment', 'url'=>array('admin')),
);
?>

<h1>Users Payments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
