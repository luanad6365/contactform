<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id'=>'slide-form',
		'enableClientValidation'=>true,
		//'enableAjaxValidation'=>true, DOESNT NEED AJAX IN FORM HAS UPLOAD FILE FIELD
		'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnType'=>false,
			'validateOnChange'=>false,
			'successCssClass' => 'has-success',
			'errorCssClass' => 'has-error',
			'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
		),
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data', // For upload image
		),
	)); 
?>
<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
		<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
	<?php endif;?>
	<?php echo $form->error($model,'image'); ?>
	<?php echo $form->error($model,'image_background'); ?>
	<?php echo $form->error($model,'order_position'); ?>
	<?php echo $form->error($model,'show'); ?>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'image'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->fileField($model, 'image'); ?> 
			</label>
		</div>
	</div>
		
	<div class="form-group">
		<?php if($model->isNewRecord!='1' && $model->image): ?>
			<?php echo CHtml::image(CfConst::IMG_DIR_SLIDE.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$model->image,"image"); ?>
		<?php endif;?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'image_background'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->fileField($model, 'image_background'); ?> 
			</label>
		</div>
	</div>
		
	<div class="form-group">
		<?php if($model->isNewRecord!='1' && $model->image_background): ?>
			<?php echo CHtml::image(CfConst::IMG_DIR_SLIDE.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$model->image_background,"image_background"); ?>
		<?php endif;?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'order_position'); ?>
		<?php echo $form->textField($model,'order_position', array('class' => 'form-control')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'show'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->checkBox($model, 'show'); ?>
			</label>
		</div>    
	</div>
		
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>

<?php $this->endWidget(); ?>