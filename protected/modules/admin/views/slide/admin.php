<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#slide-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				List Slide
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">			
					<div class="table-responsive col-lg-12">
						<div>
							<?php
							echo CHtml::link('Create new Slide',array('slide/create'), array('class'=>'btn btn-primary'));
							?>			
							<?php
							echo CHtml::ajaxLink(
								'Delete Slide(s)', $this->createUrl('slide/deletes'), 
								array(
									'type' => 'POST',
									'data' => 'js:{theIds : $.fn.yiiGridView.getChecked("slide-grid","slide-check-boxes").toString()}',
									'success' => 'function( data )
										{
											$("#slide-grid").yiiGridView.update("slide-grid");
										}',
									'error' => 'function(data) { alert("error"); }',
								), 
								array(
									'confirm' => Yii::t('app', 'Are you sure you want to delete Slide(s)?'),
									'class' => 'ajaxDelete btn btn-danger',
								)
							);
							?>
						</div>	
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'slide-grid',
							'htmlOptions'=>array(
							),
							//'ajaxUpdate'=>false, 
							'enableSorting'=>false,
							'filterPosition'=>'none', // Hide filter
							'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								array(
									'class'=>'CCheckBoxColumn',
									'header'=>'Selected',
									'id'=>'slide-check-boxes',
									'selectableRows' => 2,
									'checkBoxHtmlOptions'=>array('class'=>'slide_id'),//for js bottom of page
								),
								'id',
								array(
									'name' => 'image',
									'type' => 'html',
									'value' => 'CHtml::image(CfConst::IMG_DIR_SLIDE.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$data["image"],"image");',
								),
								array(
									'name' => 'image_background',
									'type' => 'html',
									'value' => 'CHtml::image(CfConst::IMG_DIR_SLIDE.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$data["image_background"],"image_background");',
								),
								'order_position',
								array(
									'name' => 'show',
									'value' => '$data["show"]==1?"Yes":"No"',
								),
								array(
									'class'=>'CButtonColumn',
									'template'=>'{update}{delete}',
									'deleteButtonImageUrl'=>false,
									'updateButtonImageUrl'=>false,
									'buttons'=>array(
										'update' => array(
											'options'=>array('class'=>'fa fa-pencil', 'target'=>'_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upđate'),
											'label'=>''
										),
										'delete' => array(
											'options'=>array('class'=>'fa fa-times', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
											'label'=>''
										),
									),
								),
							),
							'pager'=>array(
								'htmlOptions'=>array('class'=>'pagination'),
								'class'=>'CLinkPager',
								//'cssFile'=>false,
								//'firstPageLabel'=>'First',
								//'lastPageLabel'=>'Last',
								'nextPageLabel'=>'Next',
								'prevPageLabel'=>'Previous',
								'selectedPageCssClass'=>'active',
								'hiddenPageCssClass'=>'disabled',
								'header'=>'',
							),
						)); ?>
					</div>
					<!-- /.table-responsive -->
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<?php
	Yii::app()->clientScript->registerScript('check-form', "
		$('.ajaxDelete').click(function (){
			if($('.slide_id:checkbox:checked').length > 0){
				return true;
			}else {
				alert('You must select at least one item to do this action.');
				return false;
			}
		});
		return true;
	", CClientScript::POS_READY);
 ?>
