<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id'=>'payment-config-form',
		'enableClientValidation'=>false,
		'enableAjaxValidation'=>true,
		'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnType'=>false,
			'validateOnChange'=>false,
			'successCssClass' => 'has-success',
			'errorCssClass' => 'has-error',
			'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
		),
		'htmlOptions' => array(),
	));
?>

	<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
		<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
	<?php endif;?>
	<?php echo $form->error($model,'merchant_env'); ?>
    <?php echo $form->error($model,'authorize_md5_setting'); ?>
    <?php echo $form->error($model,'free_period'); ?>
    <?php echo $form->error($model,'monthly_premium_fee'); ?>
    <?php echo $form->error($model,'yearly_premium_fee'); ?>
    <?php echo $form->error($model,'notice_before_day'); ?>


	<div class="form-group">
		<?php echo $form->labelEx($model,'merchant_env'); ?>
        <?php echo $form->dropDownList($model,'merchant_env', CfConst::getAllEnv(), array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'authorize_md5_setting'); ?>
		<?php echo $form->textField($model,'authorize_md5_setting',array('maxlength'=>32, 'class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'free_period'); ?>
		<?php echo $form->textField($model,'free_period',array('maxlength'=>11, 'class' => 'form-control')); ?>
        <div class="text-muted"><em>When user create a free form, this filed decide how long does this form is free ?</em></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'monthly_premium_fee'); ?>
		<?php echo $form->textField($model,'monthly_premium_fee',array('maxlength'=>11, 'class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'yearly_premium_fee'); ?>
		<?php echo $form->textField($model,'yearly_premium_fee',array('maxlength'=>11, 'class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'notice_before_day'); ?>
		<?php echo $form->textField($model,'notice_before_day',array('maxlength'=>11, 'class' => 'form-control')); ?>
        <div class="text-muted"><em>How many day before end premium, user receive email notification ?, If not set, user didn't receive anything</em></div>
	</div>

    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>

<?php $this->endWidget(); ?>