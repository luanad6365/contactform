<script src="<?php echo REL_BASE_URL; ?>/ckeditor/ckeditor.js"></script>
<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id'=>'features-form',
		'enableClientValidation'=>true,
		//'enableAjaxValidation'=>true, DOESNT NEED AJAX IN FORM HAS UPLOAD FILE FIELD
		'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnType'=>false,
			'validateOnChange'=>false,
			'successCssClass' => 'has-success',
			'errorCssClass' => 'has-error',
			'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
		),
		'htmlOptions' => array(
			'onSubmit' => 'saveCkEditor()',//Define in ckeditor config.js
			'enctype' => 'multipart/form-data', // For upload image
		),
	)); 
?>

	<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
		<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
	<?php endif;?>
	<?php echo $form->error($model,'name'); ?>
	<?php echo $form->error($model,'description'); ?>
	<?php echo $form->error($model,'image'); ?>
	<?php echo $form->error($model,'order_position'); ?>
	<?php echo $form->error($model,'show'); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('maxlength'=>255, 'class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('class' => 'ckeditor')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'image'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->fileField($model, 'image'); ?> 
			</label>
		</div>
	</div>
		
	<div class="form-group">
		<?php if($model->isNewRecord!='1' && $model->image): ?>
			<?php echo CHtml::image(CfConst::IMG_DIR_FEATURES.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$model->image,"image"); ?>
		<?php endif;?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'order_position'); ?>
		<?php echo $form->textField($model,'order_position', array('class' => 'form-control')); ?>
	</div>
		
	<div class="form-group">
		<?php echo $form->labelEx($model,'show'); ?>
		<div class="checkbox" style="display: inline;">
			<label>
				<?php echo $form->checkBox($model, 'show'); ?>
			</label>
		</div>    
	</div>
		
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>

<?php $this->endWidget(); ?>

<script type="text/javascript">
//Override height
CKEDITOR.replace('Features_description', {
	height: 300,
});
</script>