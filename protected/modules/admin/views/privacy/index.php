<script src="<?php echo REL_BASE_URL; ?>/ckeditor/ckeditor.js"></script>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Update Content Privacy Policy
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<?php 
							$form = $this->beginWidget('CActiveForm', array(
								'id'=>'privacy-form',
								'enableClientValidation'=>false,
								'enableAjaxValidation'=>true,
								'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
								'clientOptions'=>array(
									'validateOnSubmit'=>true,
									'validateOnType'=>false,
									'validateOnChange'=>false,
									'successCssClass' => 'has-success',
									'errorCssClass' => 'has-error',
									'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
								),
								'htmlOptions' => array(
									'onSubmit' => 'saveCkEditor()',//Define in ckeditor config.js
								),
							)); 
						?>
							
							<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
								<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
							<?php endif;?>
							<?php echo $form->error($model, 'name'); ?>
							<?php echo $form->error($model, 'description'); ?>
							<?php echo $form->error($model, 'content'); ?>
							
							<div class="form-group">
								<?php echo $form->labelEx($model, 'name'); ?>
								<?php echo $form->textField($model, 'name', array('maxlength' => 255, 'class' => 'form-control')); ?>	
							</div>
						
							<div class="form-group">
								<?php echo $form->labelEx($model, 'description'); ?>
								<?php echo $form->textField($model, 'description', array('maxlength' => 1000, 'class' => 'form-control')); ?>
							</div>
						
							<div class="form-group">
								<?php echo $form->labelEx($model, 'content'); ?>
								<?php echo $form->textArea($model, 'content', array('class' => 'ckeditor')); ?>
							</div>
						
							<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>
						
						<?php $this->endWidget(); ?>
					</div>
					<!-- /.col-lg-6 (nested) -->
				</div>
				<!-- /.row (nested) -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>