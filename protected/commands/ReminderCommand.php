<?php
/**
 * Send remider for premium form before ... day form expired
 *
 * @author LuanAD
 */
class ReminderCommand extends CConsoleCommand
{

    //Send reminder to premium form before form expired
    public function run($args)
    {
        //Begin of today
        $startDayTime = strtotime( date('Y-m-d', time()) );
        //End of today
        $endDayTime = $startDayTime + 86400 - 1;
        $criteria = new CDbCriteria();
        $criteria->join = 'INNER JOIN users on users.id = t.user_id';
        $criteria->select = 't.*, users.email as email_account';
        //If form expired day 14, notice before 3 days, then day 11 will receive email notification
        $criteria->condition = 'expired_time - notice_before_day * 86400 >= :start AND expired_time - notice_before_day * 86400 < :end ';
        //Payment type must be premium
        $criteria->addCondition(' payment_type != :free_period');
        //Form not deleted
        $criteria->addCondition(' form_status != :form_status_delete');
        //Doesnt sent any email before
        $criteria->addCondition(' (sent_email_type != :email_notice AND sent_email_type != :email_deactivate) OR sent_email_type IS NULL');
        //Not contact form create by admin
        $criteria->addCondition(' user_id != 1');
        $criteria->params = array(
            ':end'=>$endDayTime, 
            ':start' => $startDayTime, 
            ':free_period' => CfConst::PAYMENT_TYPE_FREE_PERIOD,
            ':form_status_delete' => CfConst::CF_FORM_STATUS_DELETE,
            ':email_notice' => CfConst::EMAIL_TYPE_NOTICE,
            ':email_deactivate' => CfConst::EMAIL_TYPE_DEACTIVATE,
        );
        $data = UserForm::model()->findAll($criteria);
        
        //Send email (Need to write common send mail) and write log
        foreach ($data as $form){
            $recipientEmail = $form->email_account;
            $fromName = 'Contact Form Admin';
            $replyemail = 'noreply@contactform.com';
            $subject = 'Your contact form will expire soon';
            $params = array(
                'days' => $form->notice_before_day,
                'form' => $form,
            );
            //Cant not use view and layout like WebApp because Console App cant use CController::renderPartital()
            $fullView = 'reminderForm';
            $bodyEmail = $this->render($fullView, $params);
            //Specific function send mail for CConsole, with out render view, only set view already rendered before (in function above)
            $result = BaseEmail::sendHtmlEmailConsole($recipientEmail, $fromName, $replyemail, $subject, $params, $bodyEmail);
            if($result){
                $form->sent_email_type = CfConst::EMAIL_TYPE_NOTICE;
                $form->save();
            }
            //Write log file
            echo $form->email_account;
        }
        //Common::debug($data->toArray());
    }
    
    private function render($template, array $data = array())
    {
        $path = Yii::getPathOfAlias('application.templates.mailer.cronjob') . '/' . $template . '.php';
        if (!file_exists($path))
            throw new Exception('Template ' . $path . ' does not exist.');
        return $this->renderFile($path, $data, true);
    }

}