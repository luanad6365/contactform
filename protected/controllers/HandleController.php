<?php

class HandleController extends Controller {

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Max-Age: 3628800');
		header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
		$successSubmit = false;
		if (isset($_POST['data'])) {
			$data = Yii::app()->getRequest()->getPost('data');
			$data = json_decode($data);
			$model = new DataReceive();
			$model->from_site = $data->url;
			$model->form_id = isset($data->cf_demo_hidden_form_id) ? $data->cf_demo_hidden_form_id : '';
			//Check valid form id
			if (strpos($data->encodeForm, md5($model->form_id)) !== false) {
				$model->customer_name = isset($data->cf_demo_name) ? $data->cf_demo_name : '';
				$model->customer_email = isset($data->cf_demo_email) ? $data->cf_demo_email : '';
				$model->customer_phone = isset($data->cf_demo_phone) ? $data->cf_demo_phone : '';
				$model->customer_company = isset($data->cf_demo_company) ? $data->cf_demo_company : '';
				$model->customer_website = isset($data->cf_demo_website) ? $data->cf_demo_website : '';
				$model->customer_subject = isset($data->cf_demo_subject) ? $data->cf_demo_subject : '';
				$model->customer_message = isset($data->cf_demo_message) ? $data->cf_demo_message : '';
				if ($model->validate()) {
					// If have form id
					if ($model->form_id) {
						$userForm = UserForm::model()->findByPk($model->form_id);
						//If have form
						if ($userForm) {
							//Disable and delete status cant send mail
							if ($userForm->form_status != CfConst::CF_FORM_STATUS_DELETE && $userForm->form_status != CfConst::CF_FORM_STATUS_DISABLE) {
                                //Check if enable multi recipient function
                                if( isset($data->cf_selected_recipient) && $data->cf_selected_recipient){
                                    $multiRecipient = FormPremiumFeature::model()->findByAttributes(
                                        array(
                                            'form_id' => $userForm->id,
                                            'feature_name' => CfConst::PREMIUM_FEATURE_MULTI_DEPARTMENT,
                                            'is_enable' => 1,
                                        )
                                    );
                                    //If email in multi recipient is real, and not hacked by change js, html
                                    if( isset($multiRecipient->id) && $multiRecipient->id && strpos($multiRecipient->meta_feature, $data->cf_selected_recipient) !== false){
                                        $recipientEmail = $model->recipient_email = trim($data->cf_selected_recipient);
                                    } else {//Use default email
                                        $recipientEmail = $model->recipient_email = $userForm->recipient_email;
                                    }
                                } else { //Use default email
                                    $recipientEmail = $model->recipient_email = $userForm->recipient_email;
                                }
								$model->user_id = $userForm->user_id;
								$model->form_style = $userForm->form_style;
								$model->form_language = $userForm->form_language;
								//Send mail to user
								$fromName = 'Contact Form Admin';
								$replyemail = $model->customer_email ? $model->customer_email : 'noreply@contactform.com';
								$subject = Yii::t('_yii', 'New Message from Customer');
								$params = array(
									'customer_data' => $model->getAttributes(),
								);
								$view = 'sendInfo';
								$layout = 'main1';
								if (BaseEmail::sendHtmlEmail($recipientEmail, $fromName, $replyemail, $subject, $params, $view, $layout)){
									$model->data_status = CfConst::DATA_STATUS_RECEIVE;
								} else {
									$model->data_status = CfConst::DATA_STATUS_SENT;
								}
								$model->create_at = date('Y-m-d H:i:s', time());
								$model->save();
								//Update submit count of form
								$userForm->submit_count = $userForm->submit_count ? $userForm->submit_count : 0;
								$userForm->submit_count++;
								if (!$userForm->form_status || $userForm->form_status == CfConst::CF_FORM_STATUS_INACTIVE) {
									$userForm->form_status = CfConst::CF_FORM_STATUS_ACTIVE;
								}
								$userForm->save();
                                //Auto respond email, if form is premium, and active function autorespond, and form did not expired
                                $autoRespond = FormPremiumFeature::model()->findByAttributes(
                                    array(
                                        'form_id' => $userForm->id,
                                        'feature_name' => CfConst::PREMIUM_FEATURE_AUTORESPOND,
                                        'is_enable' => 1,
                                    )
                                );
                                //Doest need to check form expired or not, because if form expired, then status must be DISABLE
                                if( isset($autoRespond->id) && $autoRespond->id && $model->customer_email){
                                    $dataRespond = $autoRespond->meta_feature;
                                    if($dataRespond){
                                        $messageRespond = json_decode($dataRespond);
                                        $messageRespond = $messageRespond->autorespondMessage;
                                    } else {
                                        $messageRespond = CfConst::DEFAULT_RESPOND_MESSAGE;
                                    }
                                    //Send mess back to user
                                    $recipientEmail = $model->customer_email;
                                    $fromName = 'Contact Form Auto Reply';
                                    $replyemail = $userForm->recipient_email;
                                    $subject = 'Thank you for contact us.';
                                    $params = array(
                                        'message' => $messageRespond
                                    );
                                    $view = 'autoRespond';
                                    $layout = 'main1';
                                    BaseEmail::sendHtmlEmail($recipientEmail, $fromName, $replyemail, $subject, $params, $view, $layout);
                                }
							}
							$successSubmit = true;
						}
					}
				}
			}
		}
		if ($successSubmit == false) {
			echo json_encode(array('status' => 'error'));
		} else {
			echo json_encode(array('status' => 'success'));
		}
		die;
	}

}
