<?php

class FormLanguageController extends Controller
{
	public function init() {
		if ( ! Yii::app()->getModule('user')->isAdmin()) {
			$this->redirect(Yii::app()->user->returnUrl);
		}
		parent::init();
	}
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new FormLanguage;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FormLanguage']))
		{
			$model->attributes=$_POST['FormLanguage'];
			$existed_data = FormLanguage::model()->findByAttributes(array('language'=>$model->language, 'message_key'=>$model->message_key));
//			echo '<pre>';
//			print_r($existed_data->getAttributes());
//			die;
			if( isset($existed_data->id) && $existed_data->id){
				$model = $existed_data;
			}
			$model->attributes=$_POST['FormLanguage'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FormLanguage']))
		{
			//Only update message when update
			$model->message_value=$_POST['FormLanguage']['message_value'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('FormLanguage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		//DISABLE jquery core for fucking action
		Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		
		$model=new FormLanguage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FormLanguage']))
			$model->attributes=$_GET['FormLanguage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FormLanguage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FormLanguage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FormLanguage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='form-language-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionGenerate() {
		FormLanguage::model()->genAdminLanguage();
		$this->redirect(array('admin'));
	}
	
	public function actionGenerateClient() {
		FormLanguage::model()->genClientLanguage();
		$this->redirect(array('admin'));
	}
}
