<?php

class DiscussionController extends Controller {

	public function actionIndex() {
		$data = StaticPage::model()->findByAttributes(array('page_id' => 'discussion'));
		$this->render('index', array('data' => $data));
	}

}
