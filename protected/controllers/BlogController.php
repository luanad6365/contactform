<?php

class BlogController extends Controller {

	public $data;
	public $lastestPost;
	public $category;
	public $archive;

	public function init() {
		//Get header content of page
		$this->data = StaticPage::model()->findByAttributes(array('page_id' => 'blog'));

		//Get lastest 5 post
		$this->lastestPost = Blog::model()->findAll(array(
			'condition' => 't.show = 1 AND UNIX_TIMESTAMP(publish_date) <= :time',
			'params' => array(':time' => time()),
			'order' => 'publish_date DESC',
			'limit' => 5
		));

		//Get blog category
		$this->category = Category::model()->findAll();

		//Get archive last 5 month
		$this->archive = array(date('m / Y', time()) => strtotime("now"));

		for ($i = 1; $i <= 4; $i++) {
			$this->archive[date('m / Y', strtotime("-" . $i . " month"))] = strtotime("-" . $i . " month");
		}

		parent::init();
	}

	public function actionIndex() {
//		echo '<pre>';
//		print_r($_REQUEST);
//		die();
//		
//		echo Yii::app()->user->lastname;
//		$blog = new Blog;
//		$blog->id = 8;
//		$blog->title = 'how to make url slug like Quora does using Yii framework ?';
//		$blog->title = $blog->generateUniqueSlug();
//		print_r($blog->getAttributes());
		//print_r($blog->getIdFromSlug('8-how_to_make_url_slug'));
		//echo Url::url_slug('how to make url slug like Quora does using Yii framework ?');

//		echo Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
		
		
		$criteria = new CDbCriteria();
		$criteria->condition = 't.show = 1';
		$params = array();
		
		//Search by category
		$category = Yii::app()->request->getQuery('category', '');
		if($category){
			$cat = Category::model()->find( array('condition'=>'url_slug=:category','params'=>array(':category'=>$category)) );
			if(isset($cat->id) && $cat->id  ){
				$criteria->addCondition('category_id=:cat_id');
				$params[':cat_id'] = $cat->id;
			}
		}
		
		//Search by author name
		$author = Yii::app()->request->getQuery('author', '');
		if($author){
			$author = str_replace('-', ' ', $author);
			if($author ){
				$criteria->addCondition('author LIKE :author');
				$params[':author'] = $author;
			}
		}
		
		//Search by archive time
		$archive = Yii::app()->request->getQuery('archive', '');
		if($archive){
			$archive = str_replace('-', 'ctf', $archive);
			$archive = str_replace('_', 'ctf', $archive);
			if($archive){
				$arr = explode('ctf', $archive);
				if(count($arr) == 2 ){
					if( (int)($arr[0]) && (int)($arr[1]) && $arr[0] <= 12 && $arr[0]>=1  ){
						//Convert to time stamp
						$start = strtotime($arr[0].'/01/'.$arr[1]);
						//End month
						$end = strtotime($arr[0].'/'.date('t', $start).'/'.$arr[1]) + 24 * 60 *60 - 1;
						
						$criteria->addCondition('UNIX_TIMESTAMP(publish_date) >= :start');
						$criteria->addCondition('UNIX_TIMESTAMP(publish_date) <= :end');
						$params[':start'] = $start;
						$params[':end'] = $end;
					}
				}
				
			}
		}
		
		//Search by key word
		$search = Yii::app()->request->getQuery('search', '');
		if($search){
			$criteria->addSearchCondition('title',$search);
			$criteria->addSearchCondition('description', $search, true, 'OR');
		}
		
		if(!empty($params)){
			$criteria->params = $params;
		}
		
		$criteria->order = 'publish_date DESC';
		$dataProvider = $dataProvider = new CActiveDataProvider('Blog', array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => CfConst::FRONTEND_PAGE_SIZE)
		));
		$this->render('index', array(
			'dataProvider' => $dataProvider,
			'data' => $this->data,
			'lastestPost' => $this->lastestPost,
			'category' => $this->category,
			'archive' => $this->archive
		));
	}

	public function actionDetail() {
		//Get ID
		$id = Yii::app()->request->getQuery('id', 0);
		//Dont have id, request to blog page
		if (!$id) {
			$this->redirect(array('blog/'));
		}
		//Title
		$title = Yii::app()->request->getQuery('title', '');

		//Get detail blog post, show = 1 and published
		$dataDetail = Blog::model()->find(array(
			'condition' => 'id=:id AND t.show = 1 AND UNIX_TIMESTAMP(publish_date) <= :time AND url_slug = :url_slug',
			'params' => array(':id' => $id, ':time' => time(), ':url_slug' => $id . '-' . $title)
		));

		if (!isset($dataDetail->id)) {
			$this->redirect(array('blog/'));
		}

		$this->render('detail', array(
			'dataDetail' => $dataDetail,
			'data' => $this->data,
			'lastestPost' => $this->lastestPost,
			'category' => $this->category,
			'archive' => $this->archive
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	  public function filters()
	  {
	  // return the filter configuration for this controller, e.g.:
	  return array(
	  'inlineFilterName',
	  array(
	  'class'=>'path.to.FilterClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }

	  public function actions()
	  {
	  // return external action classes, e.g.:
	  return array(
	  'action1'=>'path.to.ActionClass',
	  'action2'=>array(
	  'class'=>'path.to.AnotherActionClass',
	  'propertyName'=>'propertyValue',
	  ),
	  );
	  }
	 */
}
