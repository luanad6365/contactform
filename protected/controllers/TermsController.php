<?php

class TermsController extends Controller {

	public function actionIndex() {
		$data = StaticPage::model()->findByAttributes(array('page_id' => 'terms_of_service'));
		$this->render('index', array('data' => $data));
	}
}
