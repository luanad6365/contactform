<?php

class UserFormController extends Controller {

	public $defaultAction = 'myForm';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('myForm', 'delete', 'enable', 'disable', 'upgrade'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete() {
		if (Yii::app()->request->isAjaxRequest) {
			$formId = Yii::app()->request->getPost('form_id', '');
			if ($formId) {
				$this->loadModel($formId)->completelyDelete();
			}
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UserForm the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = UserForm::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Manages all my form.
	 */
	public function actionMyForm() {
		//Redirect if it guest
		Common::checkLogin();		
		//DISABLE jquery core for fucking action
		Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		$criteria = new CDbCriteria();
		$criteria->condition = 't.form_status != :form_status and user_id = :user_id';

		$criteria->params = array(':form_status' => CfConst::CF_FORM_STATUS_DELETE, ':user_id' => Yii::app()->user->id);
		
//		$dataProvider = new CActiveDataProvider('UserForm', array(
//			'criteria' => $criteria,
//			'pagination' => false,
//		));
		
		$dataProvider = UserForm::model()->findAll($criteria);

		$this->render('myForm', array(
			'dataProvider' => $dataProvider,
		));
	}
	
	public function actionEnable(){
		if (Yii::app()->request->isAjaxRequest) {
			$formId = Yii::app()->request->getPost('form_id', '');
			if ($formId) {
				$this->loadModel($formId)->completelyEnable();
			}
		}
	}
	
	public function actionDisable(){
		if (Yii::app()->request->isAjaxRequest) {
			$formId = Yii::app()->request->getPost('form_id', '');
			if ($formId) {
				$this->loadModel($formId)->completelyDelete('disable');
			}
		}
	}

    public function actionUpgrade($id){
		//DISABLE jquery core for fucking action
		Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Redirect if it guest
		Common::checkLogin();
		$modelForm = UserForm::model()->findByPk($id);
		//Check exits form id
		if (!isset($modelForm->id) || !$modelForm->id || $modelForm->form_status == CfConst::CF_FORM_STATUS_DELETE) {
			$this->redirect(Yii::app()->getModule('user')->returnUrl);
		}
		//Check user own this form
		Common::checkOwnResource($modelForm->user_id, Yii::app()->getModule('user')->returnUrl);
        $formPlan = '';
        switch ($modelForm->payment_type) {
            case CfConst::PAYMENT_TYPE_FREE_PERIOD:
                $formPlan = 'Your form is in free plan';
                break;
            case CfConst::PAYMENT_TYPE_PREMIUM_MONTHLY:
                $formPlan = 'Your form is in monthly premium plan';
                break;
            default:
                $formPlan = 'Your form is in yearly premium plan';
                break;
        }
        $modelPayment = new UsersPayment();
        $authorizeCheckout = new AuthorizeCheckout();
        $authorizeCheckout->expiredMonth = date('m');
        $authorizeCheckout->expiredYear = date('Y');

		if (isset($_POST['AuthorizeCheckout'])) {
			$authorizeCheckout->attributes = $_POST['AuthorizeCheckout'];

			if ($authorizeCheckout->validate()) {
                $userData = User::model()->findByPk(Yii::app()->user->id);
                $paymentConfig = PaymentConfig::model()->find();
                if( isset($paymentConfig->id) && $paymentConfig->id){

                    if( $authorizeCheckout->paymentType == CfConst::PAYMENT_TYPE_PREMIUM_MONTHLY ){
                        $amount = $paymentConfig->monthly_premium_fee ? $paymentConfig->monthly_premium_fee : CfConst::DEFAULT_FEE_MONTHLY;
                    } else if( $authorizeCheckout->paymentType == CfConst::PAYMENT_TYPE_PREMIUM_YEARLY ){
                        $amount = $paymentConfig->yearly_premium_fee ? $paymentConfig->yearly_premium_fee : CfConst::DEFAULT_FEE_YEARLY;
                    }
                    if( isset($amount) ){
                        $data = array(
                            'amount' => $amount,
                            'card_num' => $authorizeCheckout->cardNumber,
                            'exp_date' => $authorizeCheckout->expiredMonth .'/'.$authorizeCheckout->expiredYear,
                            'first_name' => $authorizeCheckout->firstName,
                            'last_name' => $authorizeCheckout->lastName,
                            'address' => $authorizeCheckout->address,
                            'city' => $authorizeCheckout->city,
                            'state' => $authorizeCheckout->state,
                            'country' => $authorizeCheckout->country,
                            'zip' => $authorizeCheckout->zip,
                            'email' => $userData->email,
                            'card_code' => $authorizeCheckout->cvvCode,
                        );
                        $response  = CfPayment::getInstance()->processAuthorizeAIM($data);
                        if ($response->approved) {
                            //Save user payment
                            $userPayment = new UsersPayment();
                            $userPayment->transaction_id = $response->transaction_id;
                            $userPayment->user_id = $userData->id;
                            //Need more security for this step
                            $userPayment->form_id = $id;
                            $userPayment->payment_type = $authorizeCheckout->paymentType;
                            $userPayment->amount = $amount;
                            $userPayment->purchase_date = date('Y-m-d H:i:s', time());
                            if( $userPayment->save() ){
                                //Save more info fo user payment
                                //Old expired
                                $userPayment->expired_date = $modelForm->expired_date;          
                                //Update form info after payment
                                $modelForm->payment_type = $authorizeCheckout->paymentType;
                                $extraTime = ($modelForm->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_MONTHLY) ? '+1 month' : '+1 year';
                                $modelForm->expired_time = strtotime( date('Y-m-d H:i:s', $modelForm->expired_time) . ' ' . $extraTime );
                                $modelForm->expired_date = date('Y-m-d H:i:s', $modelForm->expired_time) ;
                                //New expired
                                $userPayment->new_expired_date = $modelForm->expired_date;
                                $modelForm->sent_email_type = CfConst::EMAIL_TYPE_NOTHING;
                                if( $modelForm->save() ){
                                    $userPayment->save();
                                    //Enable form
                                    $modelForm->completelyEnable('upgrade');
                                    //Redirect to page setup premium feature (multi department, auto responder)
                                    $this->redirect(array('form/premiumFeature/' . $id));
                                }
                            }
                        } else {
                            if ($response->response_reason_text == 'A duplicate transaction has been submitted.') {
                                $authorizeCheckout->addError('paymentType', 'A duplicate transaction has been submitted.');
                            } else {
                                $authorizeCheckout->addError('paymentType', $response->response_reason_text);
                            }
                        }
                    } else {
                        $authorizeCheckout->addError('paymentType', 'Amount is invalid');
                    }
                } else {
                    $authorizeCheckout->addError('paymentType', 'Payment Config is not set');
                }
			}
		}
        $this->render('upgrade',
            array(
                'modelForm' => $modelForm,
                'formPlan' => $formPlan,
                'modelPayment' => $modelPayment,
                'authorizeCheckout' => $authorizeCheckout,
            )
        );
    }
}
