<?php

class FeaturesController extends Controller {

	public function actionIndex() {
		$data = StaticPage::model()->findByAttributes(array('page_id' => 'features'));
		$listFeature = Features::model()->findAll(array('order' => 'order_position, id', 'condition' => 't.show = 1' ));
		$this->render('index', array('data' => $data, 'listFeature' => $listFeature));
	}
}
