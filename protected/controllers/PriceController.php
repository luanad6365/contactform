<?php

class PriceController extends Controller {

	public function actionIndex() {
        $paymentConfig = PaymentConfig::model()->find();
        $data = array();
        if( isset($paymentConfig->id) && $paymentConfig->id){
            $data['fee_monthly'] = $paymentConfig->monthly_premium_fee;
            $data['fee_yearly'] = $paymentConfig->yearly_premium_fee;
        } else {
            $data['fee_monthly'] = CfConst::DEFAULT_FEE_MONTHLY;
            $data['fee_yearly'] = CfConst::DEFAULT_FEE_YEARLY;
        }
		$this->render('index', $data);
	}
}
