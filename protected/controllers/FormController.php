<?php

class FormController extends Controller {

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionCheckImageUrl() {
		$data = Yii::app()->request->getParam('data', '');
		if (trim($data)) {
			$model = new BuildForm();
			if ($model->checkRemoteFile($data)) {
				$result = array('status' => 'success', 'img_src' => $data);
			} else {
				$result = array('status' => 'error', 'img_src' => '', 'message' => 'Image link does not valid');
			}
			echo json_encode($result);
		}
	}

	public function actionBuild() {
		$model = new BuildForm();

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'build-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		//default
		$model->form_language = CfConst::CF_LANG_ENGLISH;
		$model->button_position = CfConst::BTN_CONTACT_POS_LEFT;
		$model->form_style = CfConst::CF_STYLE_BASIC;
		$model->form_color = CfConst::CF_DEFAULT_COLOR;
		$model->has_name = '1';
		$model->has_email = '1';
		$model->has_message = '1';
		$model->extra_info = '';
//        $model->custom_logo_link = 'http://scriptbaker.com/wp-content/uploads/2013/05/yii-ajax-login-form.jpg';
		if (isset($_POST['BuildForm'])) {
			$model->setAttributes($_POST['BuildForm']);
			if ($model->validate()) {
				$modelForm = new UserForm();
				$modelForm->user_id = Yii::app()->user->id;
				$modelForm->recipient_email = $model->recipient_email;
				$modelForm->form_style = $model->form_style;
				$modelForm->avatar = CfConst::getFormAvatar($modelForm->form_style);
				$modelForm->form_language = $model->form_language;
				//Default : form is inactive, active when have submit form
				$modelForm->form_status = CfConst::CF_FORM_STATUS_INACTIVE;
				$modelForm->meta_form = json_encode($model->getAttributes());
                //Add default email type sent
                $modelForm->sent_email_type = CfConst::EMAIL_TYPE_NOTHING;
				$modelForm->created = date('Y-m-d H:i:s', time());
				$modelForm->updated = date('Y-m-d H:i:s', time());
                //Add payment type
                //By default, all form created in free type
                $modelForm->payment_type = CfConst::PAYMENT_TYPE_FREE_PERIOD;
                //Get payment config
                $paymentConfig = PaymentConfig::model()->find();
                //Add free period
                if ( isset($paymentConfig->free_period) && $paymentConfig->free_period) {
                    $modelForm->free_period = $paymentConfig->free_period;
                } else {
                    $modelForm->free_period = CfConst::DEFAULT_FREE_PERIOD;
                }
                //Add notice before day, this field only use in premium type
                if ( isset($paymentConfig->notice_before_day) && $paymentConfig->notice_before_day) {
                    $modelForm->notice_before_day = $paymentConfig->notice_before_day;
                }
                //Add expired time and date
                $modelForm->expired_time = time() + $modelForm->free_period * 86400;
                $modelForm->expired_date = date('Y-m-d H:i:s', $modelForm->expired_time);
				//Redirect if it guest
				Common::checkLogin();
				if ($modelForm->save()) {
					//Update model form id; IMPORTANT when enable, disable form
					$model->form_id = $modelForm->id;
					$modelForm->meta_form = json_encode($model->getAttributes());
					$modelForm->save();
					$client_id = GenFile::genScript($model->getAttributes());
					$content = file_get_contents(ABS_BASE_URL . '/client/template/return_client.html');
					$txt = str_replace('HASH_FORM_ID', $client_id, $content);
					//Replace FULL_DOMAIN to current domain
					$txt = str_replace('FULL_DOMAIN', ABS_BASE_URL, $txt);
					//Send mail to user
					$user = User::model()->findByPk($modelForm->user_id);
					$recipientEmail = $user->email;
					$fromName = 'Contact Form Admin';
					$replyemail = 'noreply@contactform.com';
					$subject = Yii::t('_yii', 'Your contact form code');
					$params = array(
						'code' => $txt,
					);
					$view = 'sendUserCode';
					$layout = 'main1';
					BaseEmail::sendHtmlEmail($recipientEmail, $fromName, $replyemail, $subject, $params, $view, $layout);
					$this->redirect(array('form/getCode/' . $model->form_id));
				}
			}
		}
		$this->render('build', array('model' => $model));
	}

	public function actionEditForm($id) {
		//Redirect if it guest
		Common::checkLogin();
		$modelForm = UserForm::model()->findByPk($id);
		//Check exits form id
		if (!isset($modelForm->id) || !$modelForm->id || $modelForm->form_status == CfConst::CF_FORM_STATUS_DELETE) {
			$this->redirect(Yii::app()->getModule('user')->returnUrl);
		}
		//Check user own this form
		Common::checkOwnResource($modelForm->user_id, Yii::app()->getModule('user')->returnUrl);
		$model = new BuildForm();
		//decode to array
		$val = json_decode($modelForm->meta_form, true);
		$model->setAttributes($val);
		$model->form_id = $modelForm->id;

		if (isset($_POST['ajax']) && $_POST['ajax'] === 'build-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['BuildForm'])) {
			$model->setAttributes($_POST['BuildForm']);
			if (!isset($_POST['BuildForm']['has_name'])) {
				$model->has_name = null;
			}
			if (!isset($_POST['BuildForm']['has_email'])) {
				$model->has_email = null;
			}
			if (!isset($_POST['BuildForm']['has_phone'])) {
				$model->has_phone = null;
			}
			if (!isset($_POST['BuildForm']['has_company'])) {
				$model->has_company = null;
			}
			if (!isset($_POST['BuildForm']['has_website'])) {
				$model->has_website = null;
			}
			if (!isset($_POST['BuildForm']['has_subject'])) {
				$model->has_subject = null;
			}

			if ($model->validate()) {
				//If admin edit, dont change user_id
				if (!Yii::app()->getModule('user')->isAdmin()) {
					$modelForm->user_id = Yii::app()->user->id;
				}
				$modelForm->recipient_email = $model->recipient_email;
				$modelForm->form_style = $model->form_style;
				$modelForm->avatar = CfConst::getFormAvatar($modelForm->form_style);
				$modelForm->meta_form = json_encode($model->getAttributes());
				$modelForm->form_language = $model->form_language;
				$modelForm->created = $modelForm->created ? $modelForm->created : date('Y-m-d H:i:s', time());
				$modelForm->updated = date('Y-m-d H:i:s', time());
//                //Update value if it null, only use if you dont want to remove old data
//                $modelForm->sent_email_type = $modelForm->sent_email_type ? $modelForm->sent_email_type : CfConst::EMAIL_TYPE_NOTHING;
//                //Add payment type
//                $modelForm->payment_type = $modelForm->payment_type ? $modelForm->payment_type : CfConst::PAYMENT_TYPE_FREE_PERIOD;
//                //Get payment config
//                $paymentConfig = PaymentConfig::model()->find();
//                //Add free period
//                if ( isset($paymentConfig->free_period) && $paymentConfig->free_period) {
//                    $freePeriod = $paymentConfig->free_period;
//                } else {
//                    $freePeriod = CfConst::DEFAULT_FREE_PERIOD;
//                }
//                $modelForm->free_period = $modelForm->free_period ? $modelForm->free_period : $freePeriod;
//                //Add notice before day, this field only use in premium type
//                if ( isset($paymentConfig->notice_before_day) && $paymentConfig->notice_before_day) {
//                    $modelForm->notice_before_day = $modelForm->notice_before_day ? $modelForm->notice_before_day : $paymentConfig->notice_before_day;
//                }
//                //Add expired time and date
//                $modelForm->expired_time = strtotime($modelForm->created) + $modelForm->free_period * 86400;
//                $modelForm->expired_date = date('Y-m-d H:i:s', $modelForm->expired_time);
				if ($modelForm->save()) {
                    //If form is disable, dont re-gen js
                    if($modelForm->form_status != CfConst::CF_FORM_STATUS_DISABLE){
                        $client_id = GenFile::genScript($model->getAttributes());
                    }
					$this->redirect(array('form/getCode/' . $model->form_id));
				}
			}
		}
        //Link to premium feature
        if($modelForm->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_MONTHLY || $modelForm->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_YEARLY){
            $linkPremium = '/form/premiumFeature/'.$modelForm->id;
        } else {
            $linkPremium = '';
        }
		$this->render('build', array('model' => $model, 'linkPremium' => $linkPremium));
	}

	public function actionGetCode($id) {
		//Redirect if it guest
		Common::checkLogin();
		$modelForm = UserForm::model()->findByPk($id);
		//Check exits form id
		if (!isset($modelForm->id) || !$modelForm->id || $modelForm->form_status == CfConst::CF_FORM_STATUS_DELETE) {
			$this->redirect(Yii::app()->getModule('user')->returnUrl);
		}
		//Check user own this form
		Common::checkOwnResource($modelForm->user_id, Yii::app()->getModule('user')->returnUrl);
		$client_id = md5($id);
		$content = file_get_contents(ABS_BASE_URL . '/client/template/return_client.html');
		$txt = str_replace('HASH_FORM_ID', $client_id, $content);
		//Replace FULL_DOMAIN to current domain
		$txt = str_replace('FULL_DOMAIN', ABS_BASE_URL, $txt);
		$this->render('getCode', array('data' => $txt));
	}

	public function actionTest() {
		$model = new Ckeditor();
		if (isset($_POST['Ckeditor'])) {
			$model->setAttributes($_POST['Ckeditor']);
			$model->save();
		}
		$this->render('test', array('model' => $model));
	}
	
	public function actionBuildBasic() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Redirect if it guest
		Common::checkLogin();
		$model = new BuildForm();
		$this->render('buildBasic', array('model' => $model));
	}

	public function actionBuildClassic() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Redirect if it guest
		Common::checkLogin();
		$model = new BuildForm();
		$this->render('buildClassic', array('model' => $model));
	}

	public function actionBuildNormal() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Redirect if it guest
		Common::checkLogin();
		$model = new BuildForm();
		$this->render('buildNormal', array('model' => $model));
	}

	public function actionBuildRestaurant() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Redirect if it guest
		Common::checkLogin();
		$model = new BuildForm();
		$this->render('buildRestaurant', array('model' => $model));
	}

	public function actionBuildChild() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Redirect if it guest
		Common::checkLogin();
		$model = new BuildForm();
		$this->render('buildChild', array('model' => $model));
	}

	public function actionBuildFreeStyle() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		//Redirect if it guest
		Common::checkLogin();
		$model = new BuildForm();
		$this->render('buildFreeStyle', array('model' => $model));
	}
    
    public function actionPremiumFeature($id){
        //DISABLE jquery core for fucking action
        //Yii::app()->clientScript->scriptMap['jquery.js'] = false;

		//Redirect if it guest
		Common::checkLogin();
		$modelForm = UserForm::model()->findByPk($id);
		//Check exits form id
		if (!isset($modelForm->id) || !$modelForm->id || $modelForm->form_status == CfConst::CF_FORM_STATUS_DELETE || $modelForm->payment_type == CfConst::PAYMENT_TYPE_FREE_PERIOD) {
			$this->redirect(Yii::app()->getModule('user')->returnUrl);
		}
		//Check user own this form
		Common::checkOwnResource($modelForm->user_id, Yii::app()->getModule('user')->returnUrl);

        //Get existed data multi department
        $featureMultiDept = FormPremiumFeature::model()->findByAttributes(
            array(
                'form_id'=>$modelForm->id,
                'user_id'=>$modelForm->user_id,
                'feature_name'=> CfConst::PREMIUM_FEATURE_MULTI_DEPARTMENT
            )
        );
        if( !isset($featureMultiDept->id)){
            $featureMultiDept = new FormPremiumFeature();
        }
        
        //Get existed data auto respond
        $featureAutoRespond = FormPremiumFeature::model()->findByAttributes(
            array(
                'form_id'=>$modelForm->id,
                'user_id'=>$modelForm->user_id,
                'feature_name'=> CfConst::PREMIUM_FEATURE_AUTORESPOND
            )
        );
        if( !isset($featureAutoRespond->id)){
            $featureAutoRespond = new FormPremiumFeature();
        }

        //Array contain error when submit
        $arrErr = array();

        //Department meta data
        $deptData = array();

        //Auto respond meta data
        $autoData = array();

        //Enable function multi department
        $enableMultiDept = 0;

        //Enable function auto respond
        $enableAutoRespond = 0;

        //Global variables for submit: multi dept block
        $validMultiForSubmit = false;
        //Global variables for submit: auto respond block
        $validAutoForSubmit = false;

        //Submit action
        //Process for multi department block
        //2 case: when enable block multi or uncheck checkbox
        if ( ( isset($_POST['department_name']) && isset($_POST['department_email']) ) ||  ( isset($_POST['hid_multi_department']) && $_POST['hid_multi_department'] == 'false' ) ) {
            
            $valid = true;

            if( isset( $_POST['enable_multi_department'] ) && $_POST['enable_multi_department'] == 1 ){
                $enableMultiDept = 1;
                //Use index store data, not $k, because $k is index generate by JS
                $index = 1;
                foreach ($_POST['department_name'] as $k => $val) {
                    $item = new FormDepartment();
                    $item->departmentName = $_POST['department_name'][$k];
                    $item->departmentEmail = $_POST['department_email'][$k];
                    $valid = $item->validate() && $valid;
                    if (!$valid) {
                        $arrErr['multi_department'][$index] = $item->getErrors();
                    } else {
                        $deptData[$index] = $item->getAttributes();
                    }
                    $index++;
                }
            }
            
            if ($valid && empty($arrErr)) {
                $validMultiForSubmit = true;
            }
        }

        //Process for auto respond block
        //2 case: when enable block auto or uncheck checkbox
        if (  isset($_POST['autorespond_message'])  ||  ( isset($_POST['hid_autorespond']) && $_POST['hid_autorespond'] == 'false' ) ) {

            $valid = true;

            if( isset( $_POST['enable_autorespond'] ) && $_POST['enable_autorespond'] == 1 ){
                $enableAutoRespond = 1;
                $item = new FormAutorespond();
                $item->autorespondMessage = $_POST['autorespond_message'];
                $valid = $item->validate();
                if (!$valid) {
                    $arrErr['autorespond'] = $item->getErrors();
                } else {
                    $autoData = $item->getAttributes();
                }
            }
            
            if ($valid && empty($arrErr)) {
                $validAutoForSubmit = true;
            }
        }

        //By this way, always create 2 record with empty data
        if ($validMultiForSubmit && $validAutoForSubmit && empty($arrErr)) {
            //Save data multi department
            $featureMultiDept->user_id = $modelForm->user_id;
            $featureMultiDept->form_id = $modelForm->id;
            $featureMultiDept->feature_name = CfConst::PREMIUM_FEATURE_MULTI_DEPARTMENT;
            $featureMultiDept->is_enable = $enableMultiDept;
            if($enableMultiDept == 0){
                $featureMultiDept->meta_feature = $featureMultiDept->meta_feature ? $featureMultiDept->meta_feature : json_encode($deptData);
            } else {
                $featureMultiDept->meta_feature = json_encode($deptData);
            }
            $featureMultiDept->created = $featureMultiDept->created ? $featureMultiDept->created : date('Y-m-d H:i:s', time());
            $featureMultiDept->updated = date('Y-m-d H:i:s', time());
            $featureMultiDept->save();

            //Save data auto respond
            $featureAutoRespond->user_id = $modelForm->user_id;
            $featureAutoRespond->form_id = $modelForm->id;
            $featureAutoRespond->feature_name = CfConst::PREMIUM_FEATURE_AUTORESPOND;
            $featureAutoRespond->is_enable = $enableAutoRespond;
            if($enableAutoRespond == 0){
                $featureAutoRespond->meta_feature = $featureAutoRespond->meta_feature ? $featureAutoRespond->meta_feature : json_encode($autoData);
            } else {
                $featureAutoRespond->meta_feature = json_encode($autoData);
            }
            $featureAutoRespond->created = $featureAutoRespond->created ? $featureAutoRespond->created : date('Y-m-d H:i:s', time());
            $featureAutoRespond->updated = date('Y-m-d H:i:s', time());
            $featureAutoRespond->save();

            //Re-gen script for multi department
            //If form is disable, dont re-gen js
            if($modelForm->form_status != CfConst::CF_FORM_STATUS_DISABLE){
                GenFile::genScript( json_decode( $modelForm->meta_form, true ) );
            }
        }
        
        //Data for edit action
        $dataDept = json_decode($featureMultiDept->meta_feature);
        $dataAuto = json_decode($featureAutoRespond->meta_feature);

        // displays the view to collect tabular input
        $this->render('premiumFeature', array(
            'enableMultiDept' => $featureMultiDept->is_enable,
            'enableAutoRespond' => $featureAutoRespond->is_enable,
            'dataDept' => $dataDept,
            'dataAuto' => $dataAuto,
            'arrErr' => $arrErr,
            )
        );
    }
}
