<?php

class OauthController extends Controller {
	
    public function actionIndex() {
        $provider = strtolower(Yii::app()->request->getQuery('provider'));
        // for now, only accept login from facebook and twitter, google
        if (!in_array($provider, array('facebook', 'twitter', 'google'))) {
            $provider = 'facebook';
        }
        try {
            $oauthProvider = Oauth::getInstance();
            if (isset($_GET['provider'])) { //First time
                $oauthProvider->authenticateSocial($provider);
                $userOauthProfile = $oauthProvider->userProfile;
            } else { //Redirect to base Url
                $oauthProvider->processEndpoint();
            }
        } catch (Exception $e) {
            // Hiding technical error message
            // Seem raise error code 5, close the poup immediately
            echo '<script>alert("Service error !!!");window.close();</script>';
            exit;
        }
		
        # 1 - check if user already have authenticated using this provider before
        $userOauth = UserOauth::model()->findByAttributes(
                array(
                    'provider' => $provider,
                    'identifier' => $userOauthProfile->identifier,
                )
        );
        # 2 - if authentication exists in the database, then we set the user as connected and redirect him to home page
        if ( is_object($userOauth) && $userOauth->identifier && $userOauth->user_id) {
            $userModel = User::model()->findByPk($userOauth->user_id);
            if ( is_object($userModel) && $userModel->status == User::STATUS_ACTIVE) {
                //Make user login to our system
                if ($oauthProvider->makeLogin($userOauth->identifier, $userModel->email)) {
                    $result = array(
                        'error' => 0,
                        'redirect' => $oauthProvider->getRedirect()
                    );
                } else {
                    $result = array(
                        'error' => 1,
                        'message' => 'Can not login.'
                    );
                }
            } else {
                $result = array(
                    'error' => 1,
                    'message' => 'Your account has been suspended or deleted.'
                );
            }
        } else {
            # 3 - else, here lets check if the user email we got from the provider already exists in our database ( for this example the email is UNIQUE for each user )
            // if authentication does not exist, but the email address returned  by the provider does exist in database,
            // then we tell the user that the email  is already in use
            // but, its up to you if you want to associate the authentication with the user having the adresse email in the database
            // Make temp username
            $userOauthProfile->username = $oauthProvider->generateUsername($userOauthProfile->firstName, $userOauthProfile->lastName);

            //Note Twitter API doesnot provide email
            if ($userOauthProfile->email) {
                $user = User::model()->findByAttributes(array('email' => $userOauthProfile->email));

                if ( is_object($user) && $user->id) {
                    // The user's email of $provider account has been registered.
                    $userOauthProfile->email = null;
                }
            }

            // Now we create a temp email
            if (!$userOauthProfile->email) {
                switch ($provider) {
                    case 'facebook':
                        $userOauthProfile->email = "{$userOauthProfile->username}@facebook.com";
                        break;
                    case 'twitter':
                        $userOauthProfile->email = "{$userOauthProfile->username}@twitter.com";
                        break;
                    case 'google':
                        $userOauthProfile->email = "{$userOauthProfile->username}@google.com";
                        break;
                }
            }

            # 4 - if authentication does not exist and email is not in use, then we create a new user
            // 4.1 - create new user
            $newUser = new User();
            //for now, we use email make username
            $newUser->username = $userOauthProfile->email;
            //general new pass for social user, use for action makeLogin after (pw = identifier + username(email) )
            $newUser->password = UserModule::encrypting($userOauthProfile->identifier.$newUser->username);
            $newUser->email = $userOauthProfile->email;
			$newUser->activkey = UserModule::encrypting(microtime().$newUser->password);
            $newUser->create_at = date('Y-m-d H:i:s',  time());
			$newUser->lastvisit_at = date('Y-m-d H:i:s',  time());
            $newUser->superuser = 0;
            $newUser->status = User::STATUS_ACTIVE;
            $newUser->social_connect = $provider;
            $newUser->firstname = $userOauthProfile->firstName;
            $newUser->lastname = $userOauthProfile->lastName;
            $newUser->role = CfConst::ROLE_MEMBER;

            //If use $newUser->save() -> doesnt work because capcha verified in rules()
            if ($newUser->insert()) {
				//Create user profile
				$profile = new Profile;
				$profile->regMode = true;
				$profile->firstname = $newUser->firstname;
				$profile->lastname = $newUser->lastname;
				$profile->user_id = $newUser->id;
				$profile->insert();
				
                // 4.2 - creat a new authentication for him
                $newUserOauth = new UserOauth();
                $newUserOauth->user_id = $newUser->id;
                $newUserOauth->provider = $provider;
                $newUserOauth->identifier = $userOauthProfile->identifier;
                $newUserOauth->profile_cache = json_encode($userOauthProfile);
                $newUserOauth->session_data = $oauthProvider->getSessionData();
                $newUserOauth->save();
                //Make user login to our system
                if ($oauthProvider->makeLogin($newUserOauth->identifier, $newUser->username)) {
                    $result = array(
                        'error' => 0,
                        'redirect' => $oauthProvider->getRedirect()
                    );
                } else {
                    $result = array(
                        'error' => 1,
                        'message' => 'Can not login.'
                    );
                }
            } else {
                $result = array(
                    'error' => 1,
                    'message' => 'Error when save user.'
                );
            }
        }
        $this->render('index', array('result' => $result));
    }
}
