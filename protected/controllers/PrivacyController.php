<?php

class PrivacyController extends Controller {

	public function actionIndex() {
		$data = StaticPage::model()->findByAttributes(array('page_id' => 'privacy_policy'));
		$this->render('index', array('data' => $data));
	}
}
