<?php

class DataReceiveController extends Controller {

	public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('view', 'delete', 'admin'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$model = $this->loadModel($id);
		//Check user own data
		Common::checkOwnResource($model->user_id, Yii::app()->getModule('user')->returnUrl);
		$this->render('view', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$this->loadModel($id)->completelyDelete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin($form_id) {
		$modelForm = UserForm::model()->findByPk($form_id);
		//Check exits form id
		if (!isset($modelForm->id) || !$modelForm->id || $modelForm->form_status == CfConst::CF_FORM_STATUS_DELETE) {
			$this->redirect(Yii::app()->getModule('user')->returnUrl);
		}
		//Check user own this form
		Common::checkOwnResource($modelForm->user_id, Yii::app()->getModule('user')->returnUrl);
		
		//DISABLE jquery core for fucking action
		Yii::app()->clientScript->scriptMap['jquery.js'] = false;
		$model = new DataReceive('search_single');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['DataReceive'])) {
			$model->attributes = $_GET['DataReceive'];
		}
		$model->form_id = $form_id;
		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DataReceive the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = DataReceive::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

}
