<?php

class UsersPaymentController extends Controller
{

    public $defaultAction = 'admin';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('view', 'delete', 'admin'),
				'users' => array('@'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        //DISABLE jquery core for fucking action
		Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $model = new UsersPayment('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['UsersPayment']))
            $model->attributes = $_GET['UsersPayment'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return UsersPayment the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = UsersPayment::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
