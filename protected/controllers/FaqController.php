<?php

class FaqController extends Controller {

	public function actionIndex() {
		$data = StaticPage::model()->findByAttributes(array('page_id' => 'faq'));
		$listFAQ = Faq::model()->findAll(array('order' => 'order_position, id', 'condition' => 't.show = 1' ));
		$this->render('index', array('data' => $data, 'listFAQ' => $listFAQ));
	}
}
