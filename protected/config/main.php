<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Contact Form',
	'theme'=>'classic',
//	'defaultController' => 'home',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.components.helpers.*',
        'application.components.helpers.utils.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'netviet'=>array(
			'test' => 'md5',
			'defaultController' => 'default',
		),
		'admin'=>array(
			'defaultController' => 'dashboard',
		),
        'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'md5',

            # send activation email
            'sendActivationMail' => false,

            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => true,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
//            'returnUrl' => array('/user/profile'),
            'returnUrl' => array('/userForm'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),
			
			#enable captcha for any page : true or false
			'captcha' => array('registration'=>false),
			
			# page after login ADMIN
            'returnAdminUrl' => array('/admin/dashboard'),
        ),		
	),
	
	'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.components.AppStartup'
        ),
    ),

	// application components
	'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
        ),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'caseSensitive' => false,
			'rules'=>array(
//				'admin'=>'other action to prevent access to admin page',
//				//For admin page
//				'netviet'=>'admin',
//				'netviet/<controller:\w+>'=>'admin/<controller>',
//				'netviet/<controller:\w+>/<action:\w+>'=>'admin/<controller>/<action>',
//				'netviet/<controller:\w+>/<action:\w+>/<id:\d+>'=>'admin/<controller>/<action>/<id:\d+>',	
				//BLOG
                'blog/<id:\d+>-<title:[\w\-]+>'=>'/blog/detail/<id:\d+>/<title:\w+>',
				'blog/category/<category:[\w\-]+>'=>'/blog/index/<category:\w+>',
				'blog/author/<author:[\w\-]+>'=>'/blog/index/<author:\w+>/',
				'blog/archive/<archive:[\w\-]+>'=>'/blog/index/<archive:\w+>/',
				'dataReceive/admin/<form_id:\d+>'=>'/dataReceive/admin/<form_id:\d+>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'clientScript' => array(
			//Doesnt need because user manually jquery
			'scriptMap' => array(
				//'jquery.js' => false,
			),
			//Put core script like jquery.yiiactiveform.js to bottom
			//'coreScriptPosition'=>CClientScript::POS_END
		),
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=contactform',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
            'tablePrefix' => '',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.templates.mailer',
            'pathLayouts' => 'application.templates.mailer.layouts'
        ),
        'phpThumb' => array(
            'class' => 'ext.EPhpThumb.EPhpThumb',
            'options' => array() //optional phpThumb specific options are added here
        ),
    ),
 // application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'env'=>'DEVELOPMENT', // (DEVELOPMENT: rebuild core.js from cf_render.js, PRODUCTION: none)
		'minify'=>true, //minify js, css before return client
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
        'smtp' => array(
            "Host" => 'smtp.gmail.com',
            "Username" => 'contactformmedia@gmail.com',
            "Password" => 'contactform2014',
            "From" => 'contactformmedia@gmail.com',
            "SMTPDebug" => 0,
            "SMTPAuth" => true,
            "SMTPSecure" => 'ssl',
            "Port" => 465,
            "FromName" => 'contactform.com',
        ),
		'oauth' => array(
            "base_url" => "http://" . $_SERVER['HTTP_HOST'] . "/oauth/index",
            "providers" => array(
                "Facebook" => array(
                    "enabled" => true,
                    "keys" => array("id" => "737567373013714", "secret" => "3c4235fa01a0c62f77a125ebb4eee743"),
                    "scope" => "email",
                    "display" => "popup"
                ),
                "Twitter" => array(
                    "enabled" => true,
                    "keys" => array("key" => "IngXJ4cF2QiXWQtMUzJLEsdT9", "secret" => "SqUbkhuMjpb00i2IYu0mkFI8m7E13kf48uD3ForjZaZpRMn277"),
                ),
                "Google" => array(
                    "enabled" => true,
                    "keys" => array("id" => "352556186762-ocf8i6ms70vcj3iunmkha5rpa89p4ieq.apps.googleusercontent.com", "secret" => "aNN2mcba0KQ_oB1Xa9tjt8cQ"),
                ),
            ),
            "debug_mode" => false,
            // to enable logging, set 'debug_mode' to true, then provide here a path of a writable file 
            "debug_file" => "",
        ),
	),
);