<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Cron Contact Form',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.components.helpers.*',
        'application.components.helpers.utils.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
	),

	'modules'=>array(
		'admin'=>array(
			'defaultController' => 'dashboard',
		),
        'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'md5',

            # send activation email
            'sendActivationMail' => false,

            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => true,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
//            'returnUrl' => array('/user/profile'),
            'returnUrl' => array('/userForm'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),

			#enable captcha for any page : true or false
			'captcha' => array('registration'=>false),

			# page after login ADMIN
            'returnAdminUrl' => array('/admin/dashboard'),
        ),
	),

	// application components
	'components'=>array(
        'request'=>array(
            //You need to manual edit this param because CConsole cant not detect it (need http:// for render image in email)
            'baseUrl' => 'http://contactform.com',
        ),
        'user'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
        ),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'caseSensitive' => false,
			'rules'=>array(
//				'admin'=>'other action to prevent access to admin page',
//				//For admin page
//				'netviet'=>'admin',
//				'netviet/<controller:\w+>'=>'admin/<controller>',
//				'netviet/<controller:\w+>/<action:\w+>'=>'admin/<controller>/<action>',
//				'netviet/<controller:\w+>/<action:\w+>/<id:\d+>'=>'admin/<controller>/<action>/<id:\d+>',
				//BLOG
                'blog/<id:\d+>-<title:[\w\-]+>'=>'/blog/detail/<id:\d+>/<title:\w+>',
				'blog/category/<category:[\w\-]+>'=>'/blog/index/<category:\w+>',
				'blog/author/<author:[\w\-]+>'=>'/blog/index/<author:\w+>/',
				'blog/archive/<archive:[\w\-]+>'=>'/blog/index/<archive:\w+>/',
				'dataReceive/admin/<form_id:\d+>'=>'/dataReceive/admin/<form_id:\d+>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'clientScript' => array(
			//Doesnt need because user manually jquery
			'scriptMap' => array(
				//'jquery.js' => false,
			),
			//Put core script like jquery.yiiactiveform.js to bottom
			//'coreScriptPosition'=>CClientScript::POS_END
		),
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=contactform',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
            'tablePrefix' => '',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.templates.mailer',
            'pathLayouts' => 'application.templates.mailer.layouts'
        ),
    ),
    
 // application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'env'=>'DEVELOPMENT', // (DEVELOPMENT: rebuild core.js from cf_render.js, PRODUCTION: none)
		'minify'=>true, //minify js, css before return client
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
        'smtp' => array(
            "Host" => 'smtp.gmail.com',
            "Username" => 'contactformmedia@gmail.com',
            "Password" => 'contactform2014',
            "From" => 'contactformmedia@gmail.com',
            "SMTPDebug" => 0,
            "SMTPAuth" => true,
            "SMTPSecure" => 'ssl',
            "Port" => 465,
            "FromName" => 'contactform.com',
        ),
	),
);