<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="800" style="border-collapse: collapse;">
                <tr>
                    <td align="center" bgcolor="#f2f3f7" style="padding-top: 10px; padding-right: 0; padding-bottom: 0px; padding-left: 0">
                        <img src="<?php echo Yii::app()->request->baseUrl;?>/library/img/email-template-logo2.png" alt="">
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" style="padding-top: 25px; padding-right: 25px;padding-bottom: 25px;padding-left: 25px;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="330" align="right" valign="top">
                                    <img style="max-width: 100%; height: auto;" src="<?php echo Yii::app()->request->baseUrl.$form->avatar;?>" alt="Your contact form">
                                </td>
                                <td style="font-size: 0;line-height: 0;" width="20">&nbsp;</td>
                                <td width="400" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="color: #272727; font-family: 'Arial'; font-size: 20px; font-weight: bold;">Notify!</td>
                                        </tr>
                                        <tr>
                                            <td style="color: #272727; font-family: 'Arial'; font-size: 16px;padding-top: 10px; padding-bottom: 15px;">
                                                BetterContactForm would like to inform your contact form has been expired and disabled. Please renew before reuse.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="<?php echo Yii::app()->request->baseUrl.'/userForm/upgrade/'.$form->id ?>">
                                                    <img src="<?php echo Yii::app()->request->baseUrl;?>/library/img/email-template-renew.png" alt="">
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#f2f3f7" align="center" style="padding-top: 30px; padding-bottom: 30px; color: #5a5a5a; font-size: 15px;">
                        &copy; <?php echo date('Y')?> BetterContactForm
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
