<p>Hi,</p>
<p>Thanks for your sincere interest in our contact form.</p>
<p>Please follow these instructions to integrate the contact form.</p>
<p>1. Copy your contact form code below</p>
<h3 style="background-color: #F5F5F5; border: 1px solid #CCCCCC;">
<pre style="text-align: left"><code><?php echo htmlentities($code);?></code></pre>
</h3>
<p>2. Insert the code before the <?php echo htmlentities('</body>');?> tag on all pages on your website</p>
<p>3. Test your contact form to see if it's working</p>