<div style=" display: block;">
	<?php if ($customer_data['customer_name']) :?>
		<p>Customer name: <strong><?php echo $customer_data['customer_name'];?></strong></p>
	<?php endif;?>
	<?php if ($customer_data['customer_email']) :?>
		<p>Customer email: <strong><?php echo $customer_data['customer_email'];?></strong></p>
	<?php endif;?>
	<?php if ($customer_data['customer_phone']) :?>
		<p>Customer phone: <strong><?php echo $customer_data['customer_phone'];?></strong></p>
	<?php endif;?>
	<?php if ($customer_data['customer_website']) :?>
		<p>Customer website: <strong><?php echo $customer_data['customer_website'];?></strong></p>
	<?php endif;?>
	<?php if ($customer_data['customer_subject']) :?>
		<p>Customer subject: <strong><?php echo $customer_data['customer_subject'];?></strong></p>
	<?php endif;?>
	<?php if ($customer_data['customer_company']) :?>
		<p>Customer company: <strong><?php echo $customer_data['customer_company'];?></strong></p>
	<?php endif;?>
	<?php if ($customer_data['customer_message']) :?>
		<p>Customer message: <strong><?php echo $customer_data['customer_message'];?></strong></p>
	<?php endif;?>
    <p>From domain: <strong><?php echo $customer_data['from_site'];?></strong></p>
</div>