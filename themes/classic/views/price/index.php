<link href="<?php echo REL_BASE_URL; ?>/library/css/hover.css" rel="stylesheet">
<div id="banner" class="banner-pricing">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">PRICING</h1>
                    <h3 class="text-center">58% DISCOUNT for yearly plan</h3>
                    <p class="text-center">We support small companies and startups. Contact us for discount</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="center-block text-center">
        <button href="javascript:void(0)" class="my_button margin-sm-top monthly-btn push" for="fee_monthly">Monthly</button>
        <button href="javascript:void(0)" class="my_button margin-sm-top yearly-btn push" for="fee_yearly">Yearly</button>
    </div>

    <div class="row">
        <div class="col-sm-6 margin-sm-top">
            <div class="wrap-pricing">
                <div class="pricing-block text-center">
                    <div class="pricing-block-head block-1">
                        <h3>pro</h3>
                        <div class="price-circle">
                            <div id="fee_yearly">
                                <h4>$<?php echo $fee_yearly;?></h4>
                                <span>Yearly</span>
                            </div>
                            <div id="fee_monthly">
                                <h4>$<?php echo $fee_monthly;?></h4>
                                <span>Monthly</span>
                            </div>
                        </div>
                    </div>
                    <div class="pricing-block-body">
                        <div class="pricing-bd-first">
                            <!--<h3>10.000 Submissions</h3>-->
                            <h3>Unlimited Submissions</h3>
                            <?php if( Yii::app()->user->isGuest ):?>
                                <a href="<?php echo ABS_BASE_URL . '/user/login'; ?>" class="btn-pri-signup sign-1 push">Sign in</a>
                            <?php endif;?>
                            <span>Free feature +</span>
                        </div>
                        <div class="pricing-bd-second">
                            <ul>
                                <li>Referral links removed</li>
                                <!--<li>Email File attachment (up to 5 files and 5Mb/file)</li>-->
                                <li>Autoresponder</li>
                                <li>Multi-departments</li>
                                <!--<li>Rich text format</li>-->
                                <li>Own sender</li>
                                <li>Unlimited Domains</li>
                                <li>Manage data submitted</li>
                                <li>Free installation</li>
                                <li>Email support</li>
                            </ul>
                        </div>
                    </div>
                    <div class="pricing-block-foot">
                        <a href="<?php echo ABS_BASE_URL . '/features'; ?>">See all features...</a>
                    </div>
                </div>
            </div>
        </div>

<!--        <div class="col-sm-4 margin-sm-top">
            <div class="wrap-pricing">
                <div class="pricing-block text-center">
                    <div class="pricing-block-head block-2">
                        <h3>basic</h3>
                        <div class="price-circle">
                            <h4>$49.99</h4>
                            <span>Yearly</span>
                        </div>
                    </div>
                    <div class="pricing-block-body">
                        <div class="pricing-bd-first">
                            <h3>1000 Submissions</h3>
                            <a href="#" class="btn-pri-signup sign-2 push">Signup</a>
                            <span>Free feature +</span>
                        </div>
                        <div class="pricing-bd-second">
                            <ul>
                                <li>Referral links removed</li>
                                <li>Email File attachment (up to 5 files and 5Mb/file)</li>
                                <li>Autoresponder</li>
                                <li>Multi-departments</li>
                                <li>Rich text format</li>
                                <li>Own sender</li>
                                <li>Free installation</li>
                                <li>Email support</li>
                            </ul>
                        </div>
                    </div>
                    <div class="pricing-block-foot">
                        <a href="<?php echo ABS_BASE_URL . '/features'; ?>">See all features...</a>
                    </div>
                </div>
            </div>
        </div>-->

        <div class="col-sm-6 margin-sm-top">
            <div class="wrap-pricing">
                <div class="pricing-block text-center">
                    <div class="pricing-block-head block-3">
                        <h3>free</h3>
                        <div class="price-circle">
                            <h4>0</h4>
                        </div>
                    </div>
                    <div class="pricing-block-body">
                        <div class="pricing-bd-first">
                            <!--<h3>100 Submissions</h3>-->
                            <h3>Limited Submissions</h3>
                            <?php if( Yii::app()->user->isGuest ):?>
                                <a href="<?php echo ABS_BASE_URL . '/user/login'; ?>" class="btn-pri-signup sign-3 push">Sign in</a>
                            <?php endif;?>
                            <span>Free feature +</span>
                        </div>
                        <div class="pricing-bd-second">
                            <ul>
                                <li>Submissions</li>
                                <li>Responsive form</li>
                                <li>Customizable</li>
                                <li>Anti-spam</li>
                                <li>Secure SSL</li>
                                <!--<li>Unlimited Domains</li>-->
                                <li>Modern (HTML5, Lightbox, Ajax)</li>
                            </ul>
                        </div>
                    </div>
                    <div class="pricing-block-foot">
                        <a href="<?php echo ABS_BASE_URL . '/features'; ?>">See all features...</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="feature" class="margin-md-top">
    <div id="slogan">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="slg-center text-center" style="font-size: 35px;color: #343434;font-weight: bold">
                        Frequently Asked Questions and Answers
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="qa-item">
                    <h3 class="margin-bottom-remove">Is the Better Contact Form secured?</h3>
                    <p>Yes, all plans support secured SSL connection and latest spam protection technology.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="qa-item">
                    <h3 class="margin-bottom-remove">What types of payment do you accept?</h3>
                    <p>We accept all major credit cards.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="qa-item">
                    <h3 class="margin-bottom-remove">Does Better Contact Form work on my website?</h3>
                    <p>The Better Contact Form is designed to work on all website platforms, eg. PHP, .NET, Java, WordPress,
                        Joomla, Drupal, Magento, PrestaShop ...</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="qa-item">
                    <h3 class="margin-bottom-remove">Is BetterContactForm really free for non-profit website?</h3>
                    <p>Yes it is! If your website is non-profit, please contact us to request a free Pro account.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="qa-item">
                    <h3 class="margin-bottom-remove">What kind of support do you offer?</h3>
                    <p>We offer free installation service and direct email support for all paid plans.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="qa-item">
                    <h3 class="margin-bottom-remove">What’s your refund policy?</h3>
                    <p>We offer 30 days money back, no questions asked.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="get-start margin-md-top-bottom">
        <a href="<?php echo ABS_BASE_URL . '/faq'; ?>"><img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/faq-button.png" alt=""/></a>
    </div>
</div>

<script>
    jQuery(document).ready(function (){
        jQuery('.my_button').on('click',function(){
            //monthly-btn : active
            //yearly-btn : inactive
            
            //Remove anđ add class for other button
            jQuery('.my_button').removeClass('monthly-btn');
            jQuery('.my_button').addClass('yearly-btn');
            
            //Remove anđ add class for this button
            jQuery(this).removeClass('yearly-btn');
            jQuery(this).addClass('monthly-btn');
            
            var show_block = jQuery(this).attr('for');
            showBlock(show_block);
        });
        
        showBlock('fee_monthly');
    });
    
    function showBlock(show_block){
        if(show_block == 'fee_monthly'){
            jQuery('#fee_monthly').removeClass('hide');
            jQuery('#fee_yearly').addClass('hide');
        } else {
            jQuery('#fee_yearly').removeClass('hide');
            jQuery('#fee_monthly').addClass('hide');
        }
    }
</script>