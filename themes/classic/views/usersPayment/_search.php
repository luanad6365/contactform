<!-- Date Picker http://eternicode.github.io/bootstrap-datepicker/?#sandbox -->
<link href="<?php echo REL_BASE_URL; ?>/admin_library/css/datepicker.css" rel="stylesheet">
<!-- Date Picker http://eternicode.github.io/bootstrap-datepicker/?#sandbox -->
<script src="<?php echo REL_BASE_URL; ?>/admin_library/js/bootstrap-datepicker.js"></script>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'htmlOptions' => array(
        'class' => 'form-inline'
    ),
        ));
?>
<div class="form-group">
    <?php echo $form->label($model, 'form_id'); ?>
    <?php echo $form->textField($model, 'form_id', array('maxlength' => 255, 'class' => 'form-control')); ?>
</div>
<div class="form-group input-group">
    <span class="input-group-addon"><?php echo $form->label($model,'purchase_date'); ?></span>
    <div class="input-group date" id="wrap_search_date">
        <?php echo $form->textField($model, 'purchase_date', array('maxlength' => 255, 'class' => 'form-control')); ?>
        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
    </div>
</div>
<div class="form-group">
    <?php echo $form->label($model,'payment_type');  ?>
    <?php echo $form->dropDownList($model, 'payment_type', array_merge(array('' => 'All'), CfConst::getAllPaymentTypePremium()), array('class' => 'form-control')); ?>
</div>
<button class="btn btn-default" type="submit"><i class="fa fa-search" style="font-size: 20px;"></i></button>

<?php $this->endWidget(); ?>
<script>
$('#wrap_search_date').datepicker({
	format: 'yyyy-mm-dd',
	todayBtn: 'linked',
	clearBtn: true,
	autoclose: true,
	todayHighlight: true
});
</script>