<style>
    /*Margin for edit button*/
    .fa.fa-times {margin-right: 8px;}
    /*Hide some element in paging*/
    ul.pagination .first, ul.pagination .last {
        display: none;
    }
</style>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-payment-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="feature" class="receive-admin">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 margin-md-top">
                <div class="well">
                    <h1>Payment History</h1>

                    <div class="search-form">
                        <?php
                        $this->renderPartial('_search', array(
                            'model' => $model,
                        ));
                        ?>
                    </div><!-- search-form -->
                    
                    <div class="table-responsive">
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'users-payment-grid',
                            'enableSorting' => true,
                            'filterPosition' => 'none', // Hide filter
                            'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
                            'dataProvider' => $model->search_single(),
                            'filter' => $model,
                            'columns' => array(
                                'id',
                                'transaction_id',
								array(
									'name' => 'payment_type',
									'value'=>function($data,$row){
                                        if($data->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_MONTHLY){
                                            return 'Premium monthly';
                                        } elseif ($data->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_YEARLY) {
                                            return 'Premium yearly';
                                        }
									},
								),
                                'amount',
                                'purchase_date',
                                array(
                                    'name' => 'form_id',
                                    'type' => 'raw',
                                    'value' => 'CHtml::link(CHtml::encode($data->form_id),array("form/editForm","id"=>$data->form_id), array("target"=>"_blank"))',
                                ),
								array(
									'name' => 'expired_date',
									'header'=>'Form Expired Date',
								),
                                array(
									'name' => 'new_expired_date',
									'header'=>'Form New Expired Date',
								),
                            ),
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination blog-pagination margin-top-remove'),
                                'nextPageLabel' => '&gt;',
                                'prevPageLabel' => '&lt;',
                                'selectedPageCssClass' => 'active',
                                'lastPageLabel' => '&gt;&gt;',
                                'firstPageLabel' => '&lt;&lt;',
                            ),
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
