<?php
/* @var $this FormLanguageController */
/* @var $data FormLanguage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('language')); ?>:</b>
	<?php echo CHtml::encode($data->language); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message_key')); ?>:</b>
	<?php echo CHtml::encode($data->message_key); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message_value')); ?>:</b>
	<?php echo CHtml::encode($data->message_value); ?>
	<br />


</div>