<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'htmlOptions' => array(
        'class' => 'form-inline'
    ),
        ));
?>
<div class="form-group">
	<?php echo $form->labelEx($model,'language'); ?>
	<?php echo $form->dropDownList($model,'language', array_merge( array(''=>'All'), CfConst::getAllLanguage()), array('class' => 'form-control') ); ?>
</div>
<div class="form-group">
	<?php echo $form->label($model,'message_key'); ?>
	<?php echo $form->dropDownList($model,'message_key', array_merge( array(''=>'All'), CfConst::getAllKeyFormLanguage()), array('class' => 'form-control') ); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'message_value'); ?>
	<?php echo $form->textField($model,'message_value',array('maxlength'=>255, 'autocomplete'=>'off', 'class' => 'form-control')); ?>
</div>

<button class="btn btn-default" type="submit"><i class="fa fa-search" style="font-size: 20px;"></i></button>

<?php $this->endWidget(); ?>