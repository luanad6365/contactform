<?php
/* @var $this FormLanguageController */
/* @var $model FormLanguage */

$this->breadcrumbs=array(
	'Form Languages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FormLanguage', 'url'=>array('index')),
	array('label'=>'Create FormLanguage', 'url'=>array('create')),
	array('label'=>'Update FormLanguage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FormLanguage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FormLanguage', 'url'=>array('admin')),
);
?>

<h1>View FormLanguage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'language',
		'message_key',
		'message_value',
	),
)); ?>
