<?php
/* @var $this FormLanguageController */
/* @var $model FormLanguage */
/* @var $form CActiveForm */
?>

<div class="container">

<?php 
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'form-language-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation' => false,
	'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
	'clientOptions'=>array(
		'successCssClass' => 'has-success',
		'errorCssClass' => 'has-error',
		'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
	),
));
$current_action = Yii::app()->controller->action->id;
//Update: only update message
if($current_action == 'update'){
	$extraOptions = array('disabled'=>'disabled');
} else {
	$extraOptions = array();
}
?>

	<?php if( $msg = Yii::app()->user->getFlash('success') ) : ?>
		<div id="sucess_msg" class="alert alert-success"><?php echo $msg; ?></div>
	<?php endif;?>
	<?php echo $form->error($model,'language'); ?>
	<?php echo $form->error($model,'message_key'); ?>
	<?php echo $form->error($model,'message_value'); ?>
	

	<div class="form-group">
		<?php echo $form->labelEx($model,'language'); ?>
		<?php echo $form->dropDownList($model,'language', CfConst::getAllLanguage(), array_merge($extraOptions, array('class' => 'form-control'))); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'message_key'); ?>
		<?php echo $form->dropDownList($model,'message_key', CfConst::getAllKeyFormLanguage(), array_merge($extraOptions, array('class' => 'form-control')) );?>
	</div>
	
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'message_value'); ?>
		<?php echo $form->textField($model,'message_value',array('maxlength'=>255, 'autocomplete'=>'off', 'class' => 'form-control for-text')); ?>
		<?php echo $form->dropDownList($model,'message_value', CfConst::getAllClassSize(), array('class' => 'form-control for-dropdown hide')); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-send-t margin-sm-top')); ?>
	</div>

<?php $this->endWidget();
?>

</div><!-- form -->
<script>
jQuery(document).ready(function (){
	var key = jQuery('#FormLanguage_message_key').val();
	updateMessValue(key);
});

jQuery('#FormLanguage_message_key').change(function(){
	var key = jQuery(this).val();
	updateMessValue(key);
});

function updateMessValue(key){
	if(key == '<?php echo CfConst::LANG_KEY_CLASS_SIZE;?>'){
		jQuery('.for-dropdown').removeClass('hide');
		jQuery('.for-text').addClass('hide');
		jQuery('.for-dropdown').removeAttr('disabled');
		jQuery('.for-text').attr('disabled', 'disabled');
	} else {
		jQuery('.for-text').removeClass('hide');
		jQuery('.for-dropdown').addClass('hide');
		jQuery('.for-text').removeAttr('disabled');
		jQuery('.for-dropdown').attr('disabled', 'disabled');
	}
}
</script>
