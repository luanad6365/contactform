<?php
/* @var $this FormLanguageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Form Languages',
);

$this->menu=array(
	array('label'=>'Create FormLanguage', 'url'=>array('create')),
	array('label'=>'Manage FormLanguage', 'url'=>array('admin')),
);
?>

<h1>Form Languages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
