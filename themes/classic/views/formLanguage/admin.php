<style>
    /*Margin for edit button*/
    .fa.fa-times {margin-right: 8px;}
    /*Hide some element in paging*/
    ul.pagination .first, ul.pagination .last {
        display: none;
    }
</style>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#form-language-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="feature" class="receive-admin">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 margin-md-top">
                <div class="well">
                    <h1>Manage Form Languages</h1>

                    <div class="search-form">
                        <?php
                        $this->renderPartial('_search', array(
                            'model' => $model,
                        ));
                        ?>
                    </div><!-- search-form -->


					
                    <div class="table-responsive">
						<div class="margin-md-top">
							<?php echo CHtml::link('Create New',array('formLanguage/create'), array('class'=>'btn btn-primary')); ?>	
							<?php echo CHtml::link('Generate Language Admin',array('formLanguage/generate'), array('class'=>'btn btn-success')); ?>	
							<?php echo CHtml::link('Generate Language Client',array('formLanguage/generateClient'), array('class'=>'btn btn-info')); ?>	
						</div>
						
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'form-language-grid',
							'filterPosition' => 'none', // Hide filter
                            'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
							'dataProvider'=>$model->search(),
							'filter'=>$model,
							'columns'=>array(
								'id',
								'language',
								'message_key',
								'message_value',
                                array(
                                    'class' => 'CButtonColumn',
                                    'template' => '{delete}{update}',
                                    'deleteButtonImageUrl' => false,
                                    'updateButtonImageUrl' => false,
                                    'buttons' => array(
                                        'update' => array(
                                            'options' => array('class' => 'fa fa-pencil', 'target' => '_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upđate'),
                                            'label' => '',
                                        ),
                                        'delete' => array(
                                            'options' => array('class' => 'fa fa-times', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                                            'label' => ''
                                        ),
                                    ),
                                ),
							),
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination blog-pagination margin-top-remove'),
                                'nextPageLabel' => '&gt;',
                                'prevPageLabel' => '&lt;',
                                'selectedPageCssClass' => 'active',
                                'lastPageLabel' => '&gt;&gt;',
                                'firstPageLabel' => '&lt;&lt;',
                            ),
						)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
