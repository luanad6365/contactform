<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'htmlOptions' => array(
        'class' => 'form-inline'
    ),
        ));
?>
<div class="form-group">
    <?php echo $form->label($model, 'from_site'); ?>
    <?php echo $form->textField($model, 'from_site', array('maxlength' => 255, 'class' => 'form-control')); ?>
</div>
<div class="form-group">
    <?php echo $form->label($model, 'customer_email'); ?>
    <?php echo $form->textField($model, 'customer_email', array('maxlength' => 255, 'class' => 'form-control')); ?>
</div>
<div class="form-group">
    <?php echo $form->label($model, 'customer_name'); ?>
    <?php echo $form->textField($model, 'customer_name', array('maxlength' => 255, 'class' => 'form-control')); ?>
</div>
<div class="form-group">
    <?php echo $form->label($model,'data_status');  ?>
    <?php echo $form->dropDownList($model, 'data_status', array_merge(array('' => 'All'), CfConst::getAllDataStatus()), array('class' => 'form-control')); ?>
</div>
<button class="btn btn-default" type="submit"><i class="fa fa-search" style="font-size: 20px;"></i></button>

<?php $this->endWidget(); ?>