<style>
    /*Margin for edit button*/
    .fa.fa-times {margin-right: 8px;}
    /*Hide some element in paging*/
    ul.pagination .first, ul.pagination .last {
        display: none;
    }
</style>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#data-receive-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="feature" class="receive-admin">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 margin-md-top">
                <div class="well">
                    <h1>Manage Data Receives</h1>

                    <div class="search-form">
                        <?php
                        $this->renderPartial('_search', array(
                            'model' => $model,
                        ));
                        ?>
                    </div><!-- search-form -->

                    <div class="table-responsive">
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'data-receive-grid',
                            'enableSorting' => true,
                            'filterPosition' => 'none', // Hide filter
                            'itemsCssClass' => 'table table-striped table-bordered table-hover', //Add class css for table
                            'dataProvider' => $model->search_single(),
                            'filter' => $model,
                            'columns' => array(
                                //		array(
                                //			'class'=>'CCheckBoxColumn',
                                //			'header'=>'Selected',
                                //			'id'=>'data-receive-check-boxes',
                                //			'selectableRows' => 2,
                                //			'checkBoxHtmlOptions'=>array('class'=>'data_receive_id'),//for js bottom of page
                                //		),
                                array(
                                    'name' => 'id',
                                    'type' => 'raw',
                                    'value' => 'CHtml::link(CHtml::encode($data->id),array("dataReceive/view","id"=>$data->id), array("target"=>"_blank"))',
                                ),
                                array(
                                    'name' => 'recipient_email',
                                    'type' => 'raw',
                                    'value' => 'CHtml::link(UHtml::markSearch($data,"recipient_email"), "mailto:".$data->recipient_email)',
                                ),
                                array(
                                    'name' => 'from_site',
                                    'type' => 'raw',
                                    'value' => 'CHtml::link(UHtml::markSearch($data,"from_site"), $data->from_site)',
                                ),
                                array(
                                    'name' => 'customer_email',
                                    'type' => 'raw',
                                    'value' => 'CHtml::link(UHtml::markSearch($data,"customer_email"), "mailto:".$data->customer_email)',
                                ),
                                'customer_name',
                                'create_at',
                                array(
                                    'name' => 'data_status',
                                    'value' => function($data, $row) {
                                        $stt = CfConst::getAllDataStatus();
                                        if ($data->data_status && isset($stt[$data->data_status])) {
                                            return $stt[$data->data_status];
                                        }
                                        return '';
                                    },
                                ),
                                array(
                                    'class' => 'CButtonColumn',
                                    'template' => '{delete}{view}',
                                    'deleteButtonImageUrl' => false,
                                    'updateButtonImageUrl' => false,
                                    'viewButtonImageUrl' => false,
                                    'buttons' => array(
                                        'update' => array(
                                            'options' => array('class' => 'fa fa-pencil', 'target' => '_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upđate'),
                                            'label' => '',
                                            'url' => 'Yii::app()->createUrl("form/editForm", array("id"=>$data->id))',
                                        ),
                                        'delete' => array(
                                            'options' => array('class' => 'fa fa-times', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                                            'label' => ''
                                        ),
                                        'view' => array(
                                            'options' => array('class' => 'fa fa-search', 'target' => '_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View Detail'),
                                            'label' => '',
                                        //'url'=>'Yii::app()->createUrl("admin/dataReceive/single", array("id"=>$data->id))',
                                        ),
                                    ),
                                ),
                            ),
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination blog-pagination margin-top-remove'),
                                'nextPageLabel' => '&gt;',
                                'prevPageLabel' => '&lt;',
                                'selectedPageCssClass' => 'active',
                                'lastPageLabel' => '&gt;&gt;',
                                'firstPageLabel' => '&lt;&lt;',
                            ),
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



