<?php
/* @var $this DataReceiveController */
/* @var $data DataReceive */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('form_id')); ?>:</b>
	<?php echo CHtml::encode($data->form_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('form_style')); ?>:</b>
	<?php echo CHtml::encode($data->form_style); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('form_language')); ?>:</b>
	<?php echo CHtml::encode($data->form_language); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recipient_email')); ?>:</b>
	<?php echo CHtml::encode($data->recipient_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from_site')); ?>:</b>
	<?php echo CHtml::encode($data->from_site); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_email')); ?>:</b>
	<?php echo CHtml::encode($data->customer_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_name')); ?>:</b>
	<?php echo CHtml::encode($data->customer_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_phone')); ?>:</b>
	<?php echo CHtml::encode($data->customer_phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_company')); ?>:</b>
	<?php echo CHtml::encode($data->customer_company); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_website')); ?>:</b>
	<?php echo CHtml::encode($data->customer_website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_subject')); ?>:</b>
	<?php echo CHtml::encode($data->customer_subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_message')); ?>:</b>
	<?php echo CHtml::encode($data->customer_message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	*/ ?>

</div>