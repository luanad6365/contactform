<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Upgrade Premium");?>
<link href="<?php echo REL_BASE_URL; ?>/library/css/define.css" rel="stylesheet">
<div id="banner" class="banner-pricing extend-banner-form2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h3 class="text-center">Hello <?php echo Yii::app()->user->name; ?></h3>
                </div>
                <div class="text-center">
                    <p class="text-white"><b><?php echo $formPlan; ?></b></p>
                    <p class="text-white"><b>Created date: <?php echo date('Y-m-d', strtotime($modelForm->created)); ?>   >  Date expired: <?php echo date('Y-m-d', strtotime($modelForm->expired_date)); ?></b></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="feature">
    <div class="container">
        <div class="form2-wrap margin-top-30">
            <div class="text-center">
                <h5 class="form2-line-title">Upgrade Premium</h5>
            </div>
            <div class="inner-upgrade-wrap">
                <?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
                
                    <div class="text-center">
                        <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
                    </div>
                
                <?php else: ?>
                
                    <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id'=>'authorize-checkout-form',
                            'enableClientValidation'=>false,
                            'enableAjaxValidation'=>false,
                            'errorMessageCssClass'=>'alert alert-danger', //alert alert-error
                            'clientOptions'=>array(
                                'validateOnSubmit'=>true,
                                'validateOnType'=>false,
                                'validateOnChange'=>false,
                                'successCssClass' => 'has-success',
                                'errorCssClass' => 'has-error',
                                'afterValidate' => 'js:function(form){$("#sucess_msg").remove(); return true;}'//Remove success message if it have, return true to continue validate
                            ),
                            'htmlOptions' => array(),
                        ));
                    ?>

                        <?php echo $form->error($authorizeCheckout,'paymentType'); ?>
                        <?php echo $form->error($authorizeCheckout,'firstName'); ?>
                        <?php echo $form->error($authorizeCheckout,'lastName'); ?>
                        <?php echo $form->error($authorizeCheckout,'country'); ?>
                        <?php echo $form->error($authorizeCheckout,'state'); ?>
                        <?php echo $form->error($authorizeCheckout,'city'); ?>
                        <?php echo $form->error($authorizeCheckout,'address'); ?>
                        <?php echo $form->error($authorizeCheckout,'zip'); ?>
                        <?php echo $form->error($authorizeCheckout,'cardNumber'); ?>
                        <?php echo $form->error($authorizeCheckout,'expiredMonth'); ?>
                        <?php echo $form->error($authorizeCheckout,'expiredYear'); ?>
                        <?php echo $form->error($authorizeCheckout,'cvvCode'); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'firstName'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'firstName',array('maxlength'=>255, 'class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'lastName'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'lastName',array('maxlength'=>255, 'class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'country'); ?>
                                    <?php echo $form->dropDownList($authorizeCheckout,'country', CHtml::listData(Country::model()->findAll(), 'id', 'name'), array('class' => 'form-control', 'prompt' => 'Select Country')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'state'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'state',array('maxlength'=>255, 'class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'city'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'city',array('maxlength'=>255, 'class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'address'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'address',array('maxlength'=>255, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'paymentType'); ?>
                                    <?php echo $form->dropDownList($authorizeCheckout,'paymentType', CfConst::getAllPaymentTypePremium(), array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'zip'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'zip',array('maxlength'=>255, 'class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'cardNumber'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'cardNumber',array('maxlength'=>255, 'class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'expiredMonth'); ?>
                                    <?php echo $form->dropDownList($authorizeCheckout,'expiredMonth',Common::getMonthOfYear(), array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'expiredYear'); ?>
                                    <?php echo $form->dropDownList($authorizeCheckout,'expiredYear',Common::getYearRange(), array('class' => 'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->labelEx($authorizeCheckout,'cvvCode'); ?>
                                    <?php echo $form->textField($authorizeCheckout,'cvvCode',array('maxlength'=>5, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 text-center">
                                <button type="submit" class="btn btn-lg btn-primary">Submit <i class="fa fa-arrow-circle-right"></i></button>
                                <?php //echo CHtml::submitButton('Submit', array('class' => 'btn btn-lg btn-primary')); ?>
                            </div>
                        </div>
                    <?php $this->endWidget(); ?>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
