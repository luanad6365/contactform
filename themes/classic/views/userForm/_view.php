<?php foreach ($dataProvider as $data):?>
    <div class="col-md-4">
        <div class="form-information clearfix">
            <div class="pull-left">
                <?php if( $data->payment_type == CfConst::PAYMENT_TYPE_FREE_PERIOD ):?>
                    <b>Package</b> <img src="<?php echo REL_BASE_URL; ?>/library/img/ribon-free.png" alt="">
                <?php elseif( $data->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_MONTHLY || $data->payment_type == CfConst::PAYMENT_TYPE_PREMIUM_YEARLY ):?>
                    <b>Package</b> <img src="<?php echo REL_BASE_URL; ?>/library/img/pro.png" alt="">
                <?php endif;?>
            </div>
            <div class="pull-right">
                <a href="<?php echo ABS_BASE_URL; ?>/userForm/upgrade/<?php echo $data->id; ?>" class="btn btn-success margin-top-10">Upgrade now <i class="fa fa-arrow-circle-up"></i></a>
            </div>
        </div>
        <div class="my-form-item">
            <div class="list-group">
                <div class="list-group-item">
                    <div class="img-my-form">
                        <img class="img-responsive img-rounded" src="<?php echo $data->avatar;?>" alt="Avatar" />
                    </div>
                </div>
                <div class="list-group-item"><i class="fa fa-envelope"></i> <?php echo $data->recipient_email;?></div>
                <!--<div class="list-group-item"><i class="fa fa-globe"></i> http://www.abc.com.vn</div>-->
            </div>
            
            <div class="mask-my-form list-group">

                <a href="<?php echo ABS_BASE_URL; ?>/form/editForm/<?php echo $data->id; ?>" class="list-group-item new-list-it non-radius"><i class="fa fa-edit"></i> Edit</a>
                <a href="<?php echo ABS_BASE_URL; ?>/dataReceive/admin/<?php echo $data->id; ?>" class="list-group-item new-list-it"><i class="fa fa-eye"></i> Manage Data</a>
                <a href="<?php echo ABS_BASE_URL; ?>/form/getCode/<?php echo $data->id; ?>" class="list-group-item new-list-it"><i class="fa fa-code"></i> Get code</a>
                <div class="list-group-item new-list-it non-radius clearfix">
                    <div class="pull-left"><span class="enable-title"><i class="fa fa-check"></i> Enable form</span></div>
                    <div class="switch pull-right">
                        <input id="toggle_status_<?php echo $data->id; ?>" class="check-status check-toggle check-toggle-round-flat" type="checkbox" data-form-id="<?php echo $data->id; ?>" <?php if($data->form_status != CfConst::CF_FORM_STATUS_DISABLE) echo 'checked="checked"'; ?>>
                        <label for="toggle_status_<?php echo $data->id; ?>"></label>
                        <span class="on">on</span>
                        <span class="off">off</span>
                    </div>
                </div>
                <a href="<?php echo ABS_BASE_URL; ?>/userForm/upgrade/<?php echo $data->id; ?>" class="list-group-item new-list-it"><i class="fa fa-usd"></i> Upgrade premium</a>
                <?php if( $data->payment_type != CfConst::PAYMENT_TYPE_FREE_PERIOD ):?>
                    <a href="<?php echo ABS_BASE_URL; ?>/form/premiumFeature/<?php echo $data->id; ?>" class="list-group-item new-list-it"><i class="fa fa-arrow-up"></i> Premium feature</a>
                <?php endif;?>
                <?php
                echo CHtml::ajaxLink(
                    '<i class="fa fa-trash"></i> Delete', $this->createUrl('userForm/delete'), 
                    array(
                        'type' => 'POST',
                        'data' => 'js:{form_id : '.$data->id.'}',
    //					'success' => 'js:function(data){$.fn.yiiListView.update("list-user-form",{});}',
                        'success' => 'js:function(data){window.location.reload();}',
                        'error' => 'function(data) { alert("error"); }',
                    ), 
                    array(
                        'confirm' => Yii::t('app', 'Are you sure you want to delete Form?'),
                        'class' => 'list-group-item new-list-it',
                        'id' => 'delete-form',
                    )
                );
                ?>
            </div>
        </div>
        <div class="form3-foot-date">
            <ul class="">
                <li>Created: <?php echo date('Y-m-d', strtotime($data->created)); ?></li>
                <li><img class="img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/arrow-date.png" alt=""></li>
                <li>Expiration: <?php echo date('Y-m-d', strtotime($data->expired_date)); ?></li>
            </ul>
        </div>
    </div>
<?php endforeach;?>

<script>
	//jQuery('#toggle_status').prop('checked', true);
	var ajax_loading = false;
	jQuery('.check-status').click(function (){
		if (ajax_loading === true) {
			alert("Data processing...");
			return false;
		};
		//Get status of checkbox
		var check = jQuery(this).prop('checked');
		var action = '';
		if(check){
			action = 'enable';
		} else {
			action = 'disable';
		}
		var form_id = jQuery(this).attr('data-form-id');
		//console.log(PARAMS.baseUrl + '/userForm/' + action);
		ajax_loading = true;
		$.ajax({
			url: PARAMS.baseUrl + '/userForm/' + action,
			data : {'form_id' : form_id},
			type: 'post',
			dataType: 'json',
			success: function (result){
                if(result.status == 'form_expired'){
                    alert(result.message);
                }
			}
		}).always(function() {
			ajax_loading = false;
		});
	});

</script>