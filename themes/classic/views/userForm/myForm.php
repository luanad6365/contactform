<link href="<?php echo REL_BASE_URL; ?>/library/css/define.css" rel="stylesheet">
<div id="banner" class="banner-pricing extend-banner-form2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h3 class="text-center">Hello <?php echo Yii::app()->user->name; ?></h3>
                </div>
                <div class="text-center">
                    <a href="<?php echo ABS_BASE_URL; ?>/userForm" class="manager-action-form2 active">My Form</a>
                    <a href="<?php echo ABS_BASE_URL; ?>/form/build" class="manager-action-form2">Create New</a>
                    <a href="<?php echo ABS_BASE_URL; ?>/user/logout" class="manager-action-form2">Logout</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="form-man" class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="text-center">
                <!--<a href="<?php //echo ABS_BASE_URL; ?>/form/build" class="btn btn-default btn-plus-form"><span class="glyphicon glyphicon-plus"></span></a>-->
                <!--<h3 style="font-size: 18px;">Create new contact form</h3>-->
                <a href="<?php echo ABS_BASE_URL; ?>/form/build" class="link-createctform"><img class="img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/create-contact-form.png" alt=""></a>
            </div>
        </div>
		
		<?php
		$this->renderPartial('_view',array('dataProvider'=>$dataProvider));
//		$this->widget('zii.widgets.CListView', array(
//			'id'=>'list-user-form',
//			'dataProvider' => $dataProvider,
//			'itemView' => '_view',
//			'summaryText' => '',
//			'enablePagination' => false,
//			'emptyText' => '',
//		));
		?>
        
    </div>
</div>