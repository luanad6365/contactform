<div id="banner" class="banner-faqs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">
						<?php
						if( isset($data->name) && $data->name):
							echo $data->name;
						else :
							echo 'Frequently Asked Questions';
						endif;
						?>	
					</h1>
                    <p class="text-center">
						<?php
						if( isset($data->description) && $data->description):
							echo $data->description;
						endif;
						?>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div id="wrap-faq">
        <ul class="main-faq">
			<?php
			if( isset($listFAQ) && $listFAQ):
				foreach ($listFAQ as $faq):
			?>
				<li>
					<div class="row">
						<div class="col-xs-12">
							<div class="qa-item">
								<h3><?php echo $faq->question;?></h3>
								<p><?php echo $faq->answer;?></p>
							</div>
						</div>
					</div>
				</li>
			<?php
				endforeach;
			endif;
			?>
        </ul>
    </div>
</div>