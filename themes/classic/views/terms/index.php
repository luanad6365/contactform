<?php //echo $data->name;?>
<?php //echo $data->content;?>

<div id="banner" class="banner-terms">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">
						<?php
						if( isset($data->name) && $data->name):
							echo $data->name;
						else :
							echo 'Terms of Service';
						endif;
						?>	
					</h1>
                    <p class="text-center">
						<?php
						if( isset($data->description) && $data->description):
							echo $data->description;
						else :
							echo 'We support small companies and startups. Contact us for discount';
						endif;
						?>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 margin-md-top-bottom">
            <div id="wrap_terms">
				<?php
				if( isset($data->content) && $data->content):
					echo $data->content;
				endif;
				?>
<!--                <p>
                    By using the BetterContactForm (Service), you are agreeing to be bound by the following terms and conditions 
                    ("Terms of Service"). BetterContactForm reserves the right to update and change these Terms of Service without 
                    notice. Violation of any of the terms below may result in the termination of your account. 
                </p>
                <h3 style="font-size: 16px;font-weight: 700;color: #343434;">Account Terms</h3>
                <p>1. You are responsible for maintaining the security of your account and password. BetterContactForm cannot and will not be     liable for any loss or damage from your failure to comply with this security obligation.</p>
                <p>2. You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.</p>
                <p>3. Your login may only be used by one person – a single login shared by multiple people is not permitted.</p>
                <p>4. You must be a human. Accounts registered by “bots” or other automated methods are not permitted.</p>
                <h3 style="font-size: 16px;font-weight: 700;color: #343434;">Payment, Refunds, Upgrading and Downgrading Terms</h3>
                <p>1. The Service is offered with a free plan. You are only required to pay for using the Pro plan with additional features.</p>
                <p>2. Downgrading your Service may cause the loss of features or capacity of your account. BetterContactForm does not accept any liability for such loss.</p>
                <p>
                    3. To guarantee your protection we offer 30-days money back guarantee for our service. If you are dissatisfied with your service before 30 days after the purchase for any reason, you can receive a full refund. To request a refund please submit a request containing your Transaction ID. Please note, all refund requests after 30 days are not accepted.
                    Cancellation and Termination
                    You are solely responsible for properly canceling your account. Please submit a request to cancel your account.
                    All of your content will be immediately be inaccessible from the Service upon cancellation. Within 30 days, all this content will be permanently deleted from all backups and logs. This information can not be recovered once it has been permanently deleted.
                    If you cancel the Service before the end of your current paid up month, your cancellation will take effect immediately, and you will not be charged again. But there will not be any prorating of unused time in the last billing cycle.
                    BetterContactForm has the right to suspend or terminate your account and refuse any and all current or future use of the Service for any reason at any time. We reserve the right to refuse service to anyone for any reason at any time.
                    Modifications to the Service and Prices
                    BetterContactForm reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, any part of the Service with or without notice.
                    Prices of all Services are subject to change upon 30 days notice from us. Such notice may be provided at any time by posting the changes to the BetterContactForm site or the Service itself.
                    BetterContactForm shall not be liable to you or to any third party for any modification, price change, suspension or discontinuance of the Service.
                    Copyright
                    The look and feel (design) of the Service is copyright© BetterContactForm. All rights reserved. You may not duplicate, copy, or reuse any portion of the HTML, CSS, JavaScript, or visual design elements without express written permission from BetterContactForm.
                    General Conditions
                    Your use of the Service is at your sole risk. The service is provided on an “as is” and “as available” basis.
                    Technical support is only provided via email.
                    You understand that BetterContactForm uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run the Service.
                    You must not modify, adapt or hack the Service.
                    You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without the express written permission by BetterContactForm.
                    We reserve the right to temporarily disable your account if your usage significantly exceeds the average usage of other Service customers. Of course, we'll reach out to the account owner before taking any action except in rare cases where the level of use may negatively impact the performance of the Service for other customers.
                    BetterContactForm does not warrant that (i) the service will meet your specific requirements, (ii) the service will be uninterrupted, timely, secure, or error-free, (iii) the results that may be obtained from the use of the service will be accurate or reliable, (iv) the quality of any products, services, information, or other material purchased or obtained by you through the service will meet your expectations, and (v) any errors in the Service will be corrected.
                    You expressly understand and agree that BetterContactForm shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if BetterContactForm has been advised of the possibility of such damages), resulting from: (i) the use or the inability to use the service; (ii) the cost of procurement of substitute goods and services resulting from any goods, data, information or services purchased or obtained or messages received or transactions entered into through or from the service; (iii) unauthorized access to or alteration of your transmissions or data; (iv) statements or conduct of any third party on the service; (v) or any other matter relating to the service.
                    Any new features that augment or enhance the current Service, including the release of new tools and resources, shall be subject to the Terms of Service. Continued use of the Service after any such changes shall constitute your consent to such changes. If you have any questions or concerns, please leave us a message and we’ll be happy to answer them!
                </p>-->
            </div>
        </div>
    </div>
</div>