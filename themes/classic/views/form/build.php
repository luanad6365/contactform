<!-- Custom styles for this template - display responsive -->
<link href="<?php echo REL_BASE_URL; ?>/library/css/non-responsive.css" rel="stylesheet">
<link href="<?php echo REL_BASE_URL; ?>/library/css/classic/form-builder.css" rel="stylesheet">
<link href="<?php echo REL_BASE_URL; ?>/library/css/bootstrap-colorpicker.css" rel="stylesheet">
<script src="<?php echo REL_BASE_URL; ?>/library/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo REL_BASE_URL; ?>/library/js/classic/form_messages.js"></script>

<?php $this->widget('ext.tooltipster.tooltipster',array(
    'identifier' => '.tooltipster',
    'options' => array(
        'position' => 'top',
        'delay' => '100',
        'offsetY' => '-3',
        'theme' => '.tooltipster-punk'
    )
    )); ?>

<div id="my-contact">
	<div class="container">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'build-form',
            'enableClientValidation'=>false,
            'enableAjaxValidation'=>true,
            'errorMessageCssClass'=>'error-message', //alert alert-error
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnType'=>false,
                'validateOnChange'=>false,
            ),
        )); ?>
		<div class="row">
			<div class="col-xs-12 margin-md-top-bottom">
				<h1 class="title-contact-page text-center margin-top-bottom-remove">Create Your Own Contact Form</h1>
			</div>
			<div class="clearfix"></div>
			<div id="own-contact-form" class="clearfix">
				<div class="col-xs-12">
					<div class="row">
						<div id="my-builder" class="col-xs-4">
							<a id="ctf-butt-contact" class="corlor" href="javascript:void(0)">Contact us</a>
							<?php //echo $form->errorSummary($model, NULL, NULL, $htmlOptions = array('class' => 'alert alert-error')); ?>
							<?php echo $form->error($model, 'form_color'); ?>
                            <?php echo $form->hiddenField($model, 'has_message');?>
							<h4 class="title-builder">Select your language</h4>
							<div class="form-group">
								<?php echo $form->dropDownList($model, 'form_language', CfConst::getAllLanguage(), array('class' => 'form-control')); ?>
							</div>
							<h4 class="title-builder margin-bottom-remove">Select fields to your form</h4>
							<div class="row">
								<div class="col-xs-6">
									<section class="m-top clearfix">
										<div class="squaredFour pull-left tooltipster" title="Click to display">
											<?php echo $form->checkBox($model, 'has_name', array('class'=>'chk_init_form', 'uncheckValue' => null)); ?>
											<?php echo $form->labelEx($model, 'has_name'); ?>
										</div>
										<div class="squaredlabel pull-left">Name</div>
									</section>
								</div>

								<div class="col-xs-6">
									<section class="m-top clearfix">
										<div class="squaredFour pull-left tooltipster" title="Click to display">
											<?php echo $form->checkBox($model, 'has_email', array('class'=>'chk_init_form', 'uncheckValue' => null)); ?>
											<?php echo $form->labelEx($model, 'has_email'); ?>
										</div>
										<div class="squaredlabel pull-left">Email</div>
									</section>
								</div>

								<div class="col-xs-6">
									<section class="m-top clearfix">
										<div class="squaredFour pull-left tooltipster" title="Click to display">
											<?php echo $form->checkBox($model, 'has_phone', array('class'=>'chk_init_form', 'uncheckValue' => null)); ?>
											<?php echo $form->labelEx($model, 'has_phone'); ?>
										</div>
										<div class="squaredlabel pull-left">Phone</div>
									</section>
								</div>

								<div class="col-xs-6">
									<section class="m-top clearfix">
										<div class="squaredFour pull-left tooltipster" title="Click to display">
											<?php echo $form->checkBox($model, 'has_company', array('class'=>'chk_init_form', 'uncheckValue' => null)); ?>
											<?php echo $form->labelEx($model, 'has_company'); ?>
										</div>
										<div class="squaredlabel pull-left">Company</div>
									</section>
								</div>

								<div class="col-xs-6">
									<section class="m-top clearfix">
										<div class="squaredFour pull-left tooltipster" title="Click to display">
											<?php echo $form->checkBox($model, 'has_website', array('class'=>'chk_init_form', 'uncheckValue' => null)); ?>
											<?php echo $form->labelEx($model, 'has_website'); ?>
										</div>
										<div class="squaredlabel pull-left">Website</div>
									</section>
								</div>

								<div class="col-xs-6">
									<section class="m-top clearfix">
										<div class="squaredFour pull-left tooltipster" title="Click to display">
											<?php echo $form->checkBox($model, 'has_subject', array('class'=>'chk_init_form', 'uncheckValue' => null)); ?>
											<?php echo $form->labelEx($model, 'has_subject'); ?>
										</div>
										<div class="squaredlabel pull-left">Subject</div>
									</section>
								</div>
							</div>
							<h4 class="title-builder">Select color that match your site</h4>
                                <div class="row">
                                    <div class="col-xs-9">
                                        <div class="input-group colorpicker">
                                            <?php echo $form->textField($model, 'form_color', array('id' => 'form_color', 'maxlength' => 7, 'class' => 'form-control new-control')); ?>
                                            <span class="input-group-addon"><i class="color-whell"></i></span>
                                        </div>
                                    </div>
                                </div>
							<h4 class="title-builder">Select Button position</h4>
							<div class="row">
								<div id="btn-position">
									<!--Option 1: use each radio button same name-->
									<?php
//										echo $form->radioButton($model,'button_position',array('value'=>0,'uncheckValue'=>null)); 
//										echo $form->radioButton($model,'button_position',array('value'=>1,'uncheckValue'=>null));
//										echo $form->radioButton($model,'button_position',array('value'=>2,'uncheckValue'=>null));
//										echo $form->radioButton($model,'button_position',array('value'=>3,'uncheckValue'=>null));
									?>
									<!--Option 2: use radio button list, did find way to set id for each element-->
									<?php
//									echo $form->radioButtonList($model, 'button_position',
//										array(  
//											0 => '<label for="button-left" class="b-left-center choose_position atv"></label>',
//											1 => '<label for="button-left-bottom" class="b-left-bottom choose_position"></label>',
//											2 => '<label for="button-right-bottom" class="b-right-center choose_position"></label>',
//											3 => '<label for="button-right" class="b-right-bottom choose_position"></label>',
//										), 
//										array(
//											'container' =>'', 
//											'template'=>'<div class="col-xs-3">{input}{label}</div>',
//											'separator'=>'',
//											'labelOptions'=>array(),
//											) 
//									);
									?>

									<div class="col-xs-3">
										<?php echo $form->radioButton($model, 'button_position', array('id' => 'button-left', 'value' => CfConst::BTN_CONTACT_POS_LEFT, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'button_position', array('label' => '', 'for' => 'button-left', 'class' => 'b-left-center choose_position')); ?>
									</div>

									<div class="col-xs-3">
										<?php echo $form->radioButton($model, 'button_position', array('id' => 'button-left-bottom', 'value' => CfConst::BTN_CONTACT_POS_BOTTOM_LEFT, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'button_position', array('label' => '', 'for' => 'button-left-bottom', 'class' => 'b-left-bottom choose_position')); ?>
									</div>

									<div class="col-xs-3">
										<?php echo $form->radioButton($model, 'button_position', array('id' => 'button-right-bottom', 'value' => CfConst::BTN_CONTACT_POS_BOTTOM_RIGHT, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'button_position', array('label' => '', 'for' => 'button-right-bottom', 'class' => 'b-right-bottom choose_position')); ?>
									</div>

									<div class="col-xs-3">
										<?php echo $form->radioButton($model, 'button_position', array('id' => 'button-right', 'value' => CfConst::BTN_CONTACT_POS_RIGHT, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'button_position', array('label' => '', 'for' => 'button-right', 'class' => 'b-right-center choose_position')); ?>
									</div>
								</div>
							</div>
							<h4 class="title-builder">Select contact form</h4>
							<div class="row">
								<div id="ctf-type">
									<div class="col-xs-4">
										<?php echo $form->radioButton($model, 'form_style', array('id' => 'contact-form-basic', 'value' => CfConst::CF_STYLE_BASIC, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'form_style', array('label' => 'Basic', 'for' => 'contact-form-basic', 'class' => 'button-ct-form')); ?>
									</div>
									<div class="col-xs-4">
										<?php echo $form->radioButton($model, 'form_style', array('id' => 'contact-form-normal', 'value' => CfConst::CF_STYLE_NORMAL, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'form_style', array('label' => 'Normal', 'for' => 'contact-form-normal', 'class' => 'button-ct-form')); ?>
									</div>
									<div class="col-xs-4">
										<?php echo $form->radioButton($model, 'form_style', array('id' => 'contact-form-freestyle', 'value' => CfConst::CF_STYLE_FREESTYLE, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'form_style', array('label' => 'Freestyle', 'for' => 'contact-form-freestyle', 'class' => 'button-ct-form')); ?>
									</div>
									<div class="col-xs-4 margin-sm-top">
										<?php echo $form->radioButton($model, 'form_style', array('id' => 'contact-form-restaurant', 'value' => CfConst::CF_STYLE_RESTAURANT, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'form_style', array('label' => 'Restaurant', 'for' => 'contact-form-restaurant', 'class' => 'button-ct-form')); ?>
									</div>
									<div class="col-xs-4 margin-sm-top">
										<?php echo $form->radioButton($model, 'form_style', array('id' => 'contact-form-classic', 'value' => CfConst::CF_STYLE_CLASSIC, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'form_style', array('label' => 'Classic', 'for' => 'contact-form-classic', 'class' => 'button-ct-form')); ?>
									</div>
									<div class="col-xs-4 margin-sm-top">
										<?php echo $form->radioButton($model, 'form_style', array('id' => 'contact-form-child', 'value' => CfConst::CF_STYLE_CHILD, 'uncheckValue' => null)); ?>
										<?php echo $form->labelEx($model, 'form_style', array('label' => 'Child', 'for' => 'contact-form-child', 'class' => 'button-ct-form')); ?>
									</div>
								</div>
							</div>
							<h4 class="title-builder">Your recipient email address</h4>
							<div class="form-group">
								<?php echo $form->textField($model, 'recipient_email', array('maxlength' => 128, 'class' => 'form-control', 'placeholder' => 'Eg: recipient@netvietmedia.com')); ?>
                                <?php echo $form->error($model, 'recipient_email'); ?>
								<?php echo CHtml::submitButton('Save your form', array('class' => 'btn btn-primary btn-send-t margin-sm-top', 'style' => 'height: 42px')); ?>
								<span class="center-block getcode margin-md-bottom">
                                    And get your code
                                    <?php if (Yii::app()->user->isGuest):?>
                                    <div class="error-message" style="margin-top: 10px">Note: You must login to save your form!</div>
                                    <?php endif;?>
                                    <?php if ( isset($linkPremium) && $linkPremium ):?>
                                        <br/>
                                        <a href="<?php echo ABS_BASE_URL.$linkPremium; ?>" class="btn btn-lg"><strong>Config Premium Feature</strong></a>
                                    <?php endif;?>
                                </span>
							</div>
						</div>

						<div class="col-xs-8">
							<div id="my-custom" class="clearfix">
								<div id="wrap-contact">
									<div class="row">
										<div class="col-xs-10 col-xs-offset-1" id="wrap-demo">
<!--											<div id="ctf-demo">
												<div class="ctf-wrapper">
													<div class="ctf-content">
														<div class="ctf-head-title corlor">
															<div class="ctf-logo">
                                                                <img id="custom_logo_image" src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
															</div>
															<h2>Contact Us</h2>
														</div>
														<div class="ctf-inner-content">
															<div id="ctf-contact-form" class="ctf-contact-form clearfix">
																<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
																<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
																<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
																<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
																<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
																<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
																<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
																<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
																<span class="ctf-tel ctf-orientation" id="ctf-tel">Tel: 0168 869 455</span>
																<div class="ctf-form-img ctf-right">
																	<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>-->
										</div>
									</div>
								</div>
								<div id="preview-on">
									<div class="row">
										<div class="col-xs-8 col-xs-offset-1">
											<div class="form-group margin-bottom-remove">
												<label class="col-xs-4 control-label" style="margin-top: 5px">Preview on your site</label>
												<div class="col-xs-8">
													<input type="email" class="form-control center-block" placeholder="http://www.your domain.com" >
												</div>
											</div>
										</div>
										<div class="col-xs-2">
											<?php echo CHtml::button('Preview', array('class' => 'btn btn-primary btn-send-t')); ?>
										</div>
									</div>

									<div class="row m-top">
										<div class="col-xs-8 col-xs-offset-1">
											<div class="form-group margin-bottom-remove">
												<label class="col-xs-4 control-label" style="margin-top: 5px">Your Logo link</label>
												<div class="col-xs-8">
                                                    <?php echo $form->textField($model, 'custom_logo_link', array('class' => 'form-control center-block posedo', 'placeholder' => 'Your logo link')); ?>
                                                    <img src="<?php echo REL_BASE_URL; ?>/library/img/ajax-loader.gif" alt="" id="loader" />
                                                    <?php echo $form->error($model, 'custom_logo_link'); ?>
												</div>
											</div>
										</div>
										<div class="col-xs-2">
											<?php echo CHtml::button('Submit', array('id'=>'submit_logo_link', 'class' => 'btn btn-primary btn-send-t')); ?>
										</div>
									</div>

									<div class="row m-top">
										<div class="col-xs-8 col-xs-offset-1">
											<div class="form-group margin-bottom-remove">
												<label class="col-xs-4 control-label" style="margin-top: 5px">Your extra info</label>
												<div class="col-xs-8">
													<?php echo $form->textField($model, 'extra_info', array('id'=>'extra_info','maxlength' => 128, 'class' => 'form-control center-block', 'placeholder' => 'Tel: 0164 966 889')); ?>
												</div>
											</div>
										</div>
										<div class="col-xs-2">
											<?php echo CHtml::button('Insert', array('id'=>'insert_extra_info','class' => 'btn btn-primary btn-send-t')); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        <?php $this->endWidget(); ?>
	</div>
</div>
<script src="<?php echo REL_BASE_URL; ?>/library/js/classic/cf_render.js"></script>
<script src="<?php echo REL_BASE_URL; ?>/library/js/classic/form-builder.js"></script>