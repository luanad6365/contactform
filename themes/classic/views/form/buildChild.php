<!-- Custom styles for this template - display responsive -->
<link href="<?php echo REL_BASE_URL; ?>/library/css/non-responsive.css" rel="stylesheet">
<link href="<?php echo REL_BASE_URL; ?>/library/css/classic/form-builder.css" rel="stylesheet">

<div id="my-contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 margin-md-top-bottom">
                <h1 class="title-contact-page text-center margin-top-bottom-remove">Create Your Own Contact Form</h1>
            </div>
            <div class="clearfix"></div>
            <div id="own-contact-form" class="clearfix">
                <div class="col-xs-12">
                    <div class="row">
                        <div id="my-builder" class="col-xs-4">
                            
                        </div>
                        <div class="col-xs-8">
                            <div id="my-custom" class="clearfix">
                                <div id="wrap-contact">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1">
                                            <div id="ctf-demo-child">
                                                <div class="ctf-wrapper">
                                                    <div class="ctf-content">
                                                        <div class="ctf-head-title">
                                                            <div class="ctf-logo">
                                                                <img id="custom_logo_image" src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
                                                            </div>
                                                            <h2>Contact Us</h2>
                                                        </div>
                                                        <div class="ctf-inner-content">
                                                            <div id="ctf-contact-form" class="ctf-contact-form clearfix">
                                                                <div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
                                                                    <div for="name" class="ctf-tooltip"><span>Please enter valid name.</span></div>
                                                                    <input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
                                                                </div>
                                                                <div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
                                                                    <div for="email" class="ctf-tooltip"><span>Please enter valid email.</span></div>
                                                                    <input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
                                                                </div>
                                                                <div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
                                                                    <div for="phone" class="ctf-tooltip"><span>Please enter your phone.</span></div>
                                                                    <input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
                                                                </div>
                                                                <div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
                                                                    <div for="company" class="ctf-tooltip"><span>Please enter company.</span></div>
                                                                    <input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
                                                                </div>
                                                                <div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
                                                                    <div for="website" class="ctf-tooltip"><span>Please enter your website.</span></div>
                                                                    <input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
                                                                </div>
                                                                <div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
                                                                    <div for="subject" class="ctf-tooltip"><span>Please enter subject.</span></div>
                                                                    <input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
                                                                </div>
                                                                <div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
                                                                    <div for="message" class="ctf-tooltip"><span>Please enter message.</span></div>
                                                                    <textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
                                                                </div>
                                                                <div class="ctf-form-group ctf-f-size ctf-orientation">
                                                                    <div class="ctf-dropdown ctf-field-f ctf-f-size ctf-orientation">
                                                                        <span class="ctf-department">Sale Support</span>
                                                                        <ul class="ctf-department-dropdown ctf-hidden">
                                                                            <li class="ctf-department-item">Sale Support</li>
                                                                            <li class="ctf-department-item">Technical Support</li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
                                                                <img id="ctf-send-loader" class="ctf-orientation ctf-send-loader" src="<?php echo REL_BASE_URL; ?>/library/img/send-loader.gif" alt="send-loader"/>
                                                                <span class="ctf-tel ctf-orientation">Tel: 0168 869 455</span>
                                                                <div class="ctf-form-img ctf-right">
                                                                    <img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="preview-on">
                                    <div class="row">
                                        <div class="col-xs-8 col-xs-offset-1">
                                            <div class="form-group margin-bottom-remove">
                                                <label class="col-xs-4 control-label" style="margin-top: 5px">Preview on your site</label>
                                                <div class="col-xs-8">
                                                    <input type="email" class="form-control center-block" placeholder="http://www.your domain.com" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <button type="submit" class="btn btn-primary btn-send-t">Preview</button>
                                        </div>
                                    </div>

                                    <div class="row m-top">
                                        <div class="col-xs-8 col-xs-offset-1">
                                            <div class="form-group margin-bottom-remove">
                                                <label class="col-xs-4 control-label" style="margin-top: 5px">Your Logo link</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control center-block" placeholder="Your logo link" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <button type="submit" class="btn btn-primary btn-send-t">Submit</button>
                                        </div>
                                    </div>

                                    <div class="row m-top">
                                        <div class="col-xs-8 col-xs-offset-1">
                                            <div class="form-group margin-bottom-remove">
                                                <label class="col-xs-4 control-label" style="margin-top: 5px">Your extra info</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control center-block" placeholder="Tel: 0164 966 889" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <button type="submit" class="btn btn-primary btn-send-t">Insert</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $(document).ready(function(){
            $(".ctf-dropdown").on('click', function(e){
                e.stopPropagation();
                $(this).find(".ctf-department-dropdown").toggleClass("ctf-hidden");
            });

            $(".ctf-department-item").on('click', function(e){
                e.preventDefault();
                var val_department_item = $(this).text();
                var val_select = $(".ctf-department");
                val_select.text(val_department_item);
            });
        });

        $(document).on('click', function(){
            $(".ctf-department-dropdown").addClass('ctf-hidden');
        });
    });
</script>