<link href="<?php echo REL_BASE_URL; ?>/library/css/classic/form-builder.css" rel="stylesheet">

<script src="<?php echo REL_BASE_URL; ?>/library/js/classic/form_messages.js"></script>
<script>
    //var data_collect = {'form_language':'','has_message':'1','has_name':'1','has_email':'1','has_phone':'1','has_company':'1','has_website':'1','has_subject':'1','form_color':'#ff0000','button_position':'bottom_left','form_style':'','recipient_email':'','custom_logo_link':'http://www.w3schools.com/images/compatible_chrome.gif','extra_info':'Luan','form_id':'1'};
    //CF_HTML.buildForm(data_collect, '', null, Cf_Define.formStyle.demo);
	jQuery(document).ready(function (){
		jQuery('#btn_save').val(CF_FORM_MESSAGE.EN.invalid_name);
	});
</script>
<script src="<?php echo REL_BASE_URL; ?>/ckeditor/ckeditor.js"></script>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'build-form',
            'enableClientValidation'=>false,
            'enableAjaxValidation'=>true,
            'errorMessageCssClass'=>'error-message', //alert alert-error
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnType'=>false,
                'validateOnChange'=>false,
            ),
        )); ?>
<?php echo $form->textArea($model, 'content', array('id'=>'content','maxlength' => 128, 'class' => 'ckeditor')); ?>
<?php echo CHtml::submitButton('Save your form', array('id'=>'btn_save', 'class' => 'btn btn-primary btn-send-t margin-sm-top', 'style' => 'height: 42px')); ?>
<?php $this->endWidget(); ?>


<!--
<div id="ctf-demo">
    <div class="ctf-wrapper">
        <div class="ctf-content">
            <div class="ctf-head-title corlor">
                <div class="ctf-logo">
                    <img id="custom_logo_image" src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
                </div>
                <h2>Contact Us</h2>
            </div>
			<div class="ctf-inner-content">
				<div id="ctf-contact-form" class="ctf-contact-form clearfix">
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="name" class="ctf-tooltip"><span>Please enter valid name.</span></div>
						<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="email" class="ctf-tooltip"><span>Please enter valid email.</span></div>
						<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="phone" class="ctf-tooltip"><span>Please enter your phone.</span></div>
						<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="company" class="ctf-tooltip"><span>Please enter company.</span></div>
						<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="website" class="ctf-tooltip"><span>Please enter your website.</span></div>
						<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="subject" class="ctf-tooltip"><span>Please enter subject.</span></div>
						<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
					</div>
					<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
						<div for="message" class="ctf-tooltip"><span>Please enter message.</span></div>
						<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
					</div>
					<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
					<span class="ctf-tel ctf-orientation" id="ctf-tel">Tel: 0168 869 455</span>
					<div class="ctf-form-img ctf-right">
						<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

<div id="ctf-demo-classic">
	<div class="ctf-wrapper">
		<div class="ctf-content">
			<div class="ctf-head-title">
				<div class="ctf-logo">
					<img src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
				</div>
				<h2>Contact Us</h2>
			</div>
			<div class="ctf-inner-content">
				<div id="ctf-contact-form" class="ctf-contact-form clearfix">
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="name" class="ctf-tooltip"><span>Please enter valid name.</span></div>
						<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="email" class="ctf-tooltip"><span>Please enter valid email.</span></div>
						<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="phone" class="ctf-tooltip"><span>Please enter your phone.</span></div>
						<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="company" class="ctf-tooltip"><span>Please enter company.</span></div>
						<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="website" class="ctf-tooltip"><span>Please enter your website.</span></div>
						<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="subject" class="ctf-tooltip"><span>Please enter subject.</span></div>
						<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
					</div>
					<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
						<div for="message" class="ctf-tooltip"><span>Please enter message.</span></div>
						<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
					</div>
					<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
					<span class="ctf-tel ctf-orientation">Tel: 0168 869 455</span>
					<div class="ctf-form-img ctf-right">
						<img src="<?php echo REL_BASE_URL; ?>/library/img/completed.png" alt=""/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="ctf-demo-normal">
	<div class="ctf-wrapper">
		<div class="ctf-content">
			<div class="ctf-head-title">
				<div class="ctf-logo">
					<img src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
				</div>
				<h2>Contact Us</h2>
			</div>
				<div class="ctf-inner-content">
					<div id="ctf-contact-form" class="ctf-contact-form clearfix">
						<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
							<div for="name" class="ctf-tooltip"><span>Please enter valid name.</span></div>
							<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
						</div>
						<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
							<div for="email" class="ctf-tooltip"><span>Please enter valid email.</span></div>
							<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
						</div>
						<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
							<div for="phone" class="ctf-tooltip"><span>Please enter your phone.</span></div>
							<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
						</div>
						<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
							<div for="company" class="ctf-tooltip"><span>Please enter company.</span></div>
							<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
						</div>
						<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
							<div for="website" class="ctf-tooltip"><span>Please enter your website.</span></div>
							<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
						</div>
						<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
							<div for="subject" class="ctf-tooltip"><span>Please enter subject.</span></div>
							<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
						</div>
						<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
							<div for="message" class="ctf-tooltip"><span>Please enter message.</span></div>
							<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
						</div>
						<div class="ctf-orientation ctf-wrap-btn">
							<button class="ctf-button corlor" type="button"><span class="ctf-text-fly">Send</span></button>
						</div>
					</div>
				</div>
		</div>
		<div class="bg-normal-form">
			<span class="ctf-tel">Tel: 0168 869 455</span>
		</div>
	</div>
</div>

<div id="ctf-demo-style">
	<div class="ctf-wrapper">
		<div class="ctf-content">
			<div class="ctf-head-title">
				<div class="ctf-logo">
					<img id="custom_logo_image" src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
				</div>
				<h2>Contact Us</h2>
			</div>
			<div class="ctf-inner-content">
				<div id="ctf-contact-form" class="ctf-contact-form clearfix">
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="name" class="ctf-tooltip"><span>Please enter valid name.</span></div>
						<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="email" class="ctf-tooltip"><span>Please enter valid email.</span></div>
						<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="phone" class="ctf-tooltip"><span>Please enter your phone.</span></div>
						<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="company" class="ctf-tooltip"><span>Please enter company.</span></div>
						<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="website" class="ctf-tooltip"><span>Please enter your website.</span></div>
						<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="subject" class="ctf-tooltip"><span>Please enter subject.</span></div>
						<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
					</div>
					<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
						<div for="message" class="ctf-tooltip"><span>Please enter message.</span></div>
						<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
					</div>
					<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
					<span class="ctf-tel ctf-orientation">Tel: 0168 869 455</span>
					<div class="ctf-form-img ctf-right">
						<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="ctf-demo-restaurant">
	<div class="ctf-wrapper">
		<div class="ctf-content">
			<div class="ctf-head-title">
				<div class="ctf-logo">
					<img src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
				</div>
				<h2>Contact Us</h2>
			</div>
			<div class="ctf-inner-content">
				<div id="ctf-contact-form" class="ctf-contact-form clearfix">
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="name" class="ctf-tooltip"><span>Please enter valid name.</span></div>
						<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="email" class="ctf-tooltip"><span>Please enter valid email.</span></div>
						<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="phone" class="ctf-tooltip"><span>Please enter your phone.</span></div>
						<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="company" class="ctf-tooltip"><span>Please enter company.</span></div>
						<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="website" class="ctf-tooltip"><span>Please enter your website.</span></div>
						<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="subject" class="ctf-tooltip"><span>Please enter subject.</span></div>
						<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
					</div>
					<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
						<div for="message" class="ctf-tooltip"><span>Please enter message.</span></div>
						<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
					</div>
					<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
					<span class="ctf-tel ctf-orientation">Tel: 0168 869 455</span>
					<div class="ctf-form-img ctf-right">
						<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="ctf-demo-child">
	<div class="ctf-wrapper">
		<div class="ctf-content">
			<div class="ctf-head-title">
				<div class="ctf-logo">
					<img id="custom_logo_image" src="<?php echo REL_BASE_URL; ?>/library/img/contact-logo.png" alt=""/>
				</div>
				<h2>Contact Us</h2>
			</div>
			<div class="ctf-inner-content">
				<div id="ctf-contact-form" class="ctf-contact-form clearfix">
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="name" class="ctf-tooltip"><span>Please enter valid name.</span></div>
						<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="email" class="ctf-tooltip"><span>Please enter valid email.</span></div>
						<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="phone" class="ctf-tooltip"><span>Please enter your phone.</span></div>
						<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="company" class="ctf-tooltip"><span>Please enter company.</span></div>
						<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="website" class="ctf-tooltip"><span>Please enter your website.</span></div>
						<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
					</div>
					<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
						<div for="subject" class="ctf-tooltip"><span>Please enter subject.</span></div>
						<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
					</div>
					<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
						<div for="message" class="ctf-tooltip"><span>Please enter message.</span></div>
						<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
					</div>
					<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
					<span class="ctf-tel ctf-orientation">Tel: 0168 869 455</span>
					<div class="ctf-form-img ctf-right">
						<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
-->