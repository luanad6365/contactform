<link href="<?php echo REL_BASE_URL; ?>/library/css/define.css" rel="stylesheet">
<style>
    .input-error{
        border-color: #a94442;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
</style>
<div id="feature">
    <div class="container">
        <div class="form2-wrap margin-top-30">
            <div class="text-center">
                <h5 class="form2-line-title">Config premium feature for contact form</h5>
            </div>
            <div class="inner-config-wrap">
                <?php
                $this->widget('ext.jqrelcopy.JQRelcopy',array(
                    //the id of the 'Copy' link in the view, see below.
                    'id' => 'copylink',
                    //add a icon image tag instead of the text
                    //leave empty to disable removing
                    'removeText' => 'Remove',
                    //htmlOptions of the remove link
                    'removeHtmlOptions' => array('class'=>'form-group btn btn-danger feature_multi'),
                    //options of the plugin, see http://www.andresvidal.com/labs/relcopy.html
                    'options' => array(
                        //A class to attach to each copy
                        'copyClass'=>'newrow',
                        // The number of allowed copies. Default: 0 is unlimited
                        'limit'=>0,
                        //Option to clear each copies text input fields or textarea
                        'clearInputs'=>true,
                        //A jQuery selector used to exclude an element and its children
                        'excludeSelector'=>'.skipcopy',
                        //Additional HTML to attach at the end of each copy.
                        //'append'=>CHtml::tag('span',array('class'=>'hint'),'You can remove this line'),
                        //LuanAD customize : remove class of element when clone (NOTE, after that, DELETE all in assets folder)
                        'removeClass' => 'input-error',
                        //LuanAD customize : class or id of element wrap <a>-remove
                        'wrapRemoveButton' => '.wrap-remove-button',
                    )
                ))

                ?>
                <?php echo CHtml::beginForm(); ?>
                <div class="form-group checkbox">
                    <div class="row">
                        <div class="col-md-12">
                            <!--<input type="checkbox" name="department" id="department" class="css-checkbox" checked="">-->
                            <?php echo CHtml::checkBox('enable_multi_department', $enableMultiDept ? true : false, array('class' => 'css-checkbox'));?>
                            <!--<label for="department" class="css-label text-black"><b>Enable Multi Department</b></label>-->
                            <?php echo CHtml::label('<b>Enable Multi Department</b>','enable_multi_department', array('class' => 'css-label text-black')); ?>
                            <?php echo Chtml::hiddenField('hid_multi_department'); //Use support when uncheck checkbox, form disable, cant get value of $_POST?>
<!--                            <label>-->
<!--                                --><?php //echo CHtml::checkBox('enable_multi_department', $enableMultiDept ? true : false);?>
<!--                                --><?php //echo CHtml::label('Enable Multi Department','enable_multi_department'); ?>
<!--                                --><?php //echo Chtml::hiddenField('hid_multi_department'); //Use support when uncheck checkbox, form disable, cant get value of $_POST?>
<!--                            </label>-->
                        </div>
                    </div>
                </div>
                <?php if( !empty($dataDept) && count($dataDept) > 0): ?>
                    <?php $num = 1; ?>
                    <?php foreach ($dataDept as $k => $v): ?>
                        <div class="form-group duplicate">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <span class="input-group-addon my-addon"><?php echo CHtml::label('Name','', array('class'=>'margin-bottom-0')); ?></span>
                                        <?php echo CHtml::textField('department_name['.$k.']', $v->departmentName, array('id'=>'department_name', 'maxlength'=>255, 'class' => 'feature_multi department_name form-control my-f-cotrol')); ?>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <span class="input-group-addon my-addon"><?php echo CHtml::label('Email','', array('class'=>'margin-bottom-0')); ?></span>
                                        <?php echo CHtml::textField('department_email['.$k.']', $v->departmentEmail, array('id'=>'department_email', 'maxlength'=>255, 'class' => 'feature_multi department_email form-control my-f-cotrol')); ?>
                                    </div>
                                </div>
                                <div class="col-md-2 wrap-remove-button">
                                    <?php if( $num > 1 ): ?>
                                        <a href="#" onclick="$(this).parent().parent().parent().remove(); return false;" class="form-group btn btn-danger feature_multi">Remove</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php $num++; ?>
                    <?php endforeach; ?>
                <?php else:?>
                    <div class="form-group duplicate">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon my-addon"><?php echo CHtml::label('Name','', array('class'=>'margin-bottom-0')); ?></span>
                                    <?php echo CHtml::textField('department_name[1]','', array('id'=>'department_name', 'maxlength'=>255, 'class' => 'feature_multi department_name form-control my-f-cotrol')); ?>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon my-addon"><?php echo CHtml::label('Email','', array('class'=>'margin-bottom-0')); ?></span>
                                    <?php echo CHtml::textField('department_email[1]','', array('id'=>'department_email', 'maxlength'=>255, 'class' => 'feature_multi department_email form-control my-f-cotrol')); ?>
                                </div>
                            </div>
                            <div class="col-md-2 wrap-remove-button">

                            </div>
                        </div>
                    </div>
                <?php endif;?>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <a id="copylink" class="btn btn-success feature_multi" href="#" rel=".duplicate">More department</a>
                        </div>
                    </div>
                </div>

                <div class="form-group checkbox">
                    <div class="row">
                        <div class="col-md-12">
                            <!--<input type="checkbox" name="Respond" id="Respond" class="css-checkbox" checked="">-->
                            <?php echo CHtml::checkBox('enable_autorespond', $enableAutoRespond ? true : false, array('class' => 'css-checkbox'));?>
                            <!--<label for="Respond" class="css-label text-black"><b>Enable Auto Respond</b></label>-->
                            <?php echo CHtml::label('<b>Enable Auto Respond</b>','enable_autorespond', array('class' => 'css-label text-black')); ?>
                            <?php echo Chtml::hiddenField('hid_autorespond'); //Use support when uncheck checkbox, form disable, cant get value of $_POST?>
<!--                            <label>-->
<!--                                --><?php //echo CHtml::checkBox('enable_autorespond', $enableAutoRespond ? true : false);?>
<!--                                --><?php //echo CHtml::label('Enable Auto Respond','enable_autorespond'); ?>
<!--                                --><?php //echo Chtml::hiddenField('hid_autorespond'); //Use support when uncheck checkbox, form disable, cant get value of $_POST?>
<!--                            </label>-->
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon my-addon"><?php echo CHtml::label('Autorespond Message','', array('class'=>'margin-bottom-0')); ?></span>
                                <!--Use text area then cant validate with CF_VALIDATE-> change to text box-->
                                <?php echo CHtml::textField('autorespond_message', isset($dataAuto->autorespondMessage) ? $dataAuto->autorespondMessage : '', array('maxlength'=>2000, 'class' => 'form-control my-f-cotrol')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary', 'id' => 'submit_feature')); ?>
                <?php echo CHtml::endForm(); ?>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo REL_BASE_URL; ?>/library/js/classic/cf_render.js"></script>
<script src="<?php echo REL_BASE_URL; ?>/library/js/classic/form-premium.js"></script>



