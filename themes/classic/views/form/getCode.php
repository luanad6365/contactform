<div id="feature" class="bg-getcode">
    <div class="container">
        <div class="row">
            <div class="col-md-12 margin-md-top">
                <?php echo CHtml::link('Manage my form', array('userForm/'), array('class' => 'btn btn-primary')); ?>	
                <?php echo CHtml::link('Edit this form', array('form/editForm/' . Yii::app()->request->getQuery('id')), array('class' => 'btn btn-primary')); ?>	
                <?php echo CHtml::link('Build new form', array('form/build'), array('class' => 'btn btn-primary')); ?>	
                <h3>1. Copy your contact form code below</h3>
                <pre style="text-align: left"><code><?php echo htmlentities($data); ?></code></pre>
                <h3>2. Insert the code before the <?php echo htmlentities('</body>'); ?> tag on all pages on your website</h3>
                <h3>3. Test your contact form to see if it's working</h3>
            </div>
        </div>
    </div>
</div>

