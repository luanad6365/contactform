<?php 
	$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Sign up");
?>

<?php if(Yii::app()->user->hasFlash('registration')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>
<?php else: ?>

<div class="form">
<?php $form=$this->beginWidget('UActiveForm', array(
	'id'=>'registration-form',
	'enableAjaxValidation'=>true,
	'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
	'errorMessageCssClass'=>'error-message', //alert alert-error
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'validateOnType'=>false,
		'validateOnChange'=>false,
	),
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>
		
<div id="feature">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel">
                    <div class="panel-heading margin-sm-top-bottom">
                        <img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/logo-form.png" alt=""/>
                    </div>
                    <div class="panel-body my-form-bd">
						<?php //echo $form->errorSummary(array($model,$profile)); ?>
						<?php echo $form->error($model,'email'); ?>
						<?php echo $form->error($model,'password'); ?>
						<?php echo $form->error($model,'verifyPassword'); ?>
						<?php
							$profileFields = $profile->getFields();
							if ($profileFields) {
								foreach ($profileFields as $field) {
									echo $form->error($profile, $field->varname);
								}
							}		
						?>
						<fieldset>
							<?php
								if ($profileFields) :
									foreach ($profileFields as $field) :
							?>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon my-addon"><i class="glyphicon glyphicon-user"></i></span>
										<?php  //echo $form->labelEx($profile, $field->varname); ?>
										<?php
											if ($widgetEdit = $field->widgetEdit($profile)) {
												echo $widgetEdit;
											} elseif ($field->range) {
												echo $form->dropDownList($profile, $field->varname, Profile::range($field->range));
											} elseif ($field->field_type == "TEXT") {
												echo $form->textArea($profile, $field->varname, array('rows' => 6, 'cols' => 50));
											} else {
												echo $form->textField($profile, $field->varname, array('class'=>'form-control my-f-cotrol', 'placeholder'=> $profile->getAttributeLabel($field->varname), 'size' => 60, 'maxlength' => (($field->field_size) ? $field->field_size : 255)));
											}
										?>
								</div>
							</div>
							<?php
									endforeach;
								endif;
							?>

							<?php //echo $form->textField($model,'username'); ?>
							<?php //echo $form->error($model,'username'); ?>

							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon my-addon"><i class="glyphicon glyphicon-envelope"></i></span>
									<?php echo $form->textField($model,'email', array('class'=>'form-control my-f-cotrol', 'placeholder'=>'Your email address')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon my-addon"><i class="glyphicon glyphicon-lock"></i></span>
									<?php echo $form->passwordField($model,'password', array('class'=>'form-control my-f-cotrol', 'placeholder'=>'Password')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon my-addon"><i class="glyphicon glyphicon-lock"></i></span>
									<?php echo $form->passwordField($model,'verifyPassword', array('class'=>'form-control my-f-cotrol', 'placeholder'=>'Confirm password')); ?>
								</div>
							</div>

							<?php if (UserModule::doCaptcha('registration')): ?>
							<div class="form-group">
								<div class="input-group">
								<?php echo $form->labelEx($model,'verifyCode'); ?>
								<?php $this->widget('CCaptcha'); ?>
								<?php echo $form->textField($model,'verifyCode'); ?>
								<?php echo $form->error($model,'verifyCode'); ?>
								<p class="hint"><?php echo UserModule::t("Please enter the letters as they are shown in the image above."); ?>
								<br/><?php echo UserModule::t("Letters are not case-sensitive."); ?></p>
								</div>
							</div>
							<?php endif; ?>

							<!-- Change this to a button or input when using this as a form -->
							<?php echo CHtml::submitButton(UserModule::t("Sign up"), array('name'=>'submit', 'class'=>'btn btn-lg btn-login btn-block')); ?>
						</fieldset>
						
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1">
                                <div class="line"><span>Or connect with</span></div>
                            </div>
                        </div>
						
                        <div class="row margin-md-bottom margin-sm-top">
                            <div class="col-xs-6">
                                <a href="<?php echo ABS_BASE_URL; ?>/oauth/index?provider=google" class="example google-plus js-openpopup">Google</a>
                            </div>

                            <div class="col-xs-6">
                                <a href="<?php echo ABS_BASE_URL; ?>/oauth/index?provider=facebook" class="example facebook js-openpopup">Facebook</a>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
<?php endif; ?>
