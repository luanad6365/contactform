<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");?>
<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>
<?php endif; ?>

<div id="feature">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel">
                    <div class="panel-heading margin-sm-top-bottom">
                        <img class="center-block img-responsive" src="<?php echo REL_BASE_URL;?>/library/img/logo-form.png" alt=""/>
                    </div>
                    <div class="panel-body my-form-bd">
                        <?php echo CHtml::beginForm(Yii::app()->request->requestUri,'post',array('role'=>'form')); ?>
                        <!--<form role="form">-->
                            <fieldset>
                                <?php //echo CHtml::errorSummary($model); ?>
								<?php echo CHtml::error($model, 'username', array('class'=>'error-message')); ?>
                                <?php echo CHtml::error($model, 'password', array('class'=>'error-message')); ?>
								<!--For status banned, not active-->
								<?php echo CHtml::error($model, 'status', array('class'=>'error-message')); ?>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon my-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <!--<input type="text" class="form-control my-f-cotrol" placeholder="Your email address">-->
                                        <?php echo CHtml::activeTextField($model,'username', array('placeholder' => 'Your email address', 'class' => 'form-control my-f-cotrol')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon my-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <!--<input type="password" class="form-control my-f-cotrol" placeholder="Password">-->
                                        <?php echo CHtml::activePasswordField($model,'password', array('placeholder' => 'Password', 'class' => 'form-control my-f-cotrol')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="clearfix">
                                        <a class="forgot-pass pull-left" href="<?php echo Yii::app()->getModule('user')->recoveryUrl[0]; ?>">Forgot password?</a>
                                        <!--<a class="forgot-pass pull-left" href="javascript:void(0)">Forgot password?</a>-->
                                        <div class="pull-right">
                                            <span class="an-acc">Don't have an account yet?</span>
                                            <a class="sign-up-now" href="<?php echo Yii::app()->getModule('user')->registrationUrl[0];  ?>">Sign up now</a>
                                        </div>
                                    </div>
                                </div>
                                <?php //echo CHtml::activeCheckBox($model,'rememberMe'); ?>
                                <?php //echo CHtml::activeLabelEx($model,'rememberMe'); ?>
                                <!-- Change this to a button or input when using this as a form -->
                                <!--<input type="submit" class="btn btn-lg btn-login btn-block" name="submit" value="Sign in"/>-->
                                <?php echo CHtml::submitButton(UserModule::t("Sign in"), array('name'=>'submit', 'class'=>'btn btn-lg btn-login btn-block')); ?>
                            </fieldset>
                        <!--</form>-->
                        <?php echo CHtml::endForm(); ?>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1">
                                <div class="line"><span>Or you can sign in using</span></div>
                            </div>
                        </div>
                        <div class="row margin-md-bottom margin-sm-top">
                            <div class="col-xs-6">
                                <a href="<?php echo ABS_BASE_URL; ?>/oauth/index?provider=google" class="example google-plus js-openpopup">Google</a>
                            </div>
							
                            <div class="col-xs-6">
                                <a href="<?php echo ABS_BASE_URL; ?>/oauth/index?provider=facebook" class="example facebook js-openpopup">Facebook</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo REL_BASE_URL;?>/library/js/classic/oauth.js"></script>