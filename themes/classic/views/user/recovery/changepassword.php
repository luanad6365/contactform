<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");?>

<div id="feature">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel">
                    <div class="panel-heading margin-sm-top-bottom">
                        <img class="center-block img-responsive" src="<?php echo REL_BASE_URL;?>/library/img/logo-form.png" alt=""/>
                    </div>
                    <div class="panel-body my-form-bd">
                        <?php echo CHtml::beginForm(Yii::app()->request->requestUri,'post',array('role'=>'form')); ?>
                            <fieldset>
                                <?php //echo CHtml::errorSummary($form); ?>
                                <?php echo CHtml::error($form, 'password', array('class'=>'error-message')); ?>
                                <?php echo CHtml::error($form, 'verifyPassword', array('class'=>'error-message')); ?>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon my-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <?php echo CHtml::activePasswordField($form,'password', array('placeholder' => 'New Password', 'class' => 'form-control my-f-cotrol')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon my-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <?php echo CHtml::activePasswordField($form,'verifyPassword', array('placeholder' => 'Confirm Password', 'class' => 'form-control my-f-cotrol')); ?>
                                    </div>
                                </div>
                                <?php echo CHtml::submitButton(UserModule::t("Change password"), array('name'=>'submit', 'class'=>'btn btn-lg btn-login btn-block')); ?>
                            </fieldset>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
