<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Forgot Password");?>
<div id="feature">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel">
                    <div class="panel-heading">
                        <div class="text-center">
                            <h4>Forgot Password</h4>
                        </div>
                    </div>
                    <?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
                        <div class="text-center">
                            <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
                        </div>
                    <?php else: ?>
                    <?php echo CHtml::beginForm(Yii::app()->request->requestUri,'post',array('role'=>'form')); ?>
                    <div class="panel-body my-form-bd">
                        <fieldset>
                            <?php echo CHtml::error($form, 'login_or_email', array('class'=>'error-message')); ?>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon my-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <?php echo CHtml::activeTextField($form,'login_or_email', array('class' => 'form-control my-f-cotrol', 'placeholder' => 'Your email address')) ?>
                                </div>
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <?php echo CHtml::submitButton(UserModule::t("Restore"), array('class' => 'btn btn-lg btn-login btn-block', 'name' => 'submit')); ?>
                        </fieldset>
                    </div>
                    <?php echo CHtml::endForm(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>  
