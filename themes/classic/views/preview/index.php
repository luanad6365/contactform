<link href="<?php echo REL_BASE_URL; ?>/library/css/classic/form-builder.css" rel="stylesheet">

<div id="banner" class="banner-feature">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">Our contact form</h1>
                    <p class="text-center">The professional contact form give you many style to choose, let's see and feel!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="feature">
<div class="container">
    <div class="row margin-md-top-bottom">
        <div class="item-feat-dt clearfix">
            <div class="col-sm-6">
                <div class="text-center">
                    <h1>Basic</h1>
                </div>
                <div class="feat-detail-content">
                    <div id="ctf-demo" style="padding: 0px">
						<div class="ctf-wrapper">
							<div class="ctf-content">
								<div class="ctf-head-title corlor">
									<div class="ctf-logo">
									</div>
									<h2>Contact Us</h2>
								</div>
								<div class="ctf-inner-content">
									<div id="ctf-contact-form" class="ctf-contact-form clearfix">
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
										</div>
										<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
											<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
										</div>
										<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
										<span class="ctf-tel ctf-orientation" id="ctf-tel"></span>
										<div class="ctf-form-img ctf-right">
											<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
			
			<div class="col-sm-6">
                <div class="text-center">
                    <h1>Classic</h1>
                </div>
                <div class="feat-detail-content">
					<div id="ctf-demo-classic" style="padding: 0px">
						<div class="ctf-wrapper">
							<div class="ctf-content">
								<div class="ctf-head-title">
									<div class="ctf-logo">
									</div>
									<h2>Contact Us</h2>
								</div>
								<div class="ctf-inner-content">
									<div id="ctf-contact-form" class="ctf-contact-form clearfix">
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
										</div>
										<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
											<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
										</div>
										<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
										<span class="ctf-tel ctf-orientation"></span>
										<div class="ctf-form-img ctf-right">
											<img src="<?php echo REL_BASE_URL; ?>/library/img/completed.png" alt=""/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row margin-md-top-bottom">
        <div class="item-feat-dt clearfix">
            <div class="col-sm-6">
                <div class="text-center">
                    <h1>Normal</h1>
                </div>
                <div class="feat-detail-content">
					<div id="ctf-demo-normal" style="padding: 0px">
						<div class="ctf-wrapper">
							<div class="ctf-content">
								<div class="ctf-head-title">
									<div class="ctf-logo">
										
									</div>
									<h2>Contact Us</h2>
								</div>
									<div class="ctf-inner-content">
										<div id="ctf-contact-form" class="ctf-contact-form clearfix">
											<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
												<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
											</div>
											<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
												<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
											</div>
											<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
												<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
											</div>
											<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
												<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
											</div>
											<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
												<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
											</div>
											<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
												<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
											</div>
											<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
												<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
											</div>
											<div class="ctf-orientation ctf-wrap-btn">
												<button class="ctf-button corlor" type="button"><span class="ctf-text-fly">Send</span></button>
											</div>
										</div>
									</div>
							</div>
							<div class="bg-normal-form">
								<span class="ctf-tel"></span>
							</div>
						</div>
					</div>
                </div>
            </div>
			
			<div class="col-sm-6">
                <div class="text-center">
                    <h1>Restaurant</h1>
                </div>
                <div class="feat-detail-content">
					<div id="ctf-demo-restaurant" style="padding: 0px">
						<div class="ctf-wrapper">
							<div class="ctf-content">
								<div class="ctf-head-title">
									<div class="ctf-logo">
										
									</div>
									<h2>Contact Us</h2>
								</div>
								<div class="ctf-inner-content">
									<div id="ctf-contact-form" class="ctf-contact-form clearfix">
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
										</div>
										<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
											<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
										</div>
										<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
										<span class="ctf-tel ctf-orientation"></span>
										<div class="ctf-form-img ctf-right">
											<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row margin-md-top-bottom">
        <div class="item-feat-dt clearfix">
            <div class="col-sm-6">
                <div class="text-center">
                    <h1>Free style</h1>
                </div>
                <div class="feat-detail-content">	
					<div id="ctf-demo-style" style="padding: 0px">
						<div class="ctf-wrapper">
							<div class="ctf-content">
								<div class="ctf-head-title">
									<div class="ctf-logo">
										
									</div>
									<h2>Contact Us</h2>
								</div>
								<div class="ctf-inner-content">
									<div id="ctf-contact-form" class="ctf-contact-form clearfix">
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
										</div>
										<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
											<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
										</div>
										<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
										<span class="ctf-tel ctf-orientation"></span>
										<div class="ctf-form-img ctf-right">
											<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
			
			<div class="col-sm-6">
                <div class="text-center">
                    <h1>Child</h1>
                </div>
                <div class="feat-detail-content">
                    <div id="ctf-demo-child" style="padding: 0px">
						<div class="ctf-wrapper">
							<div class="ctf-content">
								<div class="ctf-head-title">
									<div class="ctf-logo">
										
									</div>
									<h2>Contact Us</h2>
								</div>
								<div class="ctf-inner-content">
									<div id="ctf-contact-form" class="ctf-contact-form clearfix">
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your name" class="ctf-field ctf-field-name ctf-half-size ctf-icon-small ctf-icon-person ctf-orientation" name="name">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your email address" size="50" class="ctf-field ctf-field-email ctf-half-size ctf-icon-small ctf-icon-email ctf-orientation" name="email">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your phone" size="50" class="ctf-field ctf-field-phone ctf-half-size ctf-icon-small ctf-icon-phone ctf-orientation" name="phone">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your company" size="50" class="ctf-field ctf-field-company ctf-half-size ctf-icon-small ctf-icon-bag ctf-orientation" name="company">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Your website" size="50" class="ctf-field ctf-field-website ctf-half-size ctf-icon-small ctf-icon-global ctf-orientation" name="website">
										</div>
										<div class="ctf-form-group ctf-half-size ctf-orientation clearfix">
											<input placeholder="Subject" size="50" class="ctf-field ctf-field-subject ctf-half-size ctf-icon-small ctf-icon-star ctf-orientation" name="subject">
										</div>
										<div class="ctf-form-group ctf-full-size ctf-orientation clearfix">
											<textarea placeholder="Message..." cols="50" class="ctf-field ctf-field-message ctf-full-size ctf-icon-small ctf-icon-pencil ctf-orientation" id="ctf-message-editor" name="message"></textarea>
										</div>
										<button class="ctf-button ctf-orientation corlor" type="button"><span class="ctf-text-fly">Send</span></button>
										<span class="ctf-tel ctf-orientation"></span>
										<div class="ctf-form-img ctf-right">
											<img src="<?php echo REL_BASE_URL; ?>/library/img/boing.png" alt=""/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 margin-md-bottom">
        <div class="get-start">
            <a href="<?php echo ABS_BASE_URL; ?>/form/build"><img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/get-start.png" alt=""/></a>
        </div>
    </div>
</div>
</div>