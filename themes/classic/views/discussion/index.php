<?php //echo $data->name;?>
<?php //echo $data->content;?>

<div id="banner" class="banner-terms">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">
						<?php
						if( isset($data->name) && $data->name):
							echo $data->name;
						else :
							echo 'Discussion';
						endif;
						?>
					</h1>
                    <p class="text-center">
						<?php
						if( isset($data->description) && $data->description):
							echo $data->description;
						else :
							echo 'We would love to hear your feedback to make the contact form better. Please read our FAQ page before you post a comment or send us a private message';
						endif;
						?>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 margin-md-top-bottom">
			<div id="disqus_thread"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
	/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
	var disqus_shortname = 'bettercontactformmedia'; // required: replace example with your forum shortname, SHOULD BE USER NAME ON DISQUS
	var disqus_identifier = '<?php echo CfConst::DISQUS_IDENTIFIER;?>'; //Show comment anywhere identifier embed

	/* * * DON'T EDIT BELOW THIS LINE * * */
	(function() {
		var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
	})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    