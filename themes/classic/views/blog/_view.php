<div class="blog-item-wrap margin-sm-bottom">
	<div class="blog-item-head">
		<?php if($data->image) : ?>
			<a href="<?php echo ABS_BASE_URL; ?>/blog/<?php echo $data->url_slug; ?>">
				<?php echo CHtml::image(CfConst::IMG_DIR_BLOG . $data->image, $data->image, array('class' => 'img-responsive')); ?>
			</a>
		<?php endif; ?>
	</div>
	<div class="blog-title clearfix">
		<h3 class="pull-left"><a href="<?php echo ABS_BASE_URL; ?>/blog/<?php echo $data->url_slug; ?>"><?php echo $data->title; ?></a></h3>
<!--		<div class="btn-group pull-right show-on-blog">
			<a href="#" class="btn btn-default non-radius"><i class="fa fa-eye"></i></a>
			<a href="#" class="btn btn-default non-radius">66</a>
		</div>-->
	</div>
	<div class="blog-content">
		<p>
			<?php echo $data->description; ?>
		</p>
	</div>
	<div class="blog-foot">
		<a class="btn btn-default non-radius m-bottom" href="<?php echo ABS_BASE_URL; ?>/blog?author=<?php echo Url::url_slug($data->author, array('delimiter'=> '-')); ?>">
			<i class="fa fa-user"></i> By <?php echo $data->author; ?>
		</a>
		<a class="btn btn-default non-radius m-bottom" href="<?php echo ABS_BASE_URL; ?>/blog/<?php echo $data->url_slug; ?>">
			<i class="fa fa-comments"></i> Comments(0)
		</a>
<!--		<a class="btn btn-default non-radius m-bottom" href="#">
			<i class="fa fa-tag"></i> Exhibition, torino
		</a>
		<a class="btn btn-default non-radius last-ran m-bottom" href="#">
			<i class="fa fa-random"></i>
		</a>-->
	</div>
	<div class="blog-date-time">
		<span class="b-date"><?php echo date('d', strtotime($data->publish_date));?></span>
		<span class="b-time"><?php echo date('m / Y', strtotime($data->publish_date));?></span>
	</div>
</div>