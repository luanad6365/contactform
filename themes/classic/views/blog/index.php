<div id="banner" class="banner-faqs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">
						<?php
						if( isset($data->name) && $data->name):
							echo $data->name;
						else :
							echo 'Our Blog';
						endif;
						?>	
					</h1>
                    <p class="text-center">
						<?php
						if( isset($data->description) && $data->description):
							echo $data->description;
						endif;
						?>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row margin-sm-top">
        <div class="col-md-8">
			
			<?php
			$this->widget('zii.widgets.CListView', array(
				'dataProvider' => $dataProvider,
				'itemView' => '_view',
				'summaryText' => '',
				'enablePagination' => false
//				'pager' => array(
//					'class' => 'CLinkPager',
//					'header' => '',
//					'firstPageLabel' => '<<',
//					'prevPageLabel' => '<',
//					'nextPageLabel' => '>',
//					'lastPageLabel' => '>>',
//				),
			));
			?>
            <div class="text-center margin-sm-bottom">
                <nav>
					<?php
					$this->widget('CLinkPager', array(
						'header' => '',
						'pages' => $dataProvider->pagination,
						'htmlOptions'=>array('class'=>'pagination blog-pagination margin-top-remove'),
						'nextPageLabel' => '&gt;',
						'prevPageLabel' => '&lt;',
						'selectedPageCssClass'=>'active',
						'lastPageLabel' => '&gt;&gt;',
						'firstPageLabel' => '&lt;&lt;',
//						'hiddenPageCssClass'=>'disabled',
					));
					?>
<!--                    <ul class="pagination blog-pagination margin-top-remove">
                        <li>
                            <a href="#" aria-label="Previous" class="non-radius">
                                <span aria-hidden="true">&larr;</span>
                            </a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li>
                            <a href="#" aria-label="Next" class="non-radius">
                                <span aria-hidden="true">&rarr;</span>
                            </a>
                        </li>
                    </ul>-->
                </nav>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box-sidebar margin-sm-bottom">
                <form id="wrap-search" action="" method="get">
                    <div class="row">
                        <div class="col-xs-12 clearfix">
                            <div class="ipt-search pull-left">
                                <input type="text" name="search" id="txt-search" class="block-search" placeholder="Search" value="<?php echo Yii::app()->request->getQuery('search', '') ?>" />
                            </div>
                            <div class="btn-search pull-right">
                                <button type="submit" id="btn-search" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            		
			<div class="box-sidebar margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Recent Post</h3>
                </div>
                <div class="main-news m-top">
                    <ul class="wrap-news">
						<?php 
							if( isset($lastestPost) && $lastestPost):
								foreach ($lastestPost as $post):
						?>
							<li>
								<div class="row">
									<div class="col-xs-3">
										<!--<img src="<?php echo REL_BASE_URL ?>/library/img/News/GreenTree.jpg" class="img-thumbnail" alt="" />-->
										<?php if($post->image) : ?>
											<?php echo CHtml::image(CfConst::IMG_DIR_BLOG.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$post->image, $post->image, array('class' => 'img-thumbnail' )); ?>
										<?php endif; ?>
									</div>
									<div class="col-xs-9">
										<div class="news-info">
											<h4 class="news-title margin-top-bottom-remove">
												<a href="<?php echo ABS_BASE_URL; ?>/blog/<?php echo $post->url_slug; ?>"><?php echo $post->title; ?></a>
											</h4>
											<span class="news-date"><?php echo date('m / d / Y', strtotime($post->publish_date));?></span>
										</div>
									</div>
								</div>
							</li>
							
						<?php
								endforeach;
							endif;
						?>
                    </ul>
                </div>
            </div>
			
			<div class="box-sidebar  margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Blog category</h3>
                </div>
                <div class="main-news m-top">
                    <ul class="wrap-news">
						<?php 
							if( isset($category) && $category):
								foreach ($category as $cat):
						?>
						
							<li><a href="<?php echo ABS_BASE_URL; ?>/blog?category=<?php echo $cat->url_slug;?>"><?php echo $cat->name;?></a></li>
							
						<?php
						
								endforeach;
							endif;
						?>
                    </ul>
                </div>
            </div>
            
            <div class="box-sidebar  margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Archive</h3>
                </div>
                <div class="main-news m-top">
                    <ul class="wrap-news">
						<?php foreach ($archive as $k => $ar): ?>
						
							<li><a href="<?php echo ABS_BASE_URL; ?>/blog?archive=<?php echo date('m-Y', $ar);?>"><?php echo $k?></a></li>
							
						<?php endforeach; ?>
                    </ul>
                </div>
            </div>
            
<!--            <div class="box-sidebar margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Tag</h3>
                </div>
                <div class="main-tags m-top clearfix">
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag active">angle</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">awersome</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">coffee</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">corporate</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">coffee</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">coffee</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">happiness</a>
                </div>
            </div>-->
            
<!--            <div class="box-sidebar margin-sm-bottom">
                <div class="ads"><img class="img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/placeholder_600x400.svg" alt="" /></div>
            </div>-->
        </div>
    </div>
</div>