<div id="banner" class="banner-faqs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">
						<?php
						if( isset($data->name) && $data->name):
							echo $data->name;
						else :
							echo 'Our Blog';
						endif;
						?>	
					</h1>
                    <p class="text-center">
						<?php
						if( isset($data->description) && $data->description):
							echo $data->description;
						endif;
						?>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row margin-sm-top">
        <div class="col-md-8">
            <div class="blog-item-wrap margin-sm-bottom">
                <div class="blog-item-head">
					<!--<img class="img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/Blog/blog-1.jpg" alt="" />-->
					<?php if($dataDetail->image) : ?>
						<?php echo CHtml::image(CfConst::IMG_DIR_BLOG . $dataDetail->image, $dataDetail->image, array('class' => 'img-responsive')); ?>
					<?php endif; ?>
                </div>
                <div class="blog-title clearfix">
                    <h3 class="pull-left"><?php echo $dataDetail->title; ?></h3> 
                </div>
                <div class="blog-content">
					<?php echo $dataDetail->content; ?>
                </div>
                <div class="blog-foot">
					<a class="btn btn-default non-radius m-bottom" href="<?php echo ABS_BASE_URL; ?>/blog?author=<?php echo Url::url_slug($dataDetail->author); ?>">
                        <i class="fa fa-user"></i> By <?php echo $dataDetail->author; ?>
                    </a>
<!--                    <a class="btn btn-default non-radius m-bottom" href="javascript:void(0)">
                        <i class="fa fa-comments"></i> comments(0)
                    </a>-->
<!--                    <a class="btn btn-default non-radius m-bottom" href="#">
                        <i class="fa fa-tag"></i> Exhibition, torino
                    </a>
                    <a class="btn btn-default non-radius last-ran m-bottom" href="#">
                        <i class="fa fa-random"></i>
                    </a>-->
                </div>
                <div class="blog-date-time">
                    <span class="b-date"><?php echo date('d', strtotime($dataDetail->publish_date));?></span>
                    <span class="b-time"><?php echo date('m / Y', strtotime($dataDetail->publish_date));?></span>
                </div>
                <div class="wrap-comment">
					<div id="disqus_thread"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
<!--            <div class="box-sidebar margin-sm-bottom">
                <form id="wrap-search" action="" method="get">
                    <div class="row">
                        <div class="col-xs-12 clearfix">
                            <div class="ipt-search pull-left">
                                <input type="text" name="search" id="txt-search" class="block-search" placeholder="Search" />
                            </div>
                            <div class="btn-search pull-right">
                                <button type="submit" id="btn-search" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>-->
            
            <div class="box-sidebar margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Recent Post</h3>
                </div>
                <div class="main-news m-top">
                    <ul class="wrap-news">
						<?php 
							if( isset($lastestPost) && $lastestPost):
								foreach ($lastestPost as $post):
						?>
							<li>
								<div class="row">
									<div class="col-xs-3">
										<!--<img src="<?php echo REL_BASE_URL ?>/library/img/News/GreenTree.jpg" class="img-thumbnail" alt="" />-->
										<?php if($post->image) : ?>
											<?php echo CHtml::image(CfConst::IMG_DIR_BLOG.CfConst::THUMB_FOLDER_NAME.CfConst::THUMB_IMG_PREFIX_NAME.$post->image, $post->image, array('class' => 'img-thumbnail' )); ?>
										<?php endif; ?>
									</div>
									<div class="col-xs-9">
										<div class="news-info">
											<h4 class="news-title margin-top-bottom-remove">
												<a href="<?php echo ABS_BASE_URL; ?>/blog/<?php echo $post->url_slug; ?>"><?php echo $post->title; ?></a>
											</h4>
											<span class="news-date"><?php echo date('m / d / Y', strtotime($post->publish_date));?></span>
										</div>
									</div>
								</div>
							</li>
							
						<?php
								endforeach;
							endif;
						?>
                    </ul>
                </div>
            </div>
            
            <div class="box-sidebar  margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Blog category</h3>
                </div>
                <div class="main-news m-top">
                    <ul class="wrap-news">
						<?php 
							if( isset($category) && $category):
								foreach ($category as $cat):
						?>
						
							<li><a href="<?php echo ABS_BASE_URL; ?>/blog?category=<?php echo $cat->url_slug;?>"><?php echo $cat->name;?></a></li>
							
						<?php
						
								endforeach;
							endif;
						?>
                    </ul>
                </div>
            </div>
            
            <div class="box-sidebar  margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Archive</h3>
                </div>
                <div class="main-news m-top">
                    <ul class="wrap-news">
						<?php foreach ($archive as $k => $ar): ?>
						
							<li><a href="<?php echo ABS_BASE_URL; ?>/blog?archive=<?php echo date('m-Y', $ar);?>"><?php echo $k?></a></li>
							
						<?php endforeach; ?>
                    </ul>
                </div>
            </div>
            
<!--            <div class="box-sidebar margin-sm-bottom">
                <div class="sidebar-head-title">
                    <h3 class="margin-top-bottom-remove">Tag</h3>
                </div>
                <div class="main-tags m-top clearfix">
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag active">angle</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">awersome</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">coffee</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">corporate</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">coffee</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">coffee</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">bgd</a>
                    <a href="#" class="btn btn-default non-radius m-bottom blog-tag">happiness</a>
                </div>
            </div>-->
            
<!--            <div class="box-sidebar margin-sm-bottom">
                <div class="ads"><img class="img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/placeholder_600x400.svg" alt="" /></div>
            </div>-->
        </div>
    </div>
</div>

<script type="text/javascript">
	/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
	var disqus_shortname = 'bettercontactformmedia'; // required: replace example with your forum shortname, SHOULD BE USER NAME ON DISQUS
	var disqus_identifier = '<?php echo 'bettercontactformmedia-blog-'.$dataDetail->id;?>'; //Show comment anywhere identifier embed

	/* * * DON'T EDIT BELOW THIS LINE * * */
	(function() {
		var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
	})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>