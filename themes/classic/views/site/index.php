<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<!--<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>-->

<!--Start SLIDE-->
<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>
			<?php
			if( isset($listSlide) && $listSlide):
				foreach ($listSlide as $slide):
			?>
                <!--SLIDE-->
                <li data-transition="random" data-masterspeed="1000">
                    <!-- MAIN IMAGE -->
                    <!--<img src="<?php //echo REL_BASE_URL; ?>/library/img/slide3.jpg"  alt=""  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">-->
                    <?php echo CHtml::image(CfConst::IMG_DIR_SLIDE . $slide->image_background, $slide->image_background, array('data-bgfit' => 'cover', 'data-bgposition' => 'left top', 'data-bgrepeat' => 'no-repeat')); ?>
                    <div class="tp-caption customin customout"
                         data-x="left"
                         data-y="bottom"
                         data-hoffset="30"
                         data-voffset="-25"
                         data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="400"
                         style="z-index: 3">
                        <!--<img src="<?php //echo REL_BASE_URL; ?>/library/img/image-slide1.png" alt=""/>-->
                        <?php echo CHtml::image(CfConst::IMG_DIR_SLIDE . $slide->image, $slide->image); ?>
                    </div>

                    <div class="tp-caption home-banner-tt skewfromrightshort customout"
                         data-x="600"
                         data-y="60"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1700"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 3">The best free
                    </div>

                    <div class="tp-caption home-banner-tt skewfromrightshort customout"
                         data-x="600"
                         data-y="100"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1800"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 3">contact form
                    </div>

                    <div class="tp-caption home-banner-sv lfr"
                         data-x="600"
                         data-y="160"
                         data-speed="800"
                         data-start="1800"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 3">Secure, Customizable, Responsive
                    </div>

                    <div class="tp-caption home-banner-sv lfr"
                         data-x="600"
                         data-y="220"
                         data-speed="800"
                         data-start="1900"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 3">And No Spam
                    </div>

                    <div class="tp-caption home-banner-sv lfr"
                         data-x="600"
                         data-y="280"
                         data-speed="800"
                         data-start="2000"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 3">It's Fast, Free & Easy
                    </div>

                    <div class="tp-caption btn-start customin"
                         data-x="600"
                         data-y="360"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="500"
                         data-start="2500"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 3"><a class="btn btn-primary" href="<?php echo ABS_BASE_URL; ?>/form/build">Get started for free</a>
                    </div>
                </li>
            
			<?php
				endforeach;
			endif;
			?>
        </ul>
    </div>
</div>
<!--End SLIDE-->


<div class="container margin-sm-top">
    <div class="row">
        <div class="col-sm-6">
            <div class="quote-item">
                <div class="quote-img">
                    <img src="<?php echo REL_BASE_URL; ?>/library/img/quote1.png" alt=""/>
                </div>
                <div class="quote-content text-center">
                    An amazing contact form. With 2 minutes setup, now we get more customer messages everyday. 
                    Our customers feel confident to drop us any questions and suggestions they have. This is must 
                    have feature for any websites who love to listen to their customers.
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="quote-item">
                <div class="quote-img">
                    <img src="<?php echo REL_BASE_URL; ?>/library/img/quote2.png" alt=""/>
                </div>
                <div class="quote-content text-center">
                    An amazing contact form. With 2 minutes setup, now we get more customer messages everyday.
                    Our customers feel confident to drop us any questions and suggestions they have. This is must
                    have feature for any websites who love to listen to their customers.
                </div>
            </div>
        </div>
    </div>
</div>
<div id="feature" class="margin-md-top">
    <div id="slogan">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="slg-center text-center">
                        Better Contact Form helps 1.000 websites to increase 35% customer engagement with 45.500 messages are submitted through.
                        What made awesome?
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="feature-item">
                    <div class="feature-img">
                        <img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/feature1.png" alt=""/>
                    </div>
                    <div class="feature-title">
                        <h3 class="text-center">Responsive Design</h3>
                    </div>
                    <div class="feature-content">
                        <p>
                            Your nice responsive contact form allows customers to send message quickly on any devices (smart phone, tablets...)
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="feature-item">
                    <div class="feature-img">
                        <img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/feature2.png" alt=""/>
                    </div>
                    <div class="feature-title">
                        <h3 class="text-center">Never be Spammed</h3>
                    </div>
                    <div class="feature-content">
                        <p>
                            Your nice responsive contact form allows customers to send message quickly on any devices (smart phone, tablets...)
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="feature-item">
                    <div class="feature-img">
                        <img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/feature3.png" alt=""/>
                    </div>
                    <div class="feature-title">
                        <h3 class="text-center">User Friendly</h3>
                    </div>
                    <div class="feature-content">
                        <p>
                            Your nice responsive contact form allows customers to send message quickly on any devices (smart phone, tablets...)
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="feature-item">
                    <div class="feature-img">
                        <img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/feature4.png" alt=""/>
                    </div>
                    <div class="feature-title">
                        <h3 class="text-center">High Conversion Rate</h3>
                    </div>
                    <div class="feature-content">
                        <p>
                            Your nice responsive contact form allows customers to send message quickly on any devices (smart phone, tablets...)
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 margin-md-top-bottom">
                <div class="get-start">
                    <a href="<?php echo ABS_BASE_URL; ?>/form/build"><img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/get-start.png" alt=""/></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="find-out margin-md-top-bottom">
                <h3 class="text-center">95% of customers recommend Better Contact Form <a href="<?php echo ABS_BASE_URL; ?>/features">Find out why &rarr;</a></h3>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function() {
        revapi = jQuery('.tp-banner').revolution(
            {
                delay:6000,
                startwidth:1170,
                startheight:550,
                hideThumbs:10,
                fullWidth:"on",
                forceFullWidth:"on",
                hideArrowsOnMobile:"on",
                hideBulletsOnMobile:"on",
                hideTimerBar:"on",
                navigationStyle:"preview4"
            });
    });
</script>
<script>
    (function(d, s, id) {
        if ('https:' === document.location.protocol || d.getElementById(id)) return;
        var js, fjs = d.getElementsByTagName(s)[0];
        js = d.createElement(s); js.id = id; js.src = "http://contactform.com/client/js/c51ce410c124a10e0db5e4b97fc2af39.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "bcf-render"));</script>
<a id="ctf-butt-contact" class="corlor" href="javascript:void(0)">Contact us</a>