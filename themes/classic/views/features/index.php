<?php
/* @var $this FeaturesController */
?>

<div id="banner" class="banner-feature">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">
						<?php
						if( isset($data->name) && $data->name):
							echo $data->name;
						else :
							echo 'Features';
						endif;
						?>	
					</h1>
                    <p class="text-center">
						<?php
						if( isset($data->description) && $data->description):
							echo $data->description;
						else :
							echo 'The professional contact form let your customers comfortable to drop a message quickly whenever they want';
						endif;
						?>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
	<?php
	//Declare class css
	$stt = 'odd';
	if( isset($listFeature) && $listFeature):
		foreach ($listFeature as $feature):
			if($stt == 'odd'):
	?>
				<div class="row margin-md-top-bottom">
					<div class="item-feat-dt clearfix">
						<div class="col-sm-6">
							<div class="feat-detail-content">
								<h1><?php echo $feature->name;?></h1>
								<p><?php echo $feature->description;?></p>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="feat-detail-img">
								<?php echo CHtml::image(CfConst::IMG_DIR_FEATURES . $feature->image, $feature->image, array('class' => 'img-responsive')); ?>
							</div>
						</div>
					</div>
				</div>
	
			<?php else : ?>
	
				<div class="row margin-md-top-bottom">
					<div class="item-feat-dt feat-dt-second clearfix">
						<div class="col-sm-6">
							<div class="feat-detail-img">
								<?php echo CHtml::image(CfConst::IMG_DIR_FEATURES . $feature->image, $feature->image, array('class' => 'img-responsive')); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="feat-detail-content">
								<h1><?php echo $feature->name;?></h1>
								<p><?php echo $feature->description;?></p>
							</div>
						</div>
					</div>
				</div>
	<?php
			endif;
			
			//Flip odd and even to redeclare css class
			if($stt == 'odd') $stt = 'even'; else $stt = 'odd';
		endforeach;
	endif;
	?>

    <div class="col-xs-12 margin-md-bottom">
        <div class="get-start">
            <a href="<?php echo ABS_BASE_URL; ?>/form/build"><img class="center-block img-responsive" src="<?php echo REL_BASE_URL; ?>/library/img/get-start.png" alt=""/></a>
        </div>
    </div>
</div>