<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo REL_BASE_URL; ?>/admin_library/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo REL_BASE_URL; ?>/admin_library/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo REL_BASE_URL; ?>/admin_library/css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo REL_BASE_URL; ?>/admin_library/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo REL_BASE_URL; ?>/admin_library/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Date Picker http://eternicode.github.io/bootstrap-datepicker/?#sandbox -->
	<link href="<?php echo REL_BASE_URL; ?>/admin_library/css/datepicker.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<!-- jQuery -->
    <script src="<?php echo REL_BASE_URL; ?>/admin_library/js/jquery-1.11.1.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo REL_BASE_URL; ?>/admin_library/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo REL_BASE_URL; ?>/admin_library/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo REL_BASE_URL; ?>/admin_library/js/sb-admin-2.js"></script>
	
	<!-- Date Picker http://eternicode.github.io/bootstrap-datepicker/?#sandbox -->
	<script src="<?php echo REL_BASE_URL; ?>/admin_library/js/bootstrap-datepicker.js"></script>
	
	<style>
		/*Margin for edit button*/
		.fa.fa-pencil, .fa.fa-ban{margin-right: 8px;}
		/*Hide some element in paging*/
		ul.pagination .first, ul.pagination .last {
			display: none;
		}
	</style>

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand" href="<?php echo ABS_BASE_URL; ?>">Go to Contact Form Page</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

			<?php 
			$userModule = Yii::app()->getModule('user');
			//Check item active
            function checkActive($href, $route) {
				foreach ($href as $r){
					if (strpos($r, $route) !== false) {
						return 'active';
					}
				}
				return '';
			}
			?>	
			
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a class="<?php echo checkActive(array($userModule->returnAdminUrl[0]), $this->route );?>" href="<?php echo $userModule->returnAdminUrl[0]; ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><i class="fa fa-sitemap fa-fw"></i> Front-end Static Page<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
                                    <a class="<?php echo checkActive(array('/admin/slide/admin', '/admin/slide/create', '/admin/slide/update'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/slide/admin"><i class="fa fa-image fa-fw"></i> Image Slide</a>
                                </li>
								<li>
									<a href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-fw"></i> Features <span class="fa arrow"></span></a>
                                     <ul class="nav nav-third-level">
                                        <li>
											<a class="<?php echo checkActive(array('/admin/features/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/features"><i class="fa fa-eye fa-fw"></i> Overview</a>
                                        </li>
                                        <li>
											<a class="<?php echo checkActive(array('/admin/features/admin', '/admin/features/create', '/admin/features/update'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/features/admin"><i class="fa fa-list fa-fw"></i> List</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
								<li> 
                                    <a class="<?php echo checkActive(array('/admin/terms/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/terms"><i class="fa fa-check-circle fa-fw"></i> Terms of Service</a>
                                </li>
								<li>
                                    <a class="<?php echo checkActive(array('/admin/privacy/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/privacy"><i class="fa fa-file-text-o fa-fw"></i> Privacy Policy</a>
                                </li>
								<li>
									<a href="javascript:void(0)"><i class="fa fa-group fa-fw"></i> Blog <span class="fa arrow"></span></a>
                                     <ul class="nav nav-third-level">
                                        <li>
											<a class="<?php echo checkActive(array('/admin/blog/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/blog"><i class="fa fa-eye fa-fw"></i> Overview</a>
                                        </li>
                                        <li>
											<a class="<?php echo checkActive(array('/admin/blog/admin', '/admin/blog/create', '/admin/blog/update'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/blog/admin"><i class="fa fa-list fa-fw"></i> List</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
								<li>
									<a href="javascript:void(0)"><i class="fa  fa-comments fa-fw"></i> Discussion <span class="fa arrow"></span></a>
                                     <ul class="nav nav-third-level">
                                        <li>
											<a class="<?php echo checkActive(array('/admin/discussion/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/discussion"><i class="fa fa-eye fa-fw"></i> Overview</a>
                                        </li>
                                        <li>
											<a class="<?php echo checkActive(array('/admin/discussion/admin'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/discussion/admin"><i class="fa fa-list fa-fw"></i> List</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
								<li>  
									<a href="javascript:void(0)"><i class="fa fa-question-circle fa-fw"></i> FAQ <span class="fa arrow"></span></a>
                                     <ul class="nav nav-third-level">
                                        <li>
											<a class="<?php echo checkActive(array('/admin/faq/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/faq"><i class="fa fa-eye fa-fw"></i> Overview</a>
                                        </li>
                                        <li>
											<a class="<?php echo checkActive(array('/admin/faq/admin', '/admin/faq/create', '/admin/faq/update'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/faq/admin"><i class="fa fa-list fa-fw"></i> List</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
								<li>
                                    <a class="<?php echo checkActive(array('/admin/showcase/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/showcase">Showcase</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a class="<?php echo checkActive(array('/admin/manageuser/admin', '/admin/manageuser/create', '/admin/manageuser/update', '/admin/manageuser/view'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/manageUser/"><i class="fa fa-user fa-fw"></i> Manage User</a>
                        </li>
						<li>
                            <a class="<?php echo checkActive(array('/admin/userform/admin', '/admin/userform/create', '/admin/userform/update', '/admin/userform/view'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/userForm/"><i class="fa fa-send fa-fw"></i> Manage Form</a>
                        </li>
						<li>
                            <a class="<?php echo checkActive(array('/admin/datareceive/admin', '/admin/datareceive/single', '/admin/datareceive/update', '/admin/datareceive/view'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/dataReceive/"><i class="fa fa-envelope-o fa-fw"></i> Manage Data Submit</a>
                        </li>
						<li>
                            <a class="<?php echo checkActive(array('/admin/userspayment/admin'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/usersPayment/"><i class="fa fa-dollar fa-fw"></i> Manage User Payment</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><i class="fa fa-cogs fa-fw"></i> Settings<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
									<a href="javascript:void(0)"><i class="fa fa-credit-card fa-fw"></i> Authorize.net Account <span class="fa arrow"></span></a>
                                     <ul class="nav nav-third-level">
                                        <li>
											<a class="<?php echo checkActive(array('/admin/authorize/sandbox'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/authorize/sandbox"><i class="fa fa-credit-card fa-fw"></i> Sandbox</a>
                                        </li>
                                        <li>
											<a class="<?php echo checkActive(array('/admin/authorize/live'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/authorize/live"><i class="fa fa-credit-card fa-fw"></i> Live</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
								<li>
                                    <a class="<?php echo checkActive(array('/admin/paymentconfig/index'), $this->route);?>" href="<?php echo ABS_BASE_URL; ?>/admin/paymentconfig"><i class="fa fa-cog fa-fw"></i> Payment Config</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header"><?php echo CHtml::encode($this->pageName); ?></h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<?php echo $content; ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>

<script>
$(document).ready(function (){
	//Get current a active
	var link_active = $('a.active');
	//find ul parent
	if(link_active.length){
		var ul_parent = link_active.closest('ul');
		//Check if ul is highest parent or nested level
		if(ul_parent.attr('id')){
			//If <a> is level 1
			link_active.parent().addClass('active');
		} else {
			// <a> level 2, 3
			//Add class in for highest ul
			ul_parent.addClass('in');
			//Add class active for li parent
			ul_parent.parent().addClass('active');
			//<a> level 3
			if(ul_parent.hasClass('nav-third-level')){
				//Add class in for highest ul
				ul_parent.parent().closest('ul').addClass('in');
			}
		}
	}
})
</script>


</html>