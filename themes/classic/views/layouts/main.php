<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/library/css/bootstrap.css" />

    <!-- My custom -->
    <link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/library/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/library/css/style.css" />

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/library/rs-plugin/css/settings.css" media="screen" />

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo REL_BASE_URL; ?>/library/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo REL_BASE_URL; ?>/library/js/jquery-scrolltofixed-min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo REL_BASE_URL; ?>/library/js/bootstrap.min.js"></script>
    
    

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script src="<?php echo REL_BASE_URL; ?>/library/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo REL_BASE_URL; ?>/library/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
    <!-- blueprint CSS framework -->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/css/screen.css" media="screen, projection" />-->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/css/print.css" media="print" />-->
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/css/ie.css" media="screen, projection" />
    <![endif]-->

    <!--<link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/css/main.css" />-->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo REL_BASE_URL; ?>/css/form.css" />-->
    
    <!--Facebook embed-->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&appId=220776491434096&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <!--Twitter embed-->
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    
    <!--Google embed-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<!--<div class="container" id="page">-->
<div id="page">    
	<div id="header">
		<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
    <?php 
//    $this->widget('zii.widgets.CMenu', array(
//        'items' => array(
//            array('label' => 'Home', 'url' => array('/site/index')),
//            array('label' => 'About', 'url' => array('/site/page', 'view' => 'about')),
//            array('label' => 'Contact', 'url' => array('/site/contact')),
//            array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
//            array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
//            array('label' => 'Build Form', 'url' => array('/form/builder'), 'visible' => !Yii::app()->user->isGuest),
//            array('label' => 'Manage Form', 'url' => array('/userForm/admin'), 'visible' => !Yii::app()->user->isGuest),
//            array('label' => 'Receives Data', 'url' => array('/dataReceive/admin'), 'visible' => !Yii::app()->user->isGuest),
//        ),
//    ));
    
//    $this->widget('zii.widgets.CMenu', array(
//        'items' => array(
//            array('label' => 'Home', 'url' => array('/site/index')),
//            array('label' => 'About', 'url' => array('/site/page', 'view' => 'about')),
//            array('label' => 'Contact', 'url' => array('/site/contact')),
//            array('label' => 'Login', 'url' => array('/user/login'), 'visible' => Yii::app()->user->isGuest),
//            array('label' => 'Register', 'url' => array('/user/registration'), 'visible' => Yii::app()->user->isGuest),
//            array('label' => 'Profile', 'url' => array('/user/profile'), 'visible' => !Yii::app()->user->isGuest),
//            array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
//        ),
//    ));
    ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php //$this->widget('zii.widgets.CBreadcrumbs', array('links'=>$this->breadcrumbs,)); ?><!-- breadcrumbs -->
	<?php endif?>

    <!--Header-->
    <div id="top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 clearfix">
                    <div id="top-social" class="pull-right">
                        <?php $url = Yii::app()->request->hostInfo . Yii::app()->request->requestUri;?>
                        <a w href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $url; ?>">Tweet</a>
                        <div class="fb-like" data-href="<?php echo $url; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                        <div class="g-plusone" data-size="medium" data-href="<?php echo $url; ?>"></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <nav id="fixed-nav" class="navbar navbar-default non-radius" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand new-brand-logo" href="<?php echo ABS_BASE_URL; ?>"><img src="<?php echo REL_BASE_URL; ?>/library/img/logo-ctform.png" alt="logo"/></a>
            </div>

            
            <?php
            //Check item active
            function isItemActive($route, $id) {
				//explode the route ($route format example: /site/contact)
				$menu = explode("/", $route);
				//If id is route
				if (strpos($id,'/') !== false) {
					$ids = explode("/", $id);
					return ( strtolower($menu[0]) == strtolower($ids[0]) && strtolower($menu[1]) == strtolower($ids[1]) )   ? true : false;
				} else {
					//compare the first array element to the $id passed
					return strtolower($menu[0])  == strtolower($id) ? true : false;
				}
            }
            ?>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu-collapse">
                <?php 
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Features', 'url' => array('/features'), 'active' => isItemActive($this->route, 'features')),
                        array('label' => 'Price', 'url' => array('/price'), 'active' => isItemActive($this->route, 'price')),
                        array('label' => 'Preview form', 'url' => array('/preview'), 'active' => isItemActive($this->route, 'preview')),
                        array('label' => 'My form', 'url' => array('/userForm'), 'visible' => !Yii::app()->user->isGuest, 'active' => isItemActive($this->route, 'userForm') || isItemActive($this->route, 'form') ),
                        array('label' => 'My payment', 'url' => array('/usersPayment'), 'visible' => !Yii::app()->user->isGuest, 'active' => isItemActive($this->route, 'usersPayment')),
						array('label' => 'Manage backend', 'url' => Yii::app()->getModule('user')->returnAdminUrl, 'visible' => Yii::app()->getModule('user')->isAdmin(), ),
                        array('label' => 'Login', 'url' => array('/user/login'), 'visible' => Yii::app()->user->isGuest, 'active' => isItemActive($this->route, 'user/login')),
//                        array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/user/logout'), 'visible' => !Yii::app()->user->isGuest),
                        array(
                            'label' => 'Hi, '.Yii::app()->user->name .' <span class="caret"></span>',
                            'url' => 'javascript:void(0)',
                            'linkOptions' => array(
                                'class' => 'dropdown-toggle',
                                'data-toggle' => 'dropdown',
                                'role' => 'button',
                                'aria-expanded' => 'false'
                            ),
                            'visible' => !Yii::app()->user->isGuest,
                            'itemOptions' => array('class' => 'dropdown'),
                            'items' => array(
                                array(
                                    'label' => 'Change Password',
                                    'url' => array('/user/changePass'),
                                ),
                                array(
                                    'label' => '',
                                    'itemOptions' => array('class' => 'divider'),
                                ),
                                array(
                                    'label' => 'Logout',
                                    'url' => array('/user/logout'),
                                ),
                            )
                        ),
                    ),
                    'htmlOptions' => array(
                        'class' => 'nav navbar-nav navbar-right',
                    ),
                    'submenuHtmlOptions' => array(
                        'class' => 'dropdown-menu',
                    ),
                    'encodeLabel' => false,// For put html tag in label
                ));
                ?>
<!--                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="<?php echo ABS_BASE_URL; ?>/features">Features</a></li>                 
                    <?php if(Yii::app()->user->isGuest) : ?>
                    <li><a href="<?php echo ABS_BASE_URL; ?>/user/login">Login</a></li>
                    <?php else: ?>
                    <li class="dropdown">
                        <a href="<?php echo ABS_BASE_URL; ?>/user/logout" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">John Smith <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Change Password</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo ABS_BASE_URL; ?>/user/logout">Logout</a></li>
                        </ul>
                    </li>
                    <?php endif;?>
                </ul>-->
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
        
    <div id="wrap-layout">
        <?php echo $content; ?>
    </div>
	

	<div class="clear"></div>

    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <span class="copyright">© <?php echo date('Y');?> BetterContactForm</span>
                </div>
                <div class="col-sm-9 clearfix">
					<?php 
					$this->widget('zii.widgets.CMenu', array(
						'items' => array(
							array('label' => 'Terms of Service', 'url' => array('/terms'), 'active' => isItemActive($this->route, 'terms') ),
							array('label' => 'Privacy Policy', 'url' => array('/privacy'), 'active' => isItemActive($this->route, 'privacy') ),
							array('label' => 'Blog', 'url' => array('/blog'), 'active' => isItemActive($this->route, 'blog') ),
							array('label' => 'Discussion', 'url' => array('/discussion'), 'active' => isItemActive($this->route, 'discussion') ),
							array('label' => 'FAQ', 'url' => array('/faq'), 'active' => isItemActive($this->route, 'faq') ),
							//array('label' => 'Showcase', 'url' => array('/showcase'), 'active' => isItemActive($this->route, 'showcase') ),
						),
						'htmlOptions' => array(
							'class' => 'nav nav-pills pull-right',
						),
					));
					?>
					<!--
                    <ul class="nav nav-pills pull-right">
                        <li class="active"><a href="#"></a></li>
                    </ul>
					-->
                </div>
            </div>
        </div>
    </div><!-- footer -->
</div><!-- page -->
<script type="text/javascript">
$(document).ready(function(){
    $("#fixed-nav").scrollToFixed();
});
</script>
</body>
</html>