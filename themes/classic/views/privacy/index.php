<?php //echo $data->name;?>
<?php //echo $data->content;?>

<div id="banner" class="banner-terms">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner-content">
                    <h1 class="text-center">
						<?php
						if( isset($data->name) && $data->name):
							echo $data->name;
						else :
							echo 'Privacy policy';
						endif;
						?>
					</h1>
                    <p class="text-center">
						<?php
						if( isset($data->description) && $data->description):
							echo $data->description;
						else :
							echo 'We support small companies and startups. Contact us for discount';
						endif;
						?>
					</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 margin-md-top-bottom">
            <div id="wrap-privacy">
				<?php
				if( isset($data->content) && $data->content):
					echo $data->content;
				endif;
				?>
<!--                <p>
                    ContactForm is committed to protecting your privacy. We will not trade, sell, or disclose your personal info to any 
                    third parties. We’ll never open/read any submitted information from your customers. If you do not agree with any of the terms 
                    below, you should not use our BetterContactForm service. 
                </p>
                <h3 style="font-size: 16px;font-weight: 700;color: #343434;">Identity & access</h3>
                <p>When you sign up for BetterContactForm, we ask for your name and email address. That's just so you can personalize your new account, and we can send you invoices, updates, or other essential information. We’ll never sell or disclose your personal info to any third parties.   The only times we’ll ever share your info:</p>
                <ul style="list-style: square;margin-left: 30px;font-size: 15px;line-height: 25px;">
                    <li>To provide products or services you've requested, with your permission.</li>
                    <li>To investigate, prevent, or take action regarding illegal activities, suspected fraud, violations of our Terms of Service, or as otherwise required by law.</li>
                    <li>If BetterContactForm is acquired by or merged with another company — we don’t plan on that, but if it happens — we’ll notify you well before any info about you is transferred and becomes subject to a different privacy policy.</li>
                </ul>
                <h3 style="font-size: 16px;font-weight: 700;color: #343434;">Law enforcement</h3>
                <p>
                    BetterContactForm won’t hand your data over to law enforcement unless a court order say we have to. We flat-out reject 
                    requests from local and federal law enforcement when they seek data without a court order. And unless we're legally prevented 
                    from it, we’ll always inform you when such requests are made.
                </p>
                <h3 style="font-size: 16px;font-weight: 700;color: #343434;">Submitted Information</h3>
                <p>
                    You expressly understand and agree that your customers will submit messages via BetterContactForm server. Our web app 
                    also auto-detect some additional information of your customers such as IP, location, referral site, search keyword, 
                    current page URL... All these information will be delivered to your recipient email right after your customer click Send. 
                    These information will be encrypted and logged in our server in 30 days for spam prevention and report purpose. We’ll never 
                    open/read these information. With Pro version, all data is encrypted via SSL/TLS when transmitted from our servers to your browser.
                </p>
                <h3 style="font-size: 16px;font-weight: 700;color: #343434;">Changes & questions</h3>
                <p>
                    BetterContactForm may periodically update this policy — we’ll notify you about significant changes by emailing the account 
                    owner or by placing a prominent notice on our site. You can access, change or delete your personal information at any time. 
                    Questions about this privacy policy? Please leave us a message and we’ll be happy to answer them!
                </p>-->
            </div>
        </div>
    </div>
</div>