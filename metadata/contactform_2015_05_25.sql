/*
Navicat MySQL Data Transfer

Source Server         : localhost_xampp
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : contactform

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2015-05-25 17:09:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` text,
  `author` varchar(255) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL COMMENT 'User_id',
  `category_id` int(11) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `show` tinyint(1) DEFAULT NULL,
  `url_slug` varchar(255) DEFAULT NULL,
  `publish_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('7', 'Clistview ajax pagination - Yii Framework Forum', '1421146197_1420021413_blog-1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Administrator Admin', '1', '1', null, '1', '7-authentic_twee_synth__schlitz_flexitarian', '2015-01-15 10:47:30', '2014-12-31 17:23:33', '2015-01-14 15:46:37');
INSERT INTO `blog` VALUES ('8', 'Authentic twee synth, Schlitz flexitarian', '1421146285_1420021443_blog-2.jpg', 'Clistview ajax pagination - Yii Framework Forum', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n', 'Administrator Admin', '1', '2', null, '1', '8-authentic_twee_synth__schlitz_flexitarian', '2015-01-15 10:47:39', '2014-12-31 17:24:03', '2015-01-14 15:46:45');
INSERT INTO `blog` VALUES ('9', 'Authentic twee synth, Schlitz flexitarian suavecio', '1421146270_1420021471_blog-3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '<p>Suavecito</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Administrator Admin', '1', '3', null, '1', '9-authentic_twee_synth__schlitz_flexitarian_suavecio', '2015-01-14 17:23:53', '2014-12-31 17:24:31', '2015-01-14 15:46:53');
INSERT INTO `blog` VALUES ('10', 'Authentic twee synth, Schlitz flexitarian', '1421146226_1420021522_blog-4.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Administrator Admin', '1', '4', null, '1', '10-authentic_twee_synth__schlitz_flexitarian', '2014-12-24 17:23:54', '2014-12-31 17:25:22', '2015-01-14 15:46:59');
INSERT INTO `blog` VALUES ('11', 'Authentic twee synth, Schlitz flexitarian', '1421146249_1420021567_blog-5.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Administrator Admin', '1', '1', null, '1', '11-authentic_twee_synth__schlitz_flexitarian', '2015-01-14 17:23:56', '2014-12-31 17:26:07', '2015-01-14 15:47:05');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url_slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Mobile', 'mobile');
INSERT INTO `category` VALUES ('2', 'IT News', 'it-news');
INSERT INTO `category` VALUES ('3', 'Web Design', 'web-design');
INSERT INTO `category` VALUES ('4', 'Wordpress', 'wordpress');

-- ----------------------------
-- Table structure for data_receive
-- ----------------------------
DROP TABLE IF EXISTS `data_receive`;
CREATE TABLE `data_receive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `form_style` varchar(50) DEFAULT NULL,
  `form_language` varchar(255) DEFAULT NULL,
  `recipient_email` varchar(255) DEFAULT NULL,
  `from_site` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_phone` varchar(255) DEFAULT NULL,
  `customer_company` varchar(255) DEFAULT NULL,
  `customer_website` varchar(255) DEFAULT NULL,
  `customer_subject` varchar(255) DEFAULT NULL,
  `customer_message` text,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_status` varchar(50) DEFAULT NULL COMMENT 'sent, delete, reply',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of data_receive
-- ----------------------------

-- ----------------------------
-- Table structure for faq
-- ----------------------------
DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(500) DEFAULT NULL,
  `answer` text,
  `order_position` tinyint(2) DEFAULT NULL,
  `show` tinyint(1) DEFAULT NULL COMMENT 'Show or hide on front end',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faq
-- ----------------------------
INSERT INTO `faq` VALUES ('1', 'What is the advantages of Better Contact Form?', '<p>The Better Contact Form allows you to create a professional contact form within a minutes. You can customize your form fields, form color, recipient email and place the embedded code to your website quickly. There is no technical knowledge required here.</p>\r\n', '1', '1');
INSERT INTO `faq` VALUES ('2', 'What platforms does the Better Contact Form support', '<p>The Better Contact Form is designed to work on all website platforms, eg. PHP, .NET, Java, WordPress, Joomla, Drupal, Magento, PrestaShop&hellip;</p>\r\n', '2', '1');
INSERT INTO `faq` VALUES ('3', 'Does the Better Contact Form work on mobile devices?', '<p>Yes, the Better Contact Form is a responsive form that works on all devices like laptop, tablet, smartphone, ipad, iphone, android, window phone&hellip;</p>\r\n', '3', '1');
INSERT INTO `faq` VALUES ('4', 'Does the Better Contact Form slow down my website?', '<p>No, the Better Contact Form uses Javascript asynchronous loading that keep your website loading fast</p>\r\n', '4', '1');
INSERT INTO `faq` VALUES ('5', 'Why doesn’t the Better Contact Form have a CAPTCHA option?', '<p>You hate CAPTCHA, right? With the latest spam protection technology, the Better Contact Form make sure you will never be spammed and your customers will never have to enter a Captcha.</p>\r\n', '5', '1');
INSERT INTO `faq` VALUES ('6', 'My website is using JQuery library. Is there any JS conflict if I include Better Contact Form embedded code in my website?', '<p>No, the Better Contact Form use native JS/CSS code so it will not cause any JS/CSS conflict on your website.</p>\r\n', '6', '1');
INSERT INTO `faq` VALUES ('7', 'Does the Better Contact Form require any other plugins or libraries?', '<p>No, the Better Contact Form does not requires any other plugin/library. It will work smoothly with a small embedded code. For better support, we still provide integration module for popular platform like WordPress, Joomla, Magento, PrestaShop, Drupal.</p>\r\n', '7', '1');
INSERT INTO `faq` VALUES ('8', 'Do I need to sign up to create my contact form?', '<p>No. You can create your contact form without signing up. But for managing your contact forms easier, you should sign up an account.</p>\r\n', '8', '1');
INSERT INTO `faq` VALUES ('9', 'Does the Better Contact Form fit with my custom theme?', '<p>Yes, the Better Contact Form is designed to fit with any custom theme. You also can change the form color easily to match with your color scheme.</p>\r\n', '9', '1');
INSERT INTO `faq` VALUES ('10', 'I don’t want to use the floating “Contact Us” button. How can I activate the contact form popup with my custom link/button?', '<p>You can disable the floating &ldquo;Contact Us&rdquo; in the form builder. To activate the contact form popup, you can add the attribute rel=&rdquo;contactus&rdquo; to your custom link/button.</p>\r\n', '10', '1');
INSERT INTO `faq` VALUES ('11', 'How can I activate the contact form popup automatically after the webpage is loaded?', '<p>You can activate the form popup automatically by adding param #contact after the webpage URL. Example (to show contact form for this page) <a href=\"http://contactform.com:8080/faq#\">http://bettercontactform.net/faq/#contactus</a></p>\r\n', '11', '1');
INSERT INTO `faq` VALUES ('12', 'I have some other questions, how do I get support?', '<p>Feel free to <a href=\"http://contactform.com:8080/faq#\">contact us</a> or please <a href=\"http://contactform.com:8080/faq#\">leave your feedback here</a></p>\r\n', '12', '1');

-- ----------------------------
-- Table structure for features
-- ----------------------------
DROP TABLE IF EXISTS `features`;
CREATE TABLE `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `order_position` tinyint(2) DEFAULT NULL,
  `show` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of features
-- ----------------------------
INSERT INTO `features` VALUES ('1', 'Fast and Easy', '<p>Send a message quickly whenever your customers want with smart &quot;Contact Us&quot; button</p>\r\n', '1420009638_feat-detail1.png', '1', '1');
INSERT INTO `features` VALUES ('2', 'Responsive', '<p>Work on all devices (desktop, laptop, tablet, smartphones...) with user-friendly and responsive design</p>\r\n', '1419845274_feat-detail2.png', '2', '1');
INSERT INTO `features` VALUES ('3', 'Never be Spammed', '<p>Our latest spam protection technology makes sure you will never be spammed and your customer will never have to enter a Captcha</p>\r\n', '1419845310_feat-detail3.png', '3', '1');
INSERT INTO `features` VALUES ('4', 'Convenience', '<p>Save your customer&#39;s time and make them comfortable with Lightbox, HTML5 and Ajax technology</p>\r\n', '1419845362_feat-detail4.png', '4', '1');
INSERT INTO `features` VALUES ('5', 'Fully Customizable', '<p>Fit to all websites. Customize your form fields, form style and color never easier</p>\r\n', '1419845390_feat-detail5.png', '5', '1');
INSERT INTO `features` VALUES ('6', 'Multiple Departments', '<p>Allows your customers to select department and send the message to different recipient email address</p>\r\n', '1419845418_feat-detail6.png', '6', '1');
INSERT INTO `features` VALUES ('7', 'Rich Text Format', '<p>Allows your customers to highlighttheir message with bold, italic, underline, list and add hyperlink.</p>\r\n', '1419845461_feat-detail7.png', '7', '1');
INSERT INTO `features` VALUES ('8', 'File Attachment', '<p>Fit to all websites. Customize your form fields, form style and color never easier</p>\r\n', '1419845492_feat-detail8.png', '8', '1');
INSERT INTO `features` VALUES ('9', 'Customer Information', '<p>Give you the useful info like where customers are referred from, what they search before come to your site, the customers&#39; location, their device, what page they are on and submitted the message.</p>\r\n', '1419845541_feat-detail9.png', '9', '1');
INSERT INTO `features` VALUES ('10', 'Secured SSL', '<p>Support your SSL secured page (https://) and submit all data via our SSL encrypted connection.</p>\r\n', '1419845567_feat-detail10.png', '10', '1');

-- ----------------------------
-- Table structure for form_language
-- ----------------------------
DROP TABLE IF EXISTS `form_language`;
CREATE TABLE `form_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(11) DEFAULT NULL,
  `message_key` varchar(255) DEFAULT NULL,
  `message_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of form_language
-- ----------------------------
INSERT INTO `form_language` VALUES ('1', 'EN', 'invalid_name', 'Please enter valid name.');
INSERT INTO `form_language` VALUES ('2', 'EN', 'invalid_email', 'Please enter valid email.');
INSERT INTO `form_language` VALUES ('3', 'EN', 'invalid_phone', 'Please enter valid phone.');
INSERT INTO `form_language` VALUES ('4', 'EN', 'invalid_company', 'Please enter valid company.');
INSERT INTO `form_language` VALUES ('5', 'EN', 'invalid_website', 'Please enter valid website.');
INSERT INTO `form_language` VALUES ('6', 'EN', 'invalid_subject', 'Please enter valid subject.');
INSERT INTO `form_language` VALUES ('7', 'EN', 'invalid_message', 'Please enter valid message.');
INSERT INTO `form_language` VALUES ('8', 'EN', 'btn_contact', 'Contact Us');
INSERT INTO `form_language` VALUES ('9', 'VN', 'invalid_name', 'Hãy điền tên hợp lệ');
INSERT INTO `form_language` VALUES ('10', 'VN', 'invalid_email', 'Hãy điền hòm thư hợp lệ.');
INSERT INTO `form_language` VALUES ('11', 'VN', 'invalid_phone', 'Hãy điền số điện thoại hợp lệ.');
INSERT INTO `form_language` VALUES ('12', 'VN', 'invalid_company', 'Hãy điền công ty hợp lệ.');
INSERT INTO `form_language` VALUES ('13', 'VN', 'invalid_website', 'Hãy điền trang web hơp lệ.');
INSERT INTO `form_language` VALUES ('14', 'VN', 'invalid_subject', 'Hãy điền tiêu đề hợp lệ.');
INSERT INTO `form_language` VALUES ('15', 'VN', 'invalid_message', 'Hãy điền tin nhắn hợp lệ.');
INSERT INTO `form_language` VALUES ('16', 'VN', 'btn_contact', 'Liên hệ');
INSERT INTO `form_language` VALUES ('17', 'EN', 'placeholder_name', 'Your name');
INSERT INTO `form_language` VALUES ('18', 'EN', 'placeholder_email', 'Your email');
INSERT INTO `form_language` VALUES ('19', 'EN', 'placeholder_phone', 'Your phone');
INSERT INTO `form_language` VALUES ('20', 'EN', 'placeholder_company', 'Your company');
INSERT INTO `form_language` VALUES ('21', 'EN', 'placeholder_website', 'Your website');
INSERT INTO `form_language` VALUES ('22', 'EN', 'placeholder_subject', 'Subject');
INSERT INTO `form_language` VALUES ('23', 'EN', 'placeholder_message', 'Message...');
INSERT INTO `form_language` VALUES ('24', 'VN', 'placeholder_name', 'Họ và tên');
INSERT INTO `form_language` VALUES ('25', 'VN', 'placeholder_email', 'Địa chỉ email');
INSERT INTO `form_language` VALUES ('26', 'VN', 'placeholder_phone', 'Điện thoại liên lạc');
INSERT INTO `form_language` VALUES ('27', 'VN', 'placeholder_company', 'Công ty, tổ chức');
INSERT INTO `form_language` VALUES ('28', 'VN', 'placeholder_website', 'Website cá nhân');
INSERT INTO `form_language` VALUES ('29', 'VN', 'placeholder_subject', 'Tiêu đề');
INSERT INTO `form_language` VALUES ('30', 'VN', 'placeholder_message', 'Tin nhắn..');
INSERT INTO `form_language` VALUES ('31', 'EN', 'contact_label', 'Contact Us');
INSERT INTO `form_language` VALUES ('32', 'VN', 'contact_label', 'Liên hệ');
INSERT INTO `form_language` VALUES ('33', 'EN', 'btn_send', 'Send');
INSERT INTO `form_language` VALUES ('34', 'VN', 'btn_send', 'Gửi tin');
INSERT INTO `form_language` VALUES ('35', 'FR', 'contact_label', 'Contactez-nous');
INSERT INTO `form_language` VALUES ('36', 'FR', 'invalid_name', 'S\'il vous plaît entrer votre nom');
INSERT INTO `form_language` VALUES ('37', 'FR', 'invalid_email', 'S\'il vous plaît entrer une adresse email valide');
INSERT INTO `form_language` VALUES ('38', 'FR', 'invalid_phone', 'S\'il vous plaît entrez votre téléphone');
INSERT INTO `form_language` VALUES ('39', 'FR', 'invalid_company', 'S\'il vous plaît, entrez votre société');
INSERT INTO `form_language` VALUES ('40', 'FR', 'invalid_website', 'S\'il vous plaît, entrez votre site');
INSERT INTO `form_language` VALUES ('41', 'FR', 'invalid_subject', 'S\'il vous plaît entrer sujet');
INSERT INTO `form_language` VALUES ('42', 'FR', 'invalid_message', 'N\'avez-vous pas quelque chose à dire?');
INSERT INTO `form_language` VALUES ('43', 'FR', 'btn_contact', 'Contactez-nous');
INSERT INTO `form_language` VALUES ('44', 'FR', 'btn_send', 'Envoyer');
INSERT INTO `form_language` VALUES ('45', 'FR', 'placeholder_name', 'Votre nom');
INSERT INTO `form_language` VALUES ('46', 'FR', 'placeholder_email', 'Votre adresse e-mail');
INSERT INTO `form_language` VALUES ('47', 'FR', 'placeholder_phone', 'Votre téléphone');
INSERT INTO `form_language` VALUES ('48', 'FR', 'placeholder_company', 'Votre entreprise');
INSERT INTO `form_language` VALUES ('49', 'FR', 'placeholder_website', 'Votre site web');
INSERT INTO `form_language` VALUES ('50', 'FR', 'placeholder_subject', 'Sujet');
INSERT INTO `form_language` VALUES ('51', 'FR', 'placeholder_message', 'Message..');
INSERT INTO `form_language` VALUES ('52', 'ES', 'contact_label', 'Contáctenos');
INSERT INTO `form_language` VALUES ('53', 'ES', 'invalid_name', 'Por favor introduce tu nombre');
INSERT INTO `form_language` VALUES ('54', 'ES', 'invalid_email', 'Por favor introduce una dirección de email válida');
INSERT INTO `form_language` VALUES ('55', 'ES', 'invalid_phone', 'Por favor introduce tu teléfono');
INSERT INTO `form_language` VALUES ('56', 'ES', 'invalid_company', 'Por favor introduce tu compañia');
INSERT INTO `form_language` VALUES ('57', 'ES', 'invalid_website', 'Por favor introduce tu página web');
INSERT INTO `form_language` VALUES ('58', 'ES', 'invalid_subject', 'Por favor indica un asunto');
INSERT INTO `form_language` VALUES ('59', 'ES', 'btn_contact', 'Contáctenos');
INSERT INTO `form_language` VALUES ('60', 'ES', 'btn_send', 'Enviar');
INSERT INTO `form_language` VALUES ('61', 'ES', 'placeholder_name', 'Tu nombre');
INSERT INTO `form_language` VALUES ('62', 'ES', 'placeholder_email', 'Tu dirección de email');
INSERT INTO `form_language` VALUES ('63', 'ES', 'placeholder_phone', 'Tu teléfono');
INSERT INTO `form_language` VALUES ('64', 'ES', 'placeholder_company', 'Tu compañia');
INSERT INTO `form_language` VALUES ('65', 'ES', 'placeholder_website', 'Tu página web');
INSERT INTO `form_language` VALUES ('66', 'ES', 'placeholder_subject', 'Asunto');
INSERT INTO `form_language` VALUES ('67', 'ES', 'placeholder_message', 'Mensaje..');
INSERT INTO `form_language` VALUES ('68', 'ES', 'invalid_message', '¿No tienes nada que decirnos?');
INSERT INTO `form_language` VALUES ('69', 'EN', 'class_size', 'class_size_medium');
INSERT INTO `form_language` VALUES ('70', 'VN', 'class_size', 'class_size_small');
INSERT INTO `form_language` VALUES ('71', 'FR', 'class_size', 'class_size_medium');
INSERT INTO `form_language` VALUES ('72', 'ES', 'class_size', 'class_size_medium');
INSERT INTO `form_language` VALUES ('73', 'DE', 'class_size', 'class_size_large');
INSERT INTO `form_language` VALUES ('77', 'DE', 'placeholder_name', 'Ihr Name');
INSERT INTO `form_language` VALUES ('74', 'DE', 'contact_label', 'Kontaktieren Sie uns');
INSERT INTO `form_language` VALUES ('75', 'DE', 'btn_contact', 'Kontaktieren Sie uns');
INSERT INTO `form_language` VALUES ('76', 'DE', 'btn_send', 'Senden');
INSERT INTO `form_language` VALUES ('78', 'DE', 'placeholder_email', 'Ihre E-mail-adresse');
INSERT INTO `form_language` VALUES ('79', 'DE', 'placeholder_phone', 'Ihr Telefon');
INSERT INTO `form_language` VALUES ('80', 'DE', 'placeholder_company', 'Ihr Unternehmen');
INSERT INTO `form_language` VALUES ('81', 'DE', 'placeholder_website', 'Ihre Website');
INSERT INTO `form_language` VALUES ('82', 'DE', 'placeholder_subject', 'Thema');
INSERT INTO `form_language` VALUES ('83', 'DE', 'placeholder_message', 'Nachricht..');
INSERT INTO `form_language` VALUES ('84', 'DE', 'invalid_name', 'Bitte geben Sie Ihren Namen');
INSERT INTO `form_language` VALUES ('85', 'DE', 'invalid_email', 'Bitte geben Sie eine gültige E-Mail-Adresse');
INSERT INTO `form_language` VALUES ('86', 'DE', 'invalid_phone', 'Bitte geben Sie Ihre Telefon');
INSERT INTO `form_language` VALUES ('87', 'DE', 'invalid_company', 'Bitte geben Sie Ihre Firma');
INSERT INTO `form_language` VALUES ('88', 'DE', 'invalid_website', 'Bitte geben Sie Ihre Website');
INSERT INTO `form_language` VALUES ('89', 'DE', 'invalid_subject', 'Bitte geben Sie unterliegen');
INSERT INTO `form_language` VALUES ('90', 'DE', 'invalid_message', 'Haben Sie nicht etwas zu sagen?');
INSERT INTO `form_language` VALUES ('91', 'IT', 'class_size', 'class_size_medium');
INSERT INTO `form_language` VALUES ('92', 'IT', 'contact_label', 'Contattaci');
INSERT INTO `form_language` VALUES ('93', 'IT', 'btn_contact', 'Contattaci');
INSERT INTO `form_language` VALUES ('94', 'IT', 'btn_send', 'Invia');
INSERT INTO `form_language` VALUES ('95', 'IT', 'placeholder_name', 'Nome');
INSERT INTO `form_language` VALUES ('96', 'IT', 'placeholder_email', 'Indirizzo email');
INSERT INTO `form_language` VALUES ('97', 'IT', 'placeholder_phone', 'Telefono');
INSERT INTO `form_language` VALUES ('98', 'IT', 'placeholder_company', 'Azienda');
INSERT INTO `form_language` VALUES ('99', 'IT', 'placeholder_website', 'Sito');
INSERT INTO `form_language` VALUES ('100', 'IT', 'placeholder_subject', 'Oggetto');
INSERT INTO `form_language` VALUES ('101', 'IT', 'placeholder_message', 'Messaggio..');
INSERT INTO `form_language` VALUES ('102', 'IT', 'invalid_name', 'Inserisci il tuo nome');
INSERT INTO `form_language` VALUES ('103', 'IT', 'invalid_email', 'Inserisci un indirizzo email valido');
INSERT INTO `form_language` VALUES ('104', 'IT', 'invalid_phone', 'Inserisci il numero di telefono');
INSERT INTO `form_language` VALUES ('105', 'IT', 'invalid_company', 'Inserisci il nome dell\'azienda');
INSERT INTO `form_language` VALUES ('106', 'IT', 'invalid_website', 'Inserisci il sito internet');
INSERT INTO `form_language` VALUES ('107', 'IT', 'invalid_subject', 'Inserisci l\'oggetto');
INSERT INTO `form_language` VALUES ('108', 'IT', 'invalid_message', 'Hai qualcosa da dire?');
INSERT INTO `form_language` VALUES ('109', 'PT', 'class_size', 'class_size_medium');
INSERT INTO `form_language` VALUES ('110', 'PT', 'contact_label', 'Contacte-nos');
INSERT INTO `form_language` VALUES ('111', 'PT', 'btn_contact', 'Contacte-nos');
INSERT INTO `form_language` VALUES ('112', 'PT', 'btn_send', 'Enviar');
INSERT INTO `form_language` VALUES ('113', 'PT', 'invalid_name', 'Digite o seu nome e apelido');
INSERT INTO `form_language` VALUES ('114', 'PT', 'invalid_email', 'Digite um endereço de email válido');
INSERT INTO `form_language` VALUES ('115', 'PT', 'invalid_phone', 'Digite o seu número de telefone');
INSERT INTO `form_language` VALUES ('116', 'PT', 'invalid_company', 'Digite o nome da sua empresa');
INSERT INTO `form_language` VALUES ('117', 'PT', 'invalid_website', 'Digite o endereço do seu site');
INSERT INTO `form_language` VALUES ('118', 'PT', 'invalid_subject', 'Digite o assunto');
INSERT INTO `form_language` VALUES ('119', 'PT', 'invalid_message', 'Digite a sua mensagem');
INSERT INTO `form_language` VALUES ('120', 'PT', 'placeholder_name', 'Nome e Apelido');
INSERT INTO `form_language` VALUES ('121', 'PT', 'placeholder_email', 'Email');
INSERT INTO `form_language` VALUES ('122', 'PT', 'placeholder_phone', 'Telefone');
INSERT INTO `form_language` VALUES ('123', 'PT', 'placeholder_company', 'Empresa');
INSERT INTO `form_language` VALUES ('124', 'PT', 'placeholder_website', 'Site');
INSERT INTO `form_language` VALUES ('125', 'PT', 'placeholder_subject', 'Assunto');
INSERT INTO `form_language` VALUES ('126', 'PT', 'placeholder_message', 'Mensagem..');
INSERT INTO `form_language` VALUES ('127', 'RU', 'class_size', 'class_size_large');
INSERT INTO `form_language` VALUES ('128', 'RU', 'contact_label', 'Свяжитесь с нами');
INSERT INTO `form_language` VALUES ('129', 'RU', 'btn_contact', 'Свяжитесь с нами');
INSERT INTO `form_language` VALUES ('130', 'RU', 'btn_send', 'Отправить');
INSERT INTO `form_language` VALUES ('131', 'RU', 'invalid_name', 'Введите Ваше имя');
INSERT INTO `form_language` VALUES ('132', 'RU', 'invalid_email', 'Введите правильный адрес e-mail');
INSERT INTO `form_language` VALUES ('133', 'RU', 'invalid_phone', 'Введите Ваш телефон');
INSERT INTO `form_language` VALUES ('134', 'RU', 'invalid_company', 'Введите Вашу компанию');
INSERT INTO `form_language` VALUES ('135', 'RU', 'invalid_website', 'Введите Ваш сайт');
INSERT INTO `form_language` VALUES ('136', 'RU', 'invalid_subject', 'Введите тему');
INSERT INTO `form_language` VALUES ('137', 'RU', 'invalid_message', 'Вам нечего нам сказать?');
INSERT INTO `form_language` VALUES ('138', 'RU', 'placeholder_name', 'Ваше Имя');
INSERT INTO `form_language` VALUES ('139', 'RU', 'placeholder_email', 'Ваш e-mail');
INSERT INTO `form_language` VALUES ('140', 'RU', 'placeholder_phone', 'Ваш телефон');
INSERT INTO `form_language` VALUES ('141', 'RU', 'placeholder_company', 'Ваша компания');
INSERT INTO `form_language` VALUES ('142', 'RU', 'placeholder_website', 'Ваш сайт');
INSERT INTO `form_language` VALUES ('143', 'RU', 'placeholder_subject', 'Тема');
INSERT INTO `form_language` VALUES ('144', 'RU', 'placeholder_message', 'Сообщение..');

-- ----------------------------
-- Table structure for merchant_info
-- ----------------------------
DROP TABLE IF EXISTS `merchant_info`;
CREATE TABLE `merchant_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `login_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of merchant_info
-- ----------------------------
INSERT INTO `merchant_info` VALUES ('1', 'sandbox', '2A2KgS68ku', '2WE7K3p9nq396bP3', '2015-05-25 05:50:40', '2015-05-25 16:11:57');
INSERT INTO `merchant_info` VALUES ('2', 'live', '8Wke82Ru2jMG', '6pmw24Pm6866G7Jw', '2015-05-22 12:54:07', '2015-05-25 16:11:58');

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1416072261');

-- ----------------------------
-- Table structure for payment_config
-- ----------------------------
DROP TABLE IF EXISTS `payment_config`;
CREATE TABLE `payment_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_env` varchar(255) DEFAULT NULL COMMENT 'sandbox or live',
  `authorize_md5_setting` varchar(255) DEFAULT NULL,
  `free_period` int(11) DEFAULT NULL COMMENT 'When user create a free form, this filed decide how long does this form is free ?',
  `monthly_premium_fee` float(11,2) DEFAULT NULL,
  `yearly_premium_fee` float(11,2) DEFAULT NULL,
  `notice_before_day` int(11) DEFAULT NULL COMMENT 'How many day before end premium, user receive email notification ?',
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment_config
-- ----------------------------
INSERT INTO `payment_config` VALUES ('1', 'sandbox', '', '5', '10.00', '100.00', '3', '2015-05-25 15:51:27', '2015-05-25 16:44:30');

-- ----------------------------
-- Table structure for profiles
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `user_id` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profiles
-- ----------------------------
INSERT INTO `profiles` VALUES ('1', 'Admin', 'Administrator');
INSERT INTO `profiles` VALUES ('2', 'Demo', 'Demo');

-- ----------------------------
-- Table structure for profiles_fields
-- ----------------------------
DROP TABLE IF EXISTS `profiles_fields`;
CREATE TABLE `profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profiles_fields
-- ----------------------------
INSERT INTO `profiles_fields` VALUES ('1', 'lastname', 'Last Name', 'VARCHAR', '50', '3', '1', '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', '1', '3');
INSERT INTO `profiles_fields` VALUES ('2', 'firstname', 'First Name', 'VARCHAR', '50', '3', '1', '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', '0', '3');

-- ----------------------------
-- Table structure for slide
-- ----------------------------
DROP TABLE IF EXISTS `slide`;
CREATE TABLE `slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `image_background` varchar(255) DEFAULT NULL,
  `order_position` tinyint(2) DEFAULT NULL,
  `show` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slide
-- ----------------------------
INSERT INTO `slide` VALUES ('5', '1420540083_image-slide1.png', '1420540083_slide3.jpg', '0', '1');
INSERT INTO `slide` VALUES ('6', '1420540099_image-slide2.png', '1420540099_slide4.jpg', '1', '1');
INSERT INTO `slide` VALUES ('7', '1420540115_image-slide1.png', '1420540115_slide1.jpg', '2', '1');
INSERT INTO `slide` VALUES ('8', '1420540129_image-slide2.png', '1420540129_slide5.jpg', '3', '1');

-- ----------------------------
-- Table structure for static_page
-- ----------------------------
DROP TABLE IF EXISTS `static_page`;
CREATE TABLE `static_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `content` text,
  `updated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of static_page
-- ----------------------------
INSERT INTO `static_page` VALUES ('1', 'terms_of_service', 'Terms of Service', 'We support small companies and startups. Contact us for discount', '<p>By using the BetterContactForm (Service), you are agreeing to be bound by the following terms and conditions (&quot;Terms of Service&quot;). BetterContactForm reserves the right to update and change these Terms of Service without notice. Violation of any of the terms below may result in the termination of your account.</p>\r\n\r\n<h3 style=\"font-size: 16px;font-weight: 700;color: #343434;\">Account Terms</h3>\r\n\r\n<p>1. You are responsible for maintaining the security of your account and password. BetterContactForm cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.</p>\r\n\r\n<p>2. You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.</p>\r\n\r\n<p>3. Your login may only be used by one person &ndash; a single login shared by multiple people is not permitted.</p>\r\n\r\n<p>4. You must be a human. Accounts registered by &ldquo;bots&rdquo; or other automated methods are not permitted.</p>\r\n\r\n<h3 style=\"font-size: 16px;font-weight: 700;color: #343434;\">Payment, Refunds, Upgrading and Downgrading Terms</h3>\r\n\r\n<p>1. The Service is offered with a free plan. You are only required to pay for using the Pro plan with additional features.</p>\r\n\r\n<p>2. Downgrading your Service may cause the loss of features or capacity of your account. BetterContactForm does not accept any liability for such loss.</p>\r\n\r\n<p>3. To guarantee your protection we offer 30-days money back guarantee for our service. If you are dissatisfied with your service before 30 days after the purchase for any reason, you can receive a full refund. To request a refund please submit a request containing your Transaction ID. Please note, all refund requests after 30 days are not accepted. Cancellation and Termination You are solely responsible for properly canceling your account. Please submit a request to cancel your account. All of your content will be immediately be inaccessible from the Service upon cancellation. Within 30 days, all this content will be permanently deleted from all backups and logs. This information can not be recovered once it has been permanently deleted. If you cancel the Service before the end of your current paid up month, your cancellation will take effect immediately, and you will not be charged again. But there will not be any prorating of unused time in the last billing cycle. BetterContactForm has the right to suspend or terminate your account and refuse any and all current or future use of the Service for any reason at any time. We reserve the right to refuse service to anyone for any reason at any time. Modifications to the Service and Prices BetterContactForm reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, any part of the Service with or without notice. Prices of all Services are subject to change upon 30 days notice from us. Such notice may be provided at any time by posting the changes to the BetterContactForm site or the Service itself. BetterContactForm shall not be liable to you or to any third party for any modification, price change, suspension or discontinuance of the Service. Copyright The look and feel (design) of the Service is copyright&copy; BetterContactForm. All rights reserved. You may not duplicate, copy, or reuse any portion of the HTML, CSS, JavaScript, or visual design elements without express written permission from BetterContactForm. General Conditions Your use of the Service is at your sole risk. The service is provided on an &ldquo;as is&rdquo; and &ldquo;as available&rdquo; basis. Technical support is only provided via email. You understand that BetterContactForm uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run the Service. You must not modify, adapt or hack the Service. You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without the express written permission by BetterContactForm. We reserve the right to temporarily disable your account if your usage significantly exceeds the average usage of other Service customers. Of course, we&#39;ll reach out to the account owner before taking any action except in rare cases where the level of use may negatively impact the performance of the Service for other customers. BetterContactForm does not warrant that (i) the service will meet your specific requirements, (ii) the service will be uninterrupted, timely, secure, or error-free, (iii) the results that may be obtained from the use of the service will be accurate or reliable, (iv) the quality of any products, services, information, or other material purchased or obtained by you through the service will meet your expectations, and (v) any errors in the Service will be corrected. You expressly understand and agree that BetterContactForm shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if BetterContactForm has been advised of the possibility of such damages), resulting from: (i) the use or the inability to use the service; (ii) the cost of procurement of substitute goods and services resulting from any goods, data, information or services purchased or obtained or messages received or transactions entered into through or from the service; (iii) unauthorized access to or alteration of your transmissions or data; (iv) statements or conduct of any third party on the service; (v) or any other matter relating to the service. Any new features that augment or enhance the current Service, including the release of new tools and resources, shall be subject to the Terms of Service. Continued use of the Service after any such changes shall constitute your consent to such changes. If you have any questions or concerns, please leave us a message and we&rsquo;ll be happy to answer them!</p>\r\n', '2015-01-13 11:17:02');
INSERT INTO `static_page` VALUES ('2', 'privacy_policy', 'Privacy Policy', 'We support small companies and startups. Contact us for discount', '<p>\r\n                    ContactForm is committed to protecting your privacy. We will not trade, sell, or disclose your personal info to any \r\n                    third parties. We’ll never open/read any submitted information from your customers. If you do not agree with any of the terms \r\n                    below, you should not use our BetterContactForm service. \r\n                </p>\r\n                <h3 style=\"font-size: 16px;font-weight: 700;color: #343434;\">Identity & access</h3>\r\n                <p>When you sign up for BetterContactForm, we ask for your name and email address. That\'s just so you can personalize your new account, and we can send you invoices, updates, or other essential information. We’ll never sell or disclose your personal info to any third parties.   The only times we’ll ever share your info:</p>\r\n                <ul style=\"list-style: square;margin-left: 30px;font-size: 15px;line-height: 25px;\">\r\n                    <li>To provide products or services you\'ve requested, with your permission.</li>\r\n                    <li>To investigate, prevent, or take action regarding illegal activities, suspected fraud, violations of our Terms of Service, or as otherwise required by law.</li>\r\n                    <li>If BetterContactForm is acquired by or merged with another company — we don’t plan on that, but if it happens — we’ll notify you well before any info about you is transferred and becomes subject to a different privacy policy.</li>\r\n                </ul>\r\n                <h3 style=\"font-size: 16px;font-weight: 700;color: #343434;\">Law enforcement</h3>\r\n                <p>\r\n                    BetterContactForm won’t hand your data over to law enforcement unless a court order say we have to. We flat-out reject \r\n                    requests from local and federal law enforcement when they seek data without a court order. And unless we\'re legally prevented \r\n                    from it, we’ll always inform you when such requests are made.\r\n                </p>\r\n                <h3 style=\"font-size: 16px;font-weight: 700;color: #343434;\">Submitted Information</h3>\r\n                <p>\r\n                    You expressly understand and agree that your customers will submit messages via BetterContactForm server. Our web app \r\n                    also auto-detect some additional information of your customers such as IP, location, referral site, search keyword, \r\n                    current page URL... All these information will be delivered to your recipient email right after your customer click Send. \r\n                    These information will be encrypted and logged in our server in 30 days for spam prevention and report purpose. We’ll never \r\n                    open/read these information. With Pro version, all data is encrypted via SSL/TLS when transmitted from our servers to your browser.\r\n                </p>\r\n                <h3 style=\"font-size: 16px;font-weight: 700;color: #343434;\">Changes & questions</h3>\r\n                <p>\r\n                    BetterContactForm may periodically update this policy — we’ll notify you about significant changes by emailing the account \r\n                    owner or by placing a prominent notice on our site. You can access, change or delete your personal information at any time. \r\n                    Questions about this privacy policy? Please leave us a message and we’ll be happy to answer them!\r\n                </p>', '2015-01-13 11:09:21');
INSERT INTO `static_page` VALUES ('3', 'faq', 'Frequently Asked Questions', '', null, '2015-01-13 11:32:47');
INSERT INTO `static_page` VALUES ('4', 'features', 'Features', 'The professional contact form let your customers comfortable to drop a message quickly whenever they want', null, '2015-01-13 11:39:59');
INSERT INTO `static_page` VALUES ('5', 'blog', 'Our Blog', '', null, '2014-12-29 17:47:06');
INSERT INTO `static_page` VALUES ('6', 'discussion', 'Discussion', 'We would love to hear your feedback to make the contact form better. Please read our FAQ page before you post a comment or send us a private message', null, '2015-01-13 13:21:53');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `social_connect` varchar(50) DEFAULT '',
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `superuser` (`superuser`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'contactformmedia@gmail.com', '9a24eff8c15a6a141ece27eb6947da0f', '2014-11-16 00:27:38', '2014-12-16 10:38:48', '1', '1', null, 'Admin', 'Administrator', null);
INSERT INTO `users` VALUES ('2', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'luanadk52@gmail.com', '204f93f62f1b60af3295d230f59dd9f6', '2014-11-17 00:00:00', '2015-01-22 16:28:40', '0', '1', null, 'Demo', 'Demo', null);

-- ----------------------------
-- Table structure for user_form
-- ----------------------------
DROP TABLE IF EXISTS `user_form`;
CREATE TABLE `user_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `recipient_email` varchar(255) DEFAULT NULL,
  `form_style` varchar(50) DEFAULT NULL,
  `form_language` varchar(255) DEFAULT NULL,
  `meta_form` text,
  `submit_count` int(11) DEFAULT '0' COMMENT 'Number of submit form',
  `form_status` varchar(255) DEFAULT NULL COMMENT 'When create: Not active, Have submit: Active, Or Delete (Delete form mean remove css, js of form, not remove record in database), Disable',
  `avatar` varchar(255) DEFAULT NULL COMMENT 'Avatar, image represent for form',
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_form
-- ----------------------------
INSERT INTO `user_form` VALUES ('13', '1', 'contactformmedia@gmail.com', 'style_basic', 'EN', '{\"form_language\":\"EN\",\"has_name\":\"1\",\"has_email\":\"1\",\"has_phone\":null,\"has_company\":null,\"has_website\":null,\"has_subject\":null,\"has_message\":\"1\",\"form_color\":\"#009fe1\",\"button_position\":\"left\",\"form_style\":\"style_basic\",\"extra_info\":\"\",\"custom_logo_link\":\"\",\"recipient_email\":\"contactformmedia@gmail.com\",\"form_id\":\"13\"}', '0', 'inactive', '/upload/images/form_avatar/contact-form_basic.jpg', '2015-01-09 10:24:33', '2015-01-31 23:00:48');

-- ----------------------------
-- Table structure for user_oauth
-- ----------------------------
DROP TABLE IF EXISTS `user_oauth`;
CREATE TABLE `user_oauth` (
  `provider` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `identifier` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `profile_cache` text CHARACTER SET utf8,
  `session_data` text CHARACTER SET utf8,
  PRIMARY KEY (`identifier`,`provider`),
  KEY `fk_user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_oauth
-- ----------------------------
