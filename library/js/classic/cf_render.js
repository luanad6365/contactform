/**
 * 
 * Render HTML for form
 */
CF_HTML = (function() {
    return {
        initElement: function(role, arrAttrs, appendTo, inOrder) {
            //Create element
            var ele = document.createElement(role);
            //Add attributes
            for (var attr in arrAttrs) {
                //Add array class
                if (attr == 'class') {
                    for (var clas in arrAttrs[attr]) {
                        CF_STYLE.addClass(ele, arrAttrs[attr][clas]);
                    }
                } else {
                    //Add array style
                    if (attr == 'style') {
                        for (var styleName in arrAttrs[attr]) {
                            ele.style[styleName] = arrAttrs[attr][styleName];
                        }
                    } else {
                        //Add inner HTML
                        if (attr == 'innerHTML') {
                            ele.innerHTML = arrAttrs[attr];
                        } else { // Add normal attributes
                            CF_STYLE.addAttribute(ele, attr, arrAttrs[attr]);
                        }
                    }
                }
            }
            var normalOrder = true;
            //Order by 6 field
            if (inOrder) {
                var node = '';
                //Find before node of current element
                var order = Cf_Define.orderField;
                for (var stt in order) {
                    if (order[stt] == ele.id) {
                        break;
                    } else {
                        if (document.getElementById(order[stt])) {
                            node = document.getElementById(order[stt]);
                        }
                    }
                }
                if (node) { // insert current element after node found
                    node.parentNode.insertBefore(ele, node.nextSibling);
                } else { // insert current element to first child
                    var parrentNode = document.getElementById(appendTo);
                    parrentNode.insertBefore(ele, parrentNode.firstChild);
                }
                normalOrder = false;
            }
            //Normal, only append
            if (normalOrder) {
                //Append to DOM
                if (appendTo !== null) {
                    document.getElementById(appendTo).appendChild(ele);
                } else {
                    if (document.body.firstChild) {
                        document.body.insertBefore(ele, document.body.firstChild);
                    } else {
                        document.body.appendChild(ele);
                    }
                }
            }
            return ele;
        },
        removeElement: function(id) {
            var myNode = document.getElementById(id);
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
            //Apply for old IE
            myNode.parentNode.removeChild(myNode);
            //No support ie 9 
            //myNode.remove();
        },
        //formVersion : form_demo or form_live
        buildForm: function(forms, hashId, appendTo, formVersion) {
            var idForm = '';
            if(forms.form_style){
                idForm += Cf_Define.formStyle[forms.form_style];
            }
            if(hashId){
                idForm += '-'+hashId;
            }            
            //Wrap all
            CF_HTML.initElement('DIV', {'id': idForm}, appendTo);
            //Add more effect to some special form
            if(forms.form_style == Cf_Define.formKey.freestyle || forms.form_style == Cf_Define.formKey.child){
				var arrClasses = ['ctf-wrapper', 'corlor'];
			} else {
				var arrClasses = ['ctf-wrapper'];
			}
            CF_HTML.initElement('DIV', {'id': 'ctf-wrapper', 'class': arrClasses}, idForm);
            
            CF_HTML.initElement('DIV', {'id': 'ctf-content', 'class': ['ctf-content']}, 'ctf-wrapper');
            //2 big block
			//Only basic form has class corlor
			if(forms.form_style == Cf_Define.formKey.basic){
				var arrClasses = ['ctf-head-title', 'corlor'];
			} else {
				var arrClasses = ['ctf-head-title'];
			}
			
			//If normal form, add more div in wrapper
			if(forms.form_style == Cf_Define.formKey.normal){
				CF_HTML.initElement('DIV', {'id': 'bg-normal-form', 'class': ['bg-normal-form']}, 'ctf-wrapper');
			}
            CF_HTML.initElement('DIV', {'id': 'ctf-head-title', 'class': arrClasses}, 'ctf-content');
            CF_HTML.initElement('DIV', {'id': 'ctf-inner-content', 'class': ['ctf-inner-content']}, 'ctf-content');
            //First block
            CF_HTML.initElement('DIV', {'id': 'ctf-logo', 'class': ['ctf-logo']}, 'ctf-head-title');
            
            //If demo version, set blank logo
            if(formVersion && formVersion == Cf_Define.formVersion.demo){
                if (!forms.custom_logo_link) {
                    //forms.custom_logo_link = PARAMS.baseUrl+'/library/img/contact-logo.png';
                }    
            }  
			
			var arrAttrs = {
				'name': 'custom_logo_image',
				'id': 'custom_logo_image',
				'src': forms.custom_logo_link,
				'class': '',
				'style': {
					'width': '37px',
					'height': '37px'
				}
			};
			//If dont have logo, set hide image
			if (!forms.custom_logo_link) {
				arrAttrs.class = ['hide'];
            }
			CF_HTML.initElement('IMG', arrAttrs, 'ctf-logo');

            CF_HTML.initElement('H2', {'innerHTML': CF_FORM_MESSAGE[forms.form_language].contact_label}, 'ctf-head-title');
            //Second block
            CF_HTML.initElement('DIV', {'id': 'ctf-contact-form', 'class': ['ctf-contact-form', 'clearfix']}, 'ctf-inner-content');
            
            if (forms.form_id) {
                var ele = CF_FORM.createHidden(forms.form_id);
            }
			//Child and Freestyle doesnt have icon, but no need remove style icon because we dont style icon for 2 these forms
            if (forms.has_name === "1") {
                var ele = CF_FORM.createName(forms.form_language);
            }
            if (forms.has_email === "1") {
                var ele = CF_FORM.createEmail(forms.form_language);
            }
            if (forms.has_phone === "1") {
                var ele = CF_FORM.createPhone(forms.form_language);
            }
            if (forms.has_company === "1") {
                var ele = CF_FORM.createCompany(forms.form_language);
            }
            if (forms.has_website === "1") {
                var ele = CF_FORM.createWebsite(forms.form_language);
            }
            if (forms.has_subject === "1") {
                var ele = CF_FORM.createSubject(forms.form_language);
            }
            if (forms.has_message === "1") {
				var ele = CF_FORM.createMessage(forms.form_language);
            }
            //Hide all tooltip message
            CF_FORM.hideAllTooltip(idForm);
            //Btn Send
            var arrAttrs = {
                'type': 'button',
                'name': 'cf_demo_button',
                'id': 'cf_demo_button',
                'class': ['ctf-button', 'corlor']
            };
            
            //Create select box for multi department if needed
            if(forms.hasOwnProperty('multi_recipient')){
                var recipients = forms.multi_recipient;
                //If have multi recipient, then create select box
                if(Object.keys(recipients).length > 0){
                    
                    //Create div wrap block select box
                    var eleWrap = CF_HTML.initElement('DIV', {'id':'cf_wrap_multi_recipient', 'class':['ctf-form-group', 'ctf-f-size', 'ctf-orientation', 'clearfix']}, 'ctf-contact-form');
                    //Create div wrap level 2
                    var eleWrapLv2 = CF_HTML.initElement('DIV', {'id':'cf_wrap_multi_recipient_level_2', 'class':['ctf-dropdown', 'ctf-field-f', 'ctf-f-size', 'ctf-orientation', 'clearfix']}, 'cf_wrap_multi_recipient');
                    //Create span "To:" (Need to multi language)
                    var spanTo = CF_HTML.initElement('SPAN', {'innerHTML': CF_FORM_MESSAGE[forms.form_language].to_recipient, 'class':['corlor-text'], 'style':{'font-weight': 'bold'} }, 'cf_wrap_multi_recipient_level_2');
                    //Create span recipient name
                    var spanRecipient = CF_HTML.initElement('SPAN', {'class':['ctf-department']}, 'cf_wrap_multi_recipient_level_2');
                    //Create hidden to stote recipient selected
                    var hidRecipient = CF_HTML.initElement('INPUT', {'name': 'cf_selected_recipient', 'type': 'hidden', 'class':['ctf-submit-data']}, 'cf_wrap_multi_recipient_level_2' );
                    //Create ul li
                    var ulRecipient = CF_HTML.initElement('UL', {'id':'cf_ul_multi_recipient', 'class':['ctf-department-dropdown', 'ctf-hidden']}, 'cf_wrap_multi_recipient_level_2');
                    for (var i in recipients){
                        //Create li
                        var eleLi = CF_HTML.initElement('LI', {'innerHTML': recipients[i].departmentName, 'class':['ctf-department-item'], 'data-value': recipients[i].departmentEmail}, 'cf_ul_multi_recipient');
                        //Add event on change fo LI
                        eleLi.addEventListener('click', function(e) {
                            e.preventDefault();
                            spanRecipient.innerHTML = this.innerHTML;
                            hidRecipient.value = this.getAttribute('data-value');
                        });
                        //Set value default for select box
                        if(!spanRecipient.innerHTML){
                            spanRecipient.innerHTML = recipients[i].departmentName;
                            hidRecipient.value = recipients[i].departmentEmail;
                        }
                    }
                    //Add event toogle class for select box
                    eleWrapLv2.addEventListener('click', function(e) {
                        e.stopPropagation();
                        //toggleClass
                        ulRecipient.addEventListener('mouseenter', CF_STYLE.toggleClass(ulRecipient, 'ctf-hidden') );
                        //ulRecipient.addEventListener('mouseleave', CF_STYLE.toggleClass(ulRecipient, 'ctf-hidden') );
                    });
                    //Add event mouse leave for select box
                    document.addEventListener('click', function () {
                        CF_STYLE.addClass(ulRecipient, 'ctf-hidden');
                    });
                }
            }
            
            //Image loading when send ajax
            var imgArrAttrs = {
                'src': PARAMS.baseUrl+'/library/img/send-loader.gif',
                'id': 'ctf-send-loader',
                'class': ['ctf-send-loader', 'hide']
            };
			//If normal form, put btn send to wrap div
			if(forms.form_style == Cf_Define.formKey.normal){				
				CF_HTML.initElement('DIV', {'id': 'ctf-wrap-btn', 'class': ['ctf-orientation', 'ctf-wrap-btn']}, 'ctf-contact-form');
				var idAppend = 'ctf-wrap-btn';
			} else {
				//Add class for btn send
				arrAttrs.class.push('ctf-orientation');
                //Add class for img loading send
				imgArrAttrs.class.push('ctf-orientation');
				var idAppend = 'ctf-contact-form';
			}			
            var button = CF_HTML.initElement('BUTTON', arrAttrs, idAppend);
            CF_HTML.initElement('SPAN', {'innerHTML': CF_FORM_MESSAGE[forms.form_language].btn_send, 'class': ['ctf-text-fly']}, 'cf_demo_button');
            var img_loading = CF_HTML.initElement('IMG', imgArrAttrs, idAppend);
            if (!forms.extra_info) {
				forms.extra_info = '';
            }
			//If normal form, put extra info to other div - always have this span
			if(forms.form_style == Cf_Define.formKey.normal){
				var idAppend = 'bg-normal-form';
			} else {
				var idAppend = 'ctf-contact-form';
			}
			CF_HTML.initElement('SPAN', {'id': 'ctf-tel', 'innerHTML': forms.extra_info, 'class': ['ctf-tel', 'ctf-orientation']}, idAppend);

            if (forms.form_color) {
                var arrEle = document.getElementsByClassName('corlor');
                for (i = 0; i < arrEle.length; i++) {
                    arrEle[i].style.backgroundColor = forms.form_color;
                }
                var arrEleText = document.getElementsByClassName('corlor-text');
                for (i = 0; i < arrEleText.length; i++) {
                    arrEleText[i].style.color = forms.form_color;
                }
            }
            
            CF_HTML.initElement('DIV', {'id': 'ctf-form-img', 'class': ['ctf-form-img', 'ctf-right']}, 'ctf-contact-form');
            var arrAttrs = {
                'src': PARAMS.baseUrl+'/library/img/boing.png'
            };
			if(forms.form_style == Cf_Define.formKey.classic){
				arrAttrs.src = PARAMS.baseUrl+'/library/img/completed.png';
			}
			//If normal form, no image
			if(forms.form_style != Cf_Define.formKey.normal){
				CF_HTML.initElement('IMG', arrAttrs, 'ctf-form-img');
			}

            //Set button position
            if (forms.button_position) {
                //Set button position for page build and edit
                if (formVersion && formVersion == Cf_Define.formVersion.demo) {
                    //Multi language -> need hard code widh form, as 3 type of class
                    CF_FORM.setButtonPosition(forms.button_position, forms);
                } else {
                    //Set button position for page client
					CF_FORM.setLiveButtonPosition(forms.button_position);
					//Multi language -> need hard code widh form, as 3 type of class
					CF_FORM.setLiveClassMultiLanguage(forms);

                }
            }
			//Set label of button contact
			CF_FORM.setLabelButton(forms.form_language);

            button.addEventListener('click', function() {
                CF_FORM.submitForm(idForm, formVersion);
            });
            
            return idForm;
        },
		array2json: function(arr) { //object to JSON
			var parts = [];
			var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');

			for(var key in arr) {
				var value = arr[key];
				if(typeof value == "object") { //Custom handling for arrays
					if(is_list) parts.push(array2json(value)); /* :RECURSION: */
					else parts.push('"' + key + '":' + array2json(value)); /* :RECURSION: */
					//else parts[key] = array2json(value); /* :RECURSION: */

				} else {
					var str = "";
					if(!is_list) str = '"' + key + '":';

					//Custom handling for multiple data types
					if(typeof value == "number") str += value; //Numbers
					else if(value === false) str += 'false'; //The booleans
					else if(value === true) str += 'true';
					else str += '"' + value + '"'; //All other things
					// :TODO: Is there any more datatype we should be in the lookout for? (Functions?)

					parts.push(str);
				}
			}
			var json = parts.join(",");
			if(is_list) return '[' + json + ']';//Return numerical JSON
			return '{' + json + '}';//Return associative JSON
		},
        getAnchor: function () {
            return (document.URL.split('#').length > 1) ? document.URL.split('#')[1] : null;
        }
    };
})();

//-----------------------------------------------------------------------------//

/*
 * Add and remove style css for HTML 
 */
CF_STYLE = (function() {
    return {
        addClass: function(el, css) {
            var tem, C = el.className.split(/\s+/),
                A = [];
            while (C.length) {
                tem = C.shift();
                if (tem && tem !== css)
                    A[A.length] = tem;
            }
            A[A.length] = css;
            return el.className = A.join(' ');
        },
        removeClass: function(el, css) {
            var tem, C = el.className.split(/\s+/),
                A = [];
            while (C.length) {
                tem = C.shift();
                if (tem && tem != css)
                    A[A.length] = tem;
            }
            return el.className = A.join(' ');
        },
        addAttribute: function(el, attrName, attrValue) {
            el.setAttribute(attrName, attrValue);
        },
        removeAttribute: function(el, attrName) {
            var attr = el.getAttributeNode(attrName);
            el.removeAttributeNode(attr);
        },
        toggleBetweenClass: function(ele, class1, class2){
            ele.classList.toggle(class1);
            ele.classList.toggle(class2);
        },
        toggleClass: function(element, toggleClass){
           var currentClass = element.className.trim();
           if(currentClass.indexOf(toggleClass) > -1){ //has class
              newClass = currentClass.replace(toggleClass,"")
           }else{
              newClass = currentClass + " " + toggleClass;
           }
           element.className = newClass;
        },
        //Type : in or out
		fade: function(type, el, duration, IEsupport) {
            var isIn     = (type == 'in'),
                IE       = (IEsupport) ? IEsupport : false,
                opacity  = isIn ? 0 : 1,
                interval = 50,
                gap      = interval / duration;

            if(isIn) {
                el.style.display = 'block';
                el.style.opacity = opacity;
                if(IE) {
                    el.style.filter = 'alpha(opacity=' + opacity + ')';
                    el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + opacity + ')';
                }
            }

            function func() {
                opacity = isIn ? opacity + gap : opacity - gap; 
                el.style.opacity = opacity;
                if(IE) {
                    el.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
                    el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + opacity * 100 + ')';
                }
                if(opacity <= 0 || opacity >= 1) { window.clearInterval(fading); }
                if(opacity <= 0) { el.style.display = "none"; }
            }
            var fading = window.setInterval(func, interval);
        }
    };
})();

//-----------------------------------------------------------------------------//

/*
 * Define validate rule for HTML
 */

CF_VALIDATE = (function() {
    return {
        validateRequire: function(field) {
            var value;
            if (field.tagName === 'INPUT') {
                value = field.value;
            } else if (field.tagName === 'TEXTAREA') {
                value = field.innerHTML;
            }
            if (value.trim() === '')
                return false;
            else
                return true;
        },
        validateFormat: function(rule, inputValue) {
            return Cf_Define.regex[rule].test(inputValue);
        }
    };
})();

//-----------------------------------------------------------------------------//

/*
 * Clone : Ajax request
 */
var Cf_Ajax = {
    toParams: function(o) {
        if (typeof o === 'string') {
            return o;
        }
        var pieces = [];
        for (var key in o) {
            pieces.push(encodeURIComponent(key) + '=' + encodeURIComponent(o[key]));
        }
        return pieces.join('&');
    },
    request: function(params) {
        var Request;
        var requestPrepare = function() {
            if (window.XMLHttpRequest) {
                Request = new XMLHttpRequest();
                if (Request.overrideMimeType) {
                    Request.overrideMimeType('text/html');
                }
            } else if (window.ActiveXObject) {
                try {
                    Request = new ActiveXObject('Msxml2.XMLHTTP');
                } catch (e) {
                    try {
                        Request = new ActiveXObject('Microsoft.XMLHTTP');
                    } catch (er) {
                    }
                }
            }
            if (!Request) {
                console.log('Could not create a XMLHttpRequest instance.');
            }
            Request.onreadystatechange = function(e) {
                if (Request.readyState === 1) {
                    (params.load || function() {
                    })();
                } else if (Request.readyState === 4) {
                    var re = Request.responseText;
                    if (params.dataType === 'json') {
                        try {
                            re = JSON.parse(re);
                        } catch (e) {
                        }
                    }
                    if ((Request.status > 199 && Request.status < 300) || Request.status === 304) {
                        (params.success || function() {
                        })(re, Request.status);
                    } else {
                        (params.error || function() {
                        })(Request.status, re);
                    }
                }
                params.always = params.always || function() {
                };
                try {
                    params.always(Request.readyState, Request.status, re);
                } catch (e) {
                    params.always(Request.readyState);
                }
            };
        };
        params.raw = params.raw || false;
        if (!params.raw)
            params.data = Cf_Ajax.toParams(params.data || {});
        params.async = params.async || true;
        requestPrepare();
        if (params.type === 'POST') {
            Request.open('POST', params.url, params.async, Cf_Ajax.username, Cf_Ajax.password);
            if (params.contentType)
                Request.setRequestHeader('Content-type', params.contentType + "; charset=" + (params.charset || 'utf-8'));
            else if (false !== params.contentType)
                Request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=' + (params.charset || 'utf-8'));
            Request.send(params.data);
        } else {
            Request.open('GET', params.url + ((typeof params.data !== 'undefined') ? (params.url.match(/\?/) ? '&' : '?') + params.data : ''), params.async, Cf_Ajax.username, Cf_Ajax.password);
            Request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            Request.send();
        }
        return Request;
    }
};

//-----------------------------------------------------------------------------//

/*
 * Define every thing hardcode in JS
 */
var Cf_Define = {
    // order: name after nothing, email after cf_demo_name
    orderField : {
        'name': 'cf_wrap_name',
        'email': 'cf_wrap_email',
        'phone': 'cf_wrap_phone',
        'company': 'cf_wrap_company',
        'website': 'cf_wrap_website',
        'subject': 'cf_wrap_subject'
    }, 
    formVersion : {
        'demo' : 'form_demo', // Render form in page
        'live' : 'from_live' // Render form to client
    },
	//ref form key to id form
    formStyle : {
        'style_basic': 'ctf-demo',
        'style_normal': 'ctf-demo-normal',
        'style_classic': 'ctf-demo-classic',
        'style_child': 'ctf-demo-child',
        'style_freestyle': 'ctf-demo-style',
        'style_restaurant': 'ctf-demo-restaurant'
    },
	//ref to form key
    formKey : {
        'basic': 'style_basic',
        'normal': 'style_normal',
        'classic': 'style_classic',
        'child': 'style_child',
        'freestyle': 'style_freestyle',
        'restaurant':'style_restaurant'
    },
	//message error when validate
	errMess : {
		'name': {
			'format': 'Please enter valid name.'
		},
		'email': {
			'format': 'Please enter valid email.'
		},
		'phone': {
			'format': 'Please enter valid phone.'
		},
		'company': {
			'format': 'Please enter valid company.'
		},
		'website': {
			'format': 'Please enter valid website.'
		},
		'subject': {
			'format': 'Please enter valid subject.'
		},
		'message': {
			'format': 'Please enter valid message.'
		}
	},
    //Regex for validate
    regex : {
        alphanum: new RegExp('[0-9\\p{L}]'),
        url: new RegExp(/(https:[\/][\/]|http:[\/][\/]|www.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9\-\._\?\,\'\/\\\+&amp;%\$#\=~])*$/),
        email: new RegExp(/^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$/),
        message: new RegExp('[0-9\\p{L}]'),
        phone: new RegExp(/^\+?([0-9+ -]){6,15}$/)
    }
}

//-----------------------------------------------------------------------------//

/*
 * Render form
 */
var CF_FORM = {
    //Create Hidden element
    createHidden: function(value) {
        var arrAttrs = {
            'type': 'hidden',
            'name': 'cf_demo_hidden_form_id',
            'id': 'cf_demo_hidden_form_id',
            'class': ['ctf-submit-data'],// Class for submit data
            'value': value
        };
        var ele = CF_HTML.initElement('INPUT', arrAttrs, 'ctf-contact-form');
        return ele;
    },
    //Create Name element
    createName: function(language) {
        //Create div wrap name element
        var arrAttrs = {
            'id': 'cf_wrap_name',
            'class': [ 'ctf-form-group', 'ctf-orientation', 'ctf-half-size', 'clearfix'],
            'innerHTML': '<div class="ctf-tooltip"><span>'+CF_FORM_MESSAGE[language].invalid_name+'</span></div>'//Error message
        };
        //In order position by wrap
        var ele = CF_HTML.initElement('DIV', arrAttrs, 'ctf-contact-form', 'name');
        //Get error message tag
        var errEle = ele.getElementsByClassName('ctf-tooltip')[0];
        errEle.style.opacity = '0';
        //Create input name
        var arrAttrs = {
            'type': 'text',
            'name': 'cf_demo_name',
            'id': 'cf_demo_name',
            'placeholder': CF_FORM_MESSAGE[language].placeholder_name,
            'class': [
                'ctf-field', 'ctf-field-name', 'ctf-half-size', 'ctf-icon-small', 'ctf-icon-person', 'ctf-orientation',
                'ctf-submit-data'//class for submit data
            ],
            'size': '50'
        };
        var ele = CF_HTML.initElement('INPUT', arrAttrs, 'cf_wrap_name');
        //Require
        CF_STYLE.addAttribute(ele, 'data-require', 'true');
        //Focus : hide message error
        ele.addEventListener('focus', function() {
            errEle.style.opacity = '0';
        });
        return ele;
    },
    //Create Email element
    createEmail: function(language) {
        //Create div wrap email element
        var arrAttrs = {
            'id': 'cf_wrap_email',
            'class': ['ctf-form-group', 'ctf-orientation', 'ctf-half-size', 'clearfix'],
            'innerHTML': '<div class="ctf-tooltip"><span>'+CF_FORM_MESSAGE[language].invalid_email+'</span></div>'//Error message
        };
        //In order position by wrap
        var ele = CF_HTML.initElement('DIV', arrAttrs, 'ctf-contact-form', 'email');
        //Get error message tag
        var errEle = ele.getElementsByClassName('ctf-tooltip')[0];
        errEle.style.opacity = '0';
        //Create input
        var arrAttrs = {
            'type': 'text',
            'name': 'cf_demo_email',
            'id': 'cf_demo_email',
            'placeholder': CF_FORM_MESSAGE[language].placeholder_email,
            'class': [
                'ctf-field', 'ctf-field-email', 'ctf-half-size', 'ctf-icon-small', 'ctf-icon-email', 'ctf-orientation', 
                'ctf-submit-data'//class for submit data
            ],
            'size': '50'
        };
        var ele = CF_HTML.initElement('INPUT', arrAttrs, 'cf_wrap_email');
        //Require
        CF_STYLE.addAttribute(ele, 'data-require', 'true');
        CF_STYLE.addAttribute(ele, 'data-type', 'email');
        //Focus : hide message error
        ele.addEventListener('focus', function() {
            errEle.style.opacity = '0';
        });
        return ele;
    },
    //Create Phone element
    createPhone: function(language) {
        //Create div wrap phone element
        var arrAttrs = {
            'id': 'cf_wrap_phone',
            'class': [
                'ctf-form-group', 'ctf-orientation', 'ctf-half-size', 'clearfix'],
            'innerHTML': '<div class="ctf-tooltip"><span>'+CF_FORM_MESSAGE[language].invalid_phone+'</span></div>'
        };
        //In order position by wrap
        var ele = CF_HTML.initElement('DIV', arrAttrs, 'ctf-contact-form', 'phone');
        //Get error message tag
        var errEle = ele.getElementsByClassName('ctf-tooltip')[0];
        errEle.style.opacity = '0';
        //Create input
        var arrAttrs = {
            'type': 'text',
            'name': 'cf_demo_phone',
            'id': 'cf_demo_phone',
            'placeholder': CF_FORM_MESSAGE[language].placeholder_phone,
            'class': [
                'ctf-field', 'ctf-field-phone', 'ctf-half-size', 'ctf-icon-small', 'ctf-icon-phone', 'ctf-orientation',
                'ctf-submit-data'//class for submit data
            ],
            'size': '50'
        };
        var ele = CF_HTML.initElement('INPUT', arrAttrs, 'cf_wrap_phone');
        //Require
        CF_STYLE.addAttribute(ele, 'data-require', 'true');
        CF_STYLE.addAttribute(ele, 'data-type', 'phone');
        //Focus : hide message error
        ele.addEventListener('focus', function() {
            errEle.style.opacity = '0';
        });
        return ele;
    },
    //Create Company element
    createCompany: function(language) {
        //Create div wrap company element
        var arrAttrs = {
            'id': 'cf_wrap_company',
            'class': ['ctf-form-group', 'ctf-orientation', 'ctf-half-size', 'clearfix'],
            'innerHTML': '<div class="ctf-tooltip"><span>'+CF_FORM_MESSAGE[language].invalid_company+'</span></div>'
        };
        //In order position by wrap
        var ele = CF_HTML.initElement('DIV', arrAttrs, 'ctf-contact-form', 'company');
        //Get error message tag
        var errEle = ele.getElementsByClassName('ctf-tooltip')[0];
        errEle.style.opacity = '0';
        //Create input
        var arrAttrs = {
            'type': 'text',
            'name': 'cf_demo_company',
            'id': 'cf_demo_company',
            'placeholder': CF_FORM_MESSAGE[language].placeholder_company,
            'class': [
                'ctf-field', 'ctf-field-company', 'ctf-half-size', 'ctf-icon-small', 'ctf-icon-bag', 'ctf-orientation',
                'ctf-submit-data'//class for submit data
            ],
            'size': '50'
        };
        var ele = CF_HTML.initElement('INPUT', arrAttrs, 'cf_wrap_company');
        //Require
        CF_STYLE.addAttribute(ele, 'data-require', 'true');
        //Focus : hide message error
        ele.addEventListener('focus', function() {
            errEle.style.opacity = '0';
        });
        return ele;
    },
    //Create Website element
    createWebsite: function(language) {
        //Create div wrap website element
        var arrAttrs = {
            'id': 'cf_wrap_website',
            'class': ['ctf-form-group', 'ctf-orientation', 'ctf-half-size', 'clearfix'],
            'innerHTML': '<div class="ctf-tooltip"><span>'+CF_FORM_MESSAGE[language].invalid_website+'</span></div>'
        };
        //In order position by wrap
        var ele = CF_HTML.initElement('DIV', arrAttrs, 'ctf-contact-form','website');
        //Get error message tag
        var errEle = ele.getElementsByClassName('ctf-tooltip')[0];
        errEle.style.opacity = '0';
        //Create input
        var arrAttrs = {
            'type': 'text',
            'name': 'cf_demo_website',
            'id': 'cf_demo_website',
            'placeholder': CF_FORM_MESSAGE[language].placeholder_website,
            'class': [
                'ctf-field', 'ctf-field-website', 'ctf-half-size', 'ctf-icon-small', 'ctf-icon-global', 'ctf-orientation',
                'ctf-submit-data'//class for submit data
            ],
            'size': '50'
        };
        var ele = CF_HTML.initElement('INPUT', arrAttrs, 'cf_wrap_website');
        //Require
        CF_STYLE.addAttribute(ele, 'data-require', 'true');
        CF_STYLE.addAttribute(ele, 'data-type', 'url');
        //Focus : hide message error
        ele.addEventListener('focus', function() {
            errEle.style.opacity = '0';
        });
        return ele;
    },
    //Create Subject element
    createSubject: function(language) {
        //Create div wrap subject element
        var arrAttrs = {
            'id': 'cf_wrap_subject',
            'class': ['ctf-form-group', 'ctf-orientation', 'ctf-half-size', 'clearfix'],
            'innerHTML': '<div class="ctf-tooltip"><span>'+CF_FORM_MESSAGE[language].invalid_subject+'</span></div>'
        };
        //In order position by wrap
        var ele = CF_HTML.initElement('DIV', arrAttrs, 'ctf-contact-form','subject');
        //Get error message tag
        var errEle = ele.getElementsByClassName('ctf-tooltip')[0];
        errEle.style.opacity = '0';
        //Create input
        var arrAttrs = {
            'type': 'text',
            'name': 'cf_demo_subject',
            'id': 'cf_demo_subject',
            'placeholder': CF_FORM_MESSAGE[language].placeholder_subject,
            'class': [
                'ctf-field', 'ctf-field-subject', 'ctf-half-size', 'ctf-icon-small', 'ctf-icon-star', 'ctf-orientation',
                'ctf-submit-data'//class for submit data
            ],
            'size': '50'
        };
        var ele = CF_HTML.initElement('INPUT', arrAttrs, 'cf_wrap_subject');
        //Require
        CF_STYLE.addAttribute(ele, 'data-require', 'true');
        //Focus : hide message error
        ele.addEventListener('focus', function() {
            errEle.style.opacity = '0';
        });
        return ele;
    },
    //Create Message element
    createMessage: function(language){
        //Create div wrap message element
        var arrAttrs = {
            'id': 'cf_wrap_message',
            'class': ['ctf-form-group', 'ctf-orientation', 'ctf-full-size', 'clearfix'],
            'innerHTML': '<div class="ctf-tooltip" s><span>'+CF_FORM_MESSAGE[language].invalid_message+'</span></div>'
        };
        var ele = CF_HTML.initElement('DIV', arrAttrs, 'ctf-contact-form');
        //Get error message tag
        var errEle = ele.getElementsByClassName('ctf-tooltip')[0];
        errEle.style.opacity = '0';
        //Create input
        var arrAttrs = {
            'name': 'cf_demo_message',
            'id': 'cf_demo_message',
            'placeholder': CF_FORM_MESSAGE[language].placeholder_message,
            'class': [
                'ctf-field', 'ctf-field-message', 'ctf-full-size', 'ctf-icon-small', 'ctf-icon-pencil', 'ctf-orientation',
                'ctf-submit-data'//class for submit data
            ],
            'cols': '50'
        };
        var ele = CF_HTML.initElement('TEXTAREA', arrAttrs, 'cf_wrap_message');
        //Require
        CF_STYLE.addAttribute(ele, 'data-require', 'true');
        //Trigger focus to TEXTAREA to show placeholder :(
        ele.focus();
        ele.blur();
        //Focus : hide message error
        ele.addEventListener('focus', function() {
            errEle.style.opacity = '0';
        });
        return ele;
    },
    //Set button contact position in build page
    //Set button contact special class for multi language in build page
    setButtonPosition: function(p, forms) {
        var button_contact = document.getElementById('ctf-butt-contact');
        var arrStyles = {};
        var class_size = CF_FORM_MESSAGE[forms.form_language].class_size;
        if (forms.button_position === "right") {
            if (class_size === "class_size_small") {
                arrStyles = {
                    "position": "absolute", "transform": "rotate(-90deg)", "-moz-transform": "rotate(-90deg)", "-webkit-transform": "rotate(-90deg)",
                    "left": "1109px", "right": "auto", "top": "150px", "bottom": "auto", "background-color": "rgb(0, 159, 225)",
                    "width": "80px", "text-align": "center"
                };
            } else if (class_size === "class_size_medium") {
                arrStyles = {
                    "position": "absolute", "transform": "rotate(-90deg)", "-moz-transform": "rotate(-90deg)", "-webkit-transform": "rotate(-90deg)",
                    "left": "1085px", "right": "auto", "top": "150px", "bottom": "auto", "background-color": "rgb(0, 159, 225)",
                    "width": "130px", "text-align": "center"
                };
            } else {
                arrStyles = {
                    "position": "absolute", "transform": "rotate(-90deg)", "-moz-transform": "rotate(-90deg)", "-webkit-transform": "rotate(-90deg)",
                    "left": "1049px", "right": "auto", "top": "180px", "bottom": "auto", "background-color": "rgb(0, 159, 225)",
                    "width": "200px", "text-align": "center"
                };
            }
        } else if (forms.button_position === "left") {
            if (class_size === "class_size_small") {
                arrStyles = {
                    "position": "absolute", "transform": "rotate(-90deg)", "-moz-transform": "rotate(-90deg)", "-webkit-transform": "rotate(-90deg)",
                    "left": "369px", "right": "auto", "top": "150px", "bottom": "auto", "background-color": "rgb(0, 159, 225)",
                    "width": "80px", "text-align": "center"
                };
            } else if (class_size === "class_size_medium") {
                arrStyles = {
                    "position": "absolute", "transform": "rotate(-90deg)", "-moz-transform": "rotate(-90deg)", "-webkit-transform": "rotate(-90deg)",
                    "left": "344px", "right": "auto", "top": "150px", "bottom": "auto", "background-color": "rgb(0, 159, 225)",
                    "width": "130px", "text-align": "center"
                };
            } else {
                arrStyles = {
                    "position": "absolute", "transform": "rotate(-90deg)", "-moz-transform": "rotate(-90deg)", "-webkit-transform": "rotate(-90deg)",
                    "left": "308px", "right": "auto", "top": "180px", "bottom": "auto", "background-color": "rgb(0, 159, 225)",
                    "width": "200px", "text-align": "center"
                };
            }
        } else if (forms.button_position === "bottom_right") {
            arrStyles = {
                "position": "absolute", "transform": "none", "-moz-transform": "none", "-webkit-transform": "none",
                "left": "918px", "right": "auto", "top": "auto", "bottom": "143px"
            };
        } else if (forms.button_position === "bottom_left") {
            arrStyles = {
                "position": "absolute", "transform": "none", "-moz-transform": "none", "-webkit-transform": "none",
                "left": "502px", "right": "auto", "top": "auto", "bottom": "143px"
            };
        }
        
        if (button_contact) {
            for (var styleName in arrStyles) {
                button_contact.style[styleName] = arrStyles[styleName];
            }
        }
    },
	//Set button contact label
    setLabelButton: function(language) {
        var button_contact = document.getElementById('ctf-butt-contact');
        if (button_contact) {
            button_contact.innerHTML = CF_FORM_MESSAGE[language].btn_contact;
        }
    },
    //Set button contact position in live page
    setLiveButtonPosition: function(p) {
        var button_contact = document.getElementById('ctf-butt-contact');
        var btn_class = '';
        if (p === "right") {
            btn_class = 'ctf-right';
        } else if (p === "bottom_right") {
            btn_class = 'ctf-right-bottom';
        } else if (p === "bottom_left") {
            btn_class = 'ctf-left-bottom';
        } else {
            btn_class = 'ctf-left';
        }
        if (button_contact) {
            CF_STYLE.addClass(button_contact, btn_class);
        }
    },
	//Set button contact special class for multi language in live page
	setLiveClassMultiLanguage: function(forms) {
		var button_contact = document.getElementById('ctf-butt-contact');
		var btn_class = '';
		var class_size = CF_FORM_MESSAGE[forms.form_language].class_size;
		if (forms.button_position === "right") {
			if (class_size === "class_size_small") {
				btn_class = 'small-right';
			} else if (class_size === "class_size_medium") {
				btn_class = 'medium-right';
			} else {
				btn_class = 'large-right';
			}
		} else if (forms.button_position === "left") {
			if (class_size === "class_size_small") {
				btn_class = 'small-left';
			} else if (class_size === "class_size_medium") {
				btn_class = 'medium-left';
			} else {
				btn_class = 'large-left';
			}
		}
		if (button_contact) {
			CF_STYLE.addClass(button_contact, btn_class);
		}
	},
    //Submit contact form
    //formVersion demo or live
    submitForm: function(wrapFormID, formVersion) {
        var locationInfo = window.location;
        //Get all element in form
        var eles = document.getElementsByClassName('ctf-submit-data');
        var validForm = true;
        var dataSend = new Object();
        //Get location data send
        dataSend.url = locationInfo.href;
        dataSend.encodeForm = wrapFormID;
        for (var i = 0; i < eles.length; i++) {
            if (eles[i].tagName == 'INPUT' || eles[i].tagName == 'TEXTAREA') {
                //Get value of element
                var valueField = eles[i].value.trim();
                var htmlField = eles[i].innerHTML.trim();
                //Fucking crazy IE placeholder become inner HTML
                //https://connect.microsoft.com/IE/feedback/details/811408/ie10-11-a-textareas-placeholder-text-becomes-its-actual-value-when-using-innertext-innerhtml-or-outerhtml
                if(eles[i].tagName == 'TEXTAREA'){
                    var inputvalue = eles[i].getAttribute('placeholder');  // you need to collect this anyways
                    if (htmlField === inputvalue) htmlField = "";
                }
                //Validate require
                if (eles[i].getAttribute('data-require') == 'true') {
                    var validField = true;
                    if (valueField == '' && htmlField == '') {
                        validField = false;
                    }
                    //Validate format : regex
                    if (eles[i].getAttribute('data-type') != null) {
                        if (!CF_VALIDATE.validateFormat(eles[i].getAttribute('data-type'), valueField)) {
                            validField = false;
                        }
                    }
                    //If fail validate input
                    if (validField === false) {
                        //Previous ele = message ele
                        //Fade in tooltip error
                        CF_STYLE.fade('in', eles[i].previousElementSibling, 300, true);
//                        eles[i].previousElementSibling.style.opacity = '0.9';
                        validForm = false;
                    } else { //if success validate input
                        //Hide error element
                        eles[i].previousElementSibling.style.opacity = '0';
                        //Bind data valid to Object dataSend
                        if (eles[i].tagName === 'INPUT') {
                            dataSend[eles[i].name] = valueField;
                        } else if (eles[i].tagName === 'TEXTAREA') {
                            dataSend[eles[i].name] = valueField;
                        }
                    }
                } else { //Bind data valid to Object dataSend
                    if (eles[i].tagName === 'INPUT') {
                        dataSend[eles[i].name] = valueField;
                    } else if (eles[i].tagName === 'TEXTAREA') {
                        dataSend[eles[i].name] = valueField;
                    }
                }
            }
        }
        //If demo, dont submit data
        if (formVersion && formVersion == Cf_Define.formVersion.demo) {
            //Do nothing
        } else {
            //Submit data
            if (validForm === true) {
                dataSend = CF_HTML.array2json(dataSend);
                //Disable button send
                var button_contact = document.getElementById('cf_demo_button');
                CF_STYLE.addAttribute(button_contact, 'disabled', 'true');
                //Show img loading
                var img_loading = document.getElementById('ctf-send-loader');
                CF_STYLE.removeClass(img_loading, 'hide');
                Cf_Ajax.request({
                    url: PARAMS.baseUrl+'/handle/',
                    type: 'POST',
                    charset: 'utf-8',
                    data: {data: dataSend},
                    dataType: "json", // can remove this line ???
                    success: function(response) {
                        if (response.status === 'success') {
                            //Hide all contact form
                            var wrap = document.getElementById(wrapFormID);
                            var mask = wrap.getElementsByClassName('ctf-lightbox-overlay')[0];
                            mask.click();
                            //Clear all content form
                            CF_FORM.clearForm();
                             //Enable button send
                            CF_STYLE.removeAttribute(button_contact, 'disabled');
                            //Hide img loading
                            var img_loading = document.getElementById('ctf-send-loader');
                            CF_STYLE.addClass(img_loading, 'hide');
                        } else{
                            alert('Error in send process');
                        }
                    },
                    error: function() {}
                });
            }
        }
        return false;
    },
    //Clear content form after send successful
    clearForm: function(){
        var eles = document.getElementsByClassName('ctf-submit-data');
        for (var i = 0; i < eles.length; i++) {
            if (eles[i].tagName === 'INPUT' && eles[i].type === 'text') {
                eles[i].value = '';
            } else if (eles[i].tagName === 'TEXTAREA') {
                eles[i].innerHTML = '';
                eles[i].value = '';
            }
        }
        return false;
    },
    //Default when load form, hide all tooltip message
    hideAllTooltip: function(idForm){
        var form = document.getElementById(idForm);
        var errEles = form.getElementsByClassName('ctf-tooltip');
        for (var i = 0; i < errEles.length; i++) {
            errEles[i].style.opacity = '0';
        }
        return false;
    }
};
