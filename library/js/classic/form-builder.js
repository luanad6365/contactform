var $j = jQuery.noConflict();

//Onload page,find radio checked, and add class remove-checked, and add class atv for label
function findActive(wrap) {
    //Find radio checked
    $j(wrap + " input[type=radio]:checked").each(function() {
        //Add class remove-checked
        $j(this).addClass("remove-checked");
        //Add class atv for label
        var id = $j(this).attr('id');
        $j('[for="' + id + '"]').addClass("atv");
    });
}

function checkActive(wrap, tem) {
    $j(tem).click(function() {
        $j(tem).each(function() {
            $j(this).removeClass("atv");
        });
        $j(this).addClass("atv");
        var id = $j(this).attr('for');
        $j('#' + id).addClass("remove-checked");
        $j(wrap + " .remove-checked").each(function() {
            $j(this).attr("checked", false);
        });
        $j('#' + id).attr("checked", "checked");
    });
}

function setDefaultButtonPosition(pos) {
    //Collect data from language and position
    var data = $j('#build-form').serializeArray();
    var data_collect = {'form_language': '', 'button_position': ''};
    for (var i in data_collect) {
        for (ele in data) {
            if (data[ele].name == 'BuildForm[' + i + ']') {
                data_collect[i] = data[ele].value;
                break;
            }
        }
    }
    $j(pos).each(function() {
        var p = $j(this).attr('for');
        if ($j("#" + p).attr('checked') === 'checked') {
            CF_FORM.setButtonPosition($j("#" + p).val(), data_collect);
        }
    });
}

//Default, show form when load page
function renderHTMLForm(){
    var data = $j('#build-form').serializeArray();
    var data_collect = {'form_language': '', 'has_message': '1', 'has_name': '', 'has_email': '', 'has_phone': '', 'has_company': '', 'has_website': '', 'has_subject': '', 'form_color': '', 'button_position': '', 'form_style': '', 'recipient_email': '', 'custom_logo_link': '', 'extra_info': '', 'form_id': ''};
    for (var i in data_collect) {
        for (ele in data) {
            if (data[ele].name == 'BuildForm[' + i + ']') {
                data_collect[i] = data[ele].value;
                break;
            }
        }
    }
    CF_HTML.buildForm(data_collect, '', 'wrap-demo', Cf_Define.formVersion.demo);
}

$j(document).ready(function() {
    //Onload page, find radio checked, and add class remove-checked, and add class atv for label
    findActive("#btn-position");
    findActive("#ctf-type");
    //Onclick
    checkActive("#btn-position", ".choose_position");
    checkActive("#ctf-type", ".button-ct-form");
    //Set default position (also call in CF_HTML.buildForm() function )
    setDefaultButtonPosition(".choose_position");


    var picker = $j('.colorpicker').colorpicker({
        //color: "#009fe1"
    });

    picker.on('create', function(ev) {
        $j(".corlor").css('background-color', $j('#form_color').val());
        $j("#ctf-demo > .ctf-wrapper .ctf-field[data-required='true']").css('border-left-color', $j('#form_color').val());
    });

    picker.on('changeColor', function(ev) {
        $j(".corlor").css('background-color', ev.color.toHex());
        $j("#ctf-demo > .ctf-wrapper .ctf-field[data-required='true']").css('border-left-color', ev.color.toHex());
    });

    //Change extra info
    $j('#insert_extra_info').click(function() {
        var extra_info = $j('#extra_info').val();
        if (!$j.trim(extra_info)) {
//           extra_info = 'Tel: 0164 966 889';
            extra_info = '';
        }
        $j('#ctf-tel').html(extra_info);

    });

    $j('#insert_extra_info').trigger('click');

    //Change custom logo
    $j('#submit_logo_link').click(function() {
        
        var custom_logo_link = $j('#BuildForm_custom_logo_link').val();
        if ($j.trim(custom_logo_link)) {
            $j("#loader").show();
            $j.ajax({
                type: 'POST',
                url: PARAMS.baseUrl + "/form/checkImageUrl",
                data: {data: custom_logo_link},
                dataType: "json",
                cache: false,
                success: function(response) {
                    if (response.status == 'success') {
						//Show image
						$j('#custom_logo_image').removeClass('hide');
                        $j('#custom_logo_image').css({"width": "37px", "height": "37px"});
                        //Hide message error
                        $j('#BuildForm_custom_logo_link_em_').hide();
                    } else {
                        //Show message error
                        $j('#BuildForm_custom_logo_link_em_').html(response.message).show();
                        $j('#custom_logo_image').hide();
                    }
                    $j("#loader").hide();
                    $j('#custom_logo_image').attr('src', response.img_src);
                }
            });
        }
    });
    
    //Hide error
    $j('#BuildForm_custom_logo_link').focus(function(){
        $j('#BuildForm_custom_logo_link_em_').hide();
    });
    //$j('#submit_logo_link').trigger('click');

    //Change button position
    $j(".choose_position").click(function() {
        //Collect data from language and position
        var data = $j('#build-form').serializeArray();
        var data_collect = {'form_language': '', 'button_position': ''};
        for (var i in data_collect) {
            for (ele in data) {
                if (data[ele].name == 'BuildForm[' + i + ']') {
                    data_collect[i] = data[ele].value;
                    break;
                }
            }
        }
        var p = $j(this).attr('for');
        if ($j("#" + p).attr('checked') === 'checked') {
            if (p === "button-right") {
                CF_FORM.setButtonPosition('right', data_collect);
            } else if (p === "button-right-bottom") {
                CF_FORM.setButtonPosition('bottom_right', data_collect);
            } else if (p === "button-left-bottom") {
                CF_FORM.setButtonPosition('bottom_left', data_collect);
            } else {
                CF_FORM.setButtonPosition('left', data_collect);
            }
        }
    });

    //Render demo form 
    renderHTMLForm();
    
    //Show hide field in form
    $j('.chk_init_form').click(function() {
        var eleName = $j(this).attr('name');
        if ($j(this).attr('checked') == 'checked') {
			var language = $j('#BuildForm_form_language').val();
            //Add element
            switch (eleName) {
                case 'BuildForm[has_name]':
                    CF_FORM.createName(language);
                    break;
                case 'BuildForm[has_email]':
                    CF_FORM.createEmail(language);
                    break;
                case 'BuildForm[has_phone]':
                    CF_FORM.createPhone(language);
                    break;
                case 'BuildForm[has_company]':
                    CF_FORM.createCompany(language);
                    break;
                case 'BuildForm[has_website]':
                    CF_FORM.createWebsite(language);
                    break;
                case 'BuildForm[has_subject]':
                    CF_FORM.createSubject(language);
                    break;
            }
        } else {
            //Remove wrap and all child element			
            switch (eleName) {
                case 'BuildForm[has_name]':
                    CF_HTML.removeElement('cf_wrap_name');
                    break;
                case 'BuildForm[has_email]':
                    CF_HTML.removeElement('cf_wrap_email');
                    break;
                case 'BuildForm[has_phone]':
                    CF_HTML.removeElement('cf_wrap_phone');
                    break;
                case 'BuildForm[has_company]':
                    CF_HTML.removeElement('cf_wrap_company');
                    break;
                case 'BuildForm[has_website]':
                    CF_HTML.removeElement('cf_wrap_website');
                    break;
                case 'BuildForm[has_subject]':
                    CF_HTML.removeElement('cf_wrap_subject');
                    break;
            }
        }
    });
    
    //Change form style
    $j('.button-ct-form').click(function() {
        //Rebuild form and check images
        $j('#wrap-demo').html('');
        $j('#submit_logo_link').trigger('click');
        renderHTMLForm();
    });
	
	//Change language
    $j('#BuildForm_form_language').change(function() {
        //Rebuild form and check images
        $j('#wrap-demo').html('');
        $j('#submit_logo_link').trigger('click');
        renderHTMLForm();
    });

});