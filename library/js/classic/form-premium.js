var $j = jQuery.noConflict();

$j(document).ready(function () {
    toogleMulti();
    toogleAutorespond();
    $j('#submit_feature').click(function () {
        var valid = false;
        var validMulti = false;
        var validAuto = false;
        var enable_some_feature = false;
        if ($j('#enable_multi_department').is(':checked')) {
            validMulti = validateDepartment();
            enable_some_feature = true;
        } else {
            validMulti = true;
        }
        
        if ($j('#enable_autorespond').is(':checked')) {
            validAuto = validateAutorespond();
            enable_some_feature = true;
        } else {
            validAuto = true;
        }
        
        // If not turn on any feature, set valid to true and submit form
        if(!enable_some_feature){
            valid = true;
        } else { //else: if valid multi and valid auto then submit
            valid = validMulti && validAuto;
        }
        
        if (valid)
            return true;
        else
            return false;
    });
    
    $j('#enable_multi_department').change(function () {
        toogleMulti();
    });
    
    $j('#enable_autorespond').change(function () {
        toogleAutorespond();
    });
});

function validateDepartment() {
    var valid = true;
    $j('.department_email').each(function () {
        var pureObj = document.getElementById($j(this).attr('id'))
        if (CF_VALIDATE.validateRequire(pureObj)) {
            if (CF_VALIDATE.validateFormat('email', $j(this).val())) {
                $j(this).removeClass('input-error');
            } else {
                valid = false;
                $j(this).addClass('input-error');
            }
        } else {
            valid = false;
            $j(this).addClass('input-error');
        }
    });
    
    $j('.department_name').each(function () {
        var pureObj = document.getElementById($j(this).attr('id'))
        if (CF_VALIDATE.validateRequire(pureObj)) {
            $j(this).removeClass('input-error');
        } else {
            valid = false;
            $j(this).addClass('input-error');
        }
    });
    
    if (valid)
        return true;
    else
        return false;
}

function toogleMulti(){
    if( $j('#enable_multi_department').is(':checked') ){
        $j('#hid_multi_department').val(true);
        $j('.feature_multi').each(function(){
            $j(this).removeAttr('disabled');
        });
    } else {
        $j('#hid_multi_department').val(false);
        $j('.feature_multi').each(function(){
            $j(this).attr('disabled', true);
        });
    }
}

function validateAutorespond() {
    var valid = true;
    
        var pureObj = document.getElementById('autorespond_message')
        if (CF_VALIDATE.validateRequire(pureObj)) {
            $j('#autorespond_message').removeClass('input-error');
        } else {
            valid = false;
            $j('#autorespond_message').addClass('input-error');
        }
    
    if (valid)
        return true;
    else
        return false;
}

function toogleAutorespond(){
    if( $j('#enable_autorespond').is(':checked') ){
        $j('#hid_autorespond').val(true);
        $j('#autorespond_message').removeAttr('disabled');
    } else {
        $j('#hid_autorespond').val(false);
        $j('#autorespond_message').attr('disabled', true);
    }
}